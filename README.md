# ATHARIYYAH
- These are my notes, written in plain text/markdown format. Mostly gathered from audio-lessons by known Salafi Scholars or Teachers.
- The focus is on beginning with basics & essentials and then gradually climb up the ladder
- Also provided are recommended sites for more in-depth information.
- Anybody can benefit from them. You are recommended to also make such notes from audio lessons from the sites recommended and add to this repository by submitting a pull-request.

### ToDo: 
- Add source URLS of audio lessons
- Fix some markdown compatibility issues
