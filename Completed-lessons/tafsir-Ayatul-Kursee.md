## <big>Tafsir Ayah 255 of Surah Al-Baqarah from Tafsir of Sheikh Abdur-Rahman bin Naasir As-Sa'adi </big> 

`taught by:Abu Iyaad`
`{single lecture}`
<div style='text-align: justify;'>

***

Greatest ayah of Quran because of what it contains, of the meanings of Tawheed.  
Centered around tawheed, just like Surah Fatihah, Muwadatain, Ikhlas.  
Has 10 parts/sub-sentences  
***

1. ٱللَّهُ لَآ إِلَٰهَ إِلَّا هُوَ  
Allah! none has the right to be worshipped but He,

   - The Name "Allah", brings together all of the meanings of Uluhiyyah; is a name in itself & also an indication of all other names of Allah. So when we mention "Allah", it incorporates all other Names. But when we say any other Name (say "Ar-Rahman"), it doesn't indicate all other Names. Rather that Name only comprises its own meaning.  
  - To worship Allah is in the fitrah of mankind & Allah sent messengers, along w/ their Sharaa'i, to protect that fitrah.    
   فَأَقِمْ وَجْهَكَ لِلدِّينِ حَنِيفًاۚ فِطْرَتَ ٱللَّهِ ٱلَّتِى فَطَرَ ٱلنَّاسَ عَلَيْهَاۚ لَا تَبْدِيلَ لِخَلْقِ ٱللَّهِۚ ذَٰلِكَ ٱلدِّينُ ٱلْقَيِّمُ وَلَٰكِنَّ أَكْثَرَ ٱلنَّاسِ لَا يَعْلَمُونَ   
   So set you (O Muhammad ﷺ) your face towards the religion of Hanifa, Allah's Fitrah, with which He has created mankind. No change let there be in Khalq-illah (i.e. the Religion of Allah), that is the straight religion, but most of men know not. {Ar-Rum 30:30}
***

2. ٱلْحَىُّ ٱلْقَيُّومُۚ  
The Ever Living, the One Who sustains and protects all that exists.  

   - The Name "Al-Hayy" comprises of all those Names of Allah that represent *Al-Hayaat Al-Kamilah* (a perfect Life). E.g., As-Sam', Al-Basr, Al-Ilm, Al-Iraadah [Fa'aal al Limaa Ureed – Doer of what He Wishes} (Having a Will/Wish), Al-Qudrah (Having Power), etc & Sifaat Dhatiyyah
   Many of these Names can be known by pure reason, but reason can only tell us about a limited amount of Ilm about Allah. The Jahmiyyah, Mutazillah, etc based their knowledge of Allah on (corrupted) reason only. So they limited the Attributes of Allah to 7 only (those 7 which their reason understands after knowing that Allah is Al-Hayy). Every other Attribute of Allah mentioned in Quran has to submit to their reason. 
     - "Sifaat Dhatiyyah" are those Attribute of Allah with which His essence is described. He has an actual essence/existence, whose reality we don't know. It's unlike anything from His creation. He exists outside of His creation, above the 7 heaven. 
     - The use of philosophical language like saying "Allah is the creator of matter, time & space, thus Allah is immaterial, outside of time, spaceless". This is the Bidah of Jahm bin Safwaan. These are all ambiguous, dubious words, that we don't use. In reality they indicate Allah's (عزوجل) non-existence outside of mind, that He does not have any Attributes, Actions (as if He had Actions then that would mean, for them, that He is within time. He acts, then those Actions come to be, after they never came to be. This is succession in time).
   - All Attributes that relate to Allah's actions enter into the Name "Al-Qayyum", meaning The One Whose Existence is by Himself; He doesn't Exist on account of anything besides Him; He is Self-Subsisting, Existing by His Own Right & He isn't in need of anything besides Himself for His Existence to be maintained. This is unlike everything that exists among the creation – the heavens, earth, creatures. Al-Qayyum also means that everything besides Him depends on Allah as He is Al-Khaliq (The Creator), Al-Khallaq (The Masterful Creator), Ar-Raziq (The Provider), Al-Muhyyi (The One who Gives life), Al-Mumeet (The One who Takes life), The  Saver of life, The Rescuer from hardship, The One who Cures from Illness. These Names of Allah represent Actions of Allah سبحانه و تعالى & come under Al-Qayyum & He is free from needs from His creation, He is Al-Ghani (The One Free of all needs). He brought His creation into existence & causes them to remain in existence. Thus these two are from the greatest Names of Allah
***

3. لَا تَأْخُذُهُۥ سِنَةٌ وَلَا نَوْمٌۚ  
Neither slumber, nor sleep overtakes Him.   

    - *Sinnatun* means *Nu'asun* (weariness) & *Noum* means sleep. So it follows from the two Names "Al-Hayy Al-Qayyum". This removes the Shubuha that creating & maintaining all of this creation would make The Creator tired. 
     - This is a principle of Quran, that Allah affirms Names & Attributes for Himself in a specific sense (Ar-Rahman, Ar-Rahim, etc are all specific. Likewise, Al-Ilm, Al-Qudrah, etc as Attributes). Hearing & understanding these Names will make a person develop love for Allah. But, Allah denies for himself, in a general sense. E.g.,: 
        - "And there is none equal for or comparable to Him" {Surah Al-Ikhlas:4}
        - "Do not make for Allah any rivals, whilst you have knowledge"
        - "There is none which is a likeness to Him, He is the All-Hearer, All-Seer"
        - "Do you know of one who deserves Names like Allah is named"  
        All these are a general negation.  
     - Allah denies for Himself in specific only where there is a specift reason. E.g.,
        * The christians say that Allah has a son & Allah denied that He has a son
specifically.  
        * The jews said that Allah's Hands are tied (i.e., He is miserly), & denied that
        * Likewise in this part of Ayatul-Kursi, Allah negates weariness & sleep from Himself in order to remove the false presumption   
      *  This is not the case with the innovators like Jahmiyyah, etc; who don't afirm Attribute for Allah, but rather describe Him in specific negations, like saying "Allah is not deaf/blind"(instead of saying He is All-Hearer & All-Seer, "Allah is not ignorant"(instead of saying that Allah is All-Knower, "Allah is not dead"(instead of calling Him Al-Hayy). 
***

4. لَّهُ مَا فِى ٱلسَّمَٰوَٰتِ وَمَا فِى ٱلْأَرْضِۗ   
To Him belongs whatever is in the heavens and whatever is on earth.  

    - He informs here that He is the *Maalik* (Master) of everything on earth & in heavens & they are *Mamaleek* (under His servitude) & none of them can come out of this situation they are in.  
    "There is none in the heavens & on earth, except that it comes to Ar-Rahman as a slave"
      - Allah has all the Attributes that relate to the fact that He owns everything, which is his *Mulk*, His *tasarruf* i.e., how He deals/behaves with His creation, His *Mulk* indicates His *Sultan* (Authority), Ownership, *Kibriyya* (Might, Greatness)

***

5. مَن ذَا ٱلَّذِى يَشْفَعُ عِندَهُۥٓ إِلَّا بِإِذْنِهِۦۚ   
Who is he that can intercede with Him, except with His Permission?   

    - Connection b/w Allah's Mulk & reqirement of His permision to intercede: The latter is a perfection of the meaning of former.   
    - "Say:"Call upon those whom you assert to besides Allah, they possess not even the weight of an atom, either in heavens or on earth, nor have they any share in either, nor there is for Allah any supporter from among them". Intercession with Him profits not, except for him whom He permits. Until when fear is bansihed from angels' hearts, they say:"What is it that your Lord has said?" They say:"The truth. And He is the most High, the Most Great"." {Surah Saba 34:22-23}  
    Ibnul Qayyim (رحمه الله) said that in these 2 ayaat, Allah denies 4 levels/things of Shirk in succession & the polytheists may be worshipping false dieties due to believing any of these 4 things, so Allah denies all 4 of them:
      1. They don't own anyting, even of the weight of an atom in heavens or on earth. The first asl/root of shirk is them beleiving that they do. Thus Allah denied Mulk for anyone in the creation.
      2. Nor do these dieties have any *Mushaaraka* (share) in owning anything from heavens & earth. So first point denied any independant ownership for the *tawagheet* & this point denied **any share along with** Allah. Off goes the second basis of shirk.
      3. Nor does He (Allah) have from amongst them any *Dhaheer*[ظھیر] (helper). This type of Shirk was seen in many nations & they thought that some from the creation (like angels, celestial bodies, Awliya or 12 Imaams) aid Allah in running this world & Allah needs them.  
      4. No intercession profits except by one with Allah's permission. One might say that he agrees that a creation doesnt own/share anything, nor it supports Allah in running this world, but that creation may have a position by which it convinces Allah, like a minister intercedes to a king.  
      This isn't the case with the dominion of Allah & thus it shows *tamaam mulkihi* (the completeness of dominion) of Allah & that none has any qoul (say) or any tasarruf, except by Allah's permission.  
      "Say: To Allah belongs all intercession. His is the Sovereignty of the heavens & the earth, then to Him you shal be brought back" [34:44]
      So the permission depends on the pleasure of Allah & He isn't pleased except with Towheed & following His Messengers.  
***

6. يَعْلَمُ مَا بَيْنَ أَيْدِيهِمْ وَمَا خَلْفَهُمْۖ  
He knows what happens to them (His creatures) in this world, and what will happen to them in the Hereafter.   

    - This refers to Allah's All-Encompassing Knwoledge. There is nothing hidden from Allah. 
    - So Allah knows all affairs that are yet to come & those which have no end; those which have no limit. This is a refutation of people of Kalaam. Ahlus-Sunnah belives that Allah's existence is eternal, He is uncreated & always existed. And He existed with all His Names, He has always been Al-Khallaaq, Ar-Razzaaq, Al-Ghaffaar & all Attributes have always been with Him as well. Allah is Fa'aalul Limaa Ureed (One Who does what he wishes), Alah has a Will & He Chooses to Act. His Attributes are eternal & He Chooses & Wishes to Act, His Actions are eternal - He Choses to Speak whenever He Wills, with whatever He Wills, So His Speech is eternal &  Allah is eternally One who Acts & Speaks in the past & One who Acts & Speaks in the future. {Ahlul-Kalaam deny this & some believe that in the future, all events will come to an end, paradise & hellfire will stop, motions will stop & everything will freeze}. Imam Ahmad said: "Allah has never siezed to Be One Who Speaks when He Wills".  
***

7. وَلَا يُحِيطُونَ بِشَىْءٍ مِّنْ عِلْمِهِۦٓ إِلَّا بِمَا شَآءَۚ   
And they will never compass anything of His Knowledge except that which He wills.   

    - There's none from the creation who can encompass the Ilm of Allah & that which is to be known from the creation.
    - Creation only knows wat Allah  Allows & Wills them to know, in terms of affairs that are *Shar'eeah* (ike their knowing from the Names of Allah, angels, jinn, paradise, hellfire, life of grave, etc from the unseen, ahkaam, ibadaat, etc) & *Qadariyyah* (like the way Allah created His creation, the ways & means that He has put in it). 
    - All of the knowledge mankind has, from revelation or what they study from the worldy (affairs which are the *asbaab*, i.e., *Qadariyyah* aka *Kawniyyah*), this ilm is a tiny portion & insignificant compared to Allah's Ilm & types of Ilm. 
    - Most knowledgeable of the creation - the Messengers & Angels - said:
    "Glorified be you, we have no knowledge, except that which you taught us"
    - Mankind's nature is ignorant & oppresive. 
***

8. وَسِعَ كُرْسِيُّهُ ٱلسَّمَٰوَٰتِ وَٱلْأَرْضَۖ  
His Kursi (footstool) extends over (encompasses) the heavens and the earth,   

9. وَلَا يَـُٔودُهُۥ حِفْظُهُمَاۚ  
And He feels no fatigue in guarding and preserving them.   

    - Allah preseves them & whatever is within them, from all of the different worlds. He does so via *asbaab wan-nizamaat* (ways & means). Like drinking water removes a person's thirst. So water is a *sabab* here. There are so many of the asbaab & we can't understand them all. 
    - In the creation, things run by way of asbaab. Ahlus-Sunnah differ from Ash'aris, who don't believe in the *asbaab wal musababaat*. They say, e.g., water doesn't remove our thirst, rather Allah creates the feeling of removal of thirst at the same time we drink water, so water doesn't have any qualities that cause our thirst to be removed. They believe that to ascribe any properties/qualities to things is Shirk. So they are led to believe that at every instance, Allah is creating everything afresh. So they say, e.g., "The colour you are seeing of this table, is being created every micro-second, etc, perpetually", based on *Ilm-ul-Kalaam*. This was said by Ibn Kulaab, As-Ash'ari, etc, while arguing with Mu'tazillah, who don't believe Allah has Attributes. Ibn Kullab said "You say Allah can't have Attributes is coz you can't have something which is eternal along with Allah (Who is Eternal)". So Ibn Kulaab tried to make a distinction between attributes in creation & The Attributes of Allah. So he said that The Attributes of Allah are eternal but attributes of creation are being constantly re-created all the time, thus our attributes can't be eternal, as are we.  
    - We believe that Allah created things & put properties in those things & the properties have effects. So we can say "The water quenched my thirst" or "This medicine removed my pain", as Allah brimgs about the effect of the things by those properties He has put in them. 
    - So, there are causes, ways & means etc that hold the universe & the creation is how it is. Likewise there are things that we can't see, like angels, that are controlling (by the permission of Allah) certain phenomenon like rain, lightening, mountains. Also, since we believe in the asbaab & nizamaat, we have an asl (foundation) for studying the creation of Allah w/o any conflict (unlike what the kufaar assume). 
    - None of all this tires Allah & this is because of the perfection in his *Adhmah* (Greatness) & his *hikmah* (Wisdom)
***

10. وَهُوَ ٱلْعَلِىُّ ٱلْعَظِيمُ  
And He is the Most High, the Most Great. 

    - Allah is The Highest in 3 different ways. He is the One who is Highest, 
      - *Bi-Dhaatihee* (With His actual essence) 
      - *Bi-Adhmati Sifaatihee* (In terms of the Greatness of His Attributes) 
      - As He has subdued & controlled all of the creation &  every existing things has lowered & humbled itself to Him 
    - Imam Al-Awza'ee said: "We used to say, when the Tabi'een were widespread that Allah is ablove His throne"
    - Imam Qutaybah bin Saeed (d. 240AH) said: "This is the statement of the Imaams in Islaam & the Sunnah & the Jama'ah: We know our Lord above the 7th heaven, above His throne, as Allah said: Ar-Rahman Ascended above the Throne".
    - Likewise, there are many such statements from Abu Zur'ah Ar-Raazi; Abu Haatim Ar-Raazi; Zakariyyah As-Saaji; Ibn Battah Al-Ukbari, who said: "Allah does not dwelve within His creation nor vice versa, rather He is separate from His creation. Yet His Knowledge encompasses all of His creation. Muslims are upon Ijma' (unanimous agreement), from the Sahaba, the Tabieen & all of the people of knowledge from Muslims, that Allah is above His throne, above the heavens, separate from HIs creation & His Knowledge encompasses all of His creation. None denies it except the one upon doctrine of Hululiyyah 
***
All this makes our hearts love Allah, & our sould venerate & glorify Him

</div>
