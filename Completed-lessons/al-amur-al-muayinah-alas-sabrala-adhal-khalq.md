<big> 
<div align='center'>  

## الامور المعینہ علی الصبر علی اذی الخلق
## Keys to Attaining Patience with People's Harms</big>
**Shaykh ul-Islaam Abul-Abbas Taqiuddin Ahmad ibn Abdul-Haleem ibn Abdus-Salaam ibn Taymiyyah Al-Harraani (رحمه الله)**  
**Explained by Sheikh Abdur-Razzaq bin Abdul-Muhsin Al-Abbad Al-Badr (حفظه الله)**  
**Taught by Shaykh Abul-Abbas Moosaa Richardson (حفظه الله)** 
</div>

***


<div style='text-align: justify;'>  


<span style='color:red;'>[L1 | 16/08/1441]</span>
- Patience is a great station & rank from the stations & ranks of the deen. 
- Imam Ahmad said: Patience is mentioned in Quran in > 90 places.
- Pateince is of 3 types:
  - Patience to do acts of obedience & continuing doing them (*Ta'aat*) & have stability (*Thabaat*). Highest rank 
  of patience as it is to be executed all te time
  - Patience to leave off/resist harmful, impermissible matters; desires.
  - Patience against losses & trials that Allah decrees. 
- In Quran, Allah:
  - Praised people who were patient.
  - Mentions rewards for those patient in hereafter & that He loves them & is with them (Ma'eeyatun Khaasah) 
  {Ma'eeyah Aa'mah is for everyone, & Ma'eeyah Khaassah is for Awliyah)
- Patience is a broad subject & many other topics are related to it. We'll deal with only one aspect: Patience when facing harm from the creation. 
- Since people are of differnt types & vary in levels of their characters, make-up, nature & interactions, so a 
person in this life isn't able to be safe from the harms of the people. 
  - It's a must for a muslim gto adorn himself with patience upon the harms of *al-khalq* (the creation). 
  - Many people fall short on this *baab* (subject/concept)
  - Thus the work/speech of the ahlul-ilm, on what will aid a person in being patient with al-khalq, is a lantern of light for muslim.
- *Deen* is two halves: being patient & being grateful 
***

<div align='center'>  

### <big>01</big></div>
<span style='color:green;'> That the servant testifies that Allah is the creator of the actions of the people - their movements (or lack thereof), their ambitions. So whatever Allah willed to happen, it happens. So nothing moves in the heavens & earth w/o the permission of Allah. And the people are an *aalah* (tool), look to the One that gave them the ability to overtake you (i.e., think beyond the transgression/harm that you are facing, & remember that it is Allah Who created the action of all people). and you will *tastarih* relax from the *hamm*(sadness) & *gham*(being overly worried/concerned) {Huzn:Past |  Gham:Present |  Hamm:Future} </span>  

{Here, before talking about manners, Sheikh ul-Islaam first taught us matters of Aqeedah i.e., Usool before furoo'. This is the way of Ulama Rabbaniyoon}
- The servants are created & so are their actions
- The person hmself doesn't will any type of actions, except it is the Will of Allah.   
 <<And you will not, unless (it be) that Allah wills, the Lord of the 'Alamin.>> [81:29]
- If a man was sincere & devout in his obedience to Allah, He is an Awliya of Allah. So when he seeks protection 
from Allah, it is granted. So how then does he get harmed by the people except that he looks that perhaps there is a flaw in my connection with Allah & there is a reason why Allah is not protecting me from these fooslish people. This is how the salaf 
used to look at such matters, even  the behaviour of a riding beast, as being an indication that the riding beastis 't obeying you coz you're not obeying Allah. Or that the disobedience of man's wife is a mirror of his relation with Allah. None looks at his life like this, except that he is going to blame himself & to improve. 
{this is the opposite of the western ideology, where you are opressed & all you need is that you take your right back from people & don't look beyond that}
***

<div align='center'>  

### <big>02</big></div>
<span style='color:green;'> That he testifies to his own sins, & that Allah only allowed them to be ble to harm him coz  of his sins  
  <<And whatever of misfortune befalls you, it is because of what your hands have earned. And He pardons much>> [42:30]  </span>

And if someone asks about the hadith of Rusulullah "Whomever Allah wants good for, then He puts him to trial" & 
"The people who are put to trial the most are the Prophets & then those most resembling them & then those who resemble them the most" {the ones 
Allah Wants the most good for (i.e., His most beloved creation), are put to trial the most}.  
So a believer sees the trial as a result of his own sins, & the reality may be that its not from his sins, 
but Allah loves him - That is with Allah. We ourselves take the generality of the verse mentioned above (42:30). So consider the result of both the stances 
  - Even if the believer is close to Allah, and he considers the calamity as a result of his misdeeds, he will repent & humble himself to Allah & if 
  it is actually due to his sins, he might be forgiven. 
  - And if he takes/sees the calamity only as a sign that Allah loves him, then he'll be lax, won't turn to 
  repentance, whatever might be 
  the actual reason of the trial befallen on him. The latter case is 
  insincerity & none does so except he is ruined. Such people are evil or Innovators, like Sufis. Deluded, open sinners
  - Ibn Abi Mulayka: I met some 30 Sahabis, each of them feared hypocrisy for himself.
  - **Hadith of Hanzala & Abu Bakr** (رضي الله عنهم)   
    Hanzala (رضي الله عنه) reported:  
    We were in the company of Allah's Messenger (ﷺ) and he delivered to us a sermon and made a mention of Hell-Fire. Then I came to my house and began to laugh with my children and sport with my wife. (Hanzala) further reported: I went out and met Abu Bakr & made a mention of that to him. Thereupon he said: I have done the same as you have mentioned. So we went to see Allah's Messenger (ﷺ) & said to him: Allah's Messenger, Hanzala has turned to he a hypocrite. And he (the Prophet) said Show respite. And then I narrated to him the story, and Abu Bakr said: I have done the same as he has done. Thereupon he (the Prophet) said: <u>Hanzala, there is a time for worldly affairs and a time for (worship & devotion), & if your state of mind is always the same as it is at the time of remembrance of Allah, the Angels would shake hands with you and would greet you on the path by saying: As-Salamu-Alaikum.</u>
    {Sahih Muslim 2750 b}

<span style='color:red;'>[L2 | 20/08/1441]</span>

<span style='color:green;'> Thus he busies himself with *tawbah*, & not with blaming people. Doing the latter is real calamity, but former becomes a *ni'amah* (blessing/oppurtunity). Ali (رضي الله عنه) said: "A word from the jewels of speech: A servant shouldn't have any real hopes for anything except his Lord, & he shouldn't have any real fear except for his sins". No trial fell except due to sin & no calamity was removed except with repentence.</span>

This point is based on first one. 
***

<span style='color:red;'>[L3 | 21/08/1441]</span>

<div align='center'>  

### <big>03</big></div>
<span style='color:green;'>

Recall that the goodness of the reward that Allah promised for those who excuse & be patient. [42:40] →  People are of 3 types: *Dhalimun* {takes/responds more than his right} | *Muqtasidun* {takes exact value} | *Muhsinun* {forgives & abandons his right}. The *Muhsin* will witness a call on *Yawn al-Qiyamah* - "Nay! Let then one stand up whose reward is incumbent on Allah", so none stands except ones known for excusing & rectifying matters. 

</span>  

There are 2 levels of good treatment:
1. *Martabtus-Sabr* (Rank of being patient)
2. *Ya'fua anhum* (Forgiving them)

Forgiving is a higher level than being patient, their recompense is with Allah HImself. [3:134] → Allah Loves Muhsineen. The station of *Ihsaan* → Hadith of Jibreel → Doing deeds knowing that Allah is watching you.  Such are the *Al-Muqarraboon Al-Muhsinoon*. 
Thus, bringing to mind the *Ajr wath-thawaab* (recompense & reward) helps a servant in increasing his level near Allah. 

[42:40] actually praises forgiving & also  *islaah* (rectification). So, sometimes only forgiving the matter may not bring improvement/rectification & may cause other person to be negligent.
- Ibn Uthaymeen in *"Makaaram ul-Akhlaaq"* said: From the balance of Islamic legislation, is that we've been given the option of giving excuse or to hold someone accountable for crimes. So for example, for the family of a man killed by hit & run car accident, it depends on the kind of person that driver is. 
  - <p> Comparatively, the jews were supposed to always take retribution & hold the punishment. So this became difficult for them, so they could't fulfil it all times, so they'd excuse their nobles & rich.</p> 
  - <p> For the Christians, the legislation was opposite & had to always excuse all people. </p>
  So, Islam brings rectification & when its benefitial for all parties to hold account, then you punish & vice versa.
  
[16:126]
  - referring to *Jihad*
  - In a battle, in response to enemy pillaging our villages & killed its people, if we take their village, we can take those POW to get ransom, or kill them, but:
    - In response to their killing our women & children, we can't kill their non-combatants ∵ doing so, we would be crossing the limits Allah has set. 
    - ∴ we can legit respond their evil with same evil only as long as it is permissible. 

An example of the three scenarios is: A woman wants *Khula* from her husband. SO she has to give *fidya* (negotiable) to the man to end the marraige. 
- *Muqtasid*: Man takes only the amount of *Mahr* & lets her go
- *Muhsin*: Leaves off some or all of the *mahr* & lets her go.
- *Dhaalim*:  Doesn't let her go.
***


<span style='color:red;'>[L4 | 24/08/1441]</span>

<div align='center'>  

### <big>04</big></div>
<span style='color:green;'> 

Bring to mind that excusing & being kind would give him *salamatil-qalb* (purity/cleanliness of heart) for his brother from *al-ghish* الغش (deception) | *al-ghil* الغل (rancour) for other muslims | *talab al-intiqaam* | *iraadatish-shar*. He will get sweetness of pardoning in his short & long term affair, way beyond whatever he would've gained by taking revenge. He'll be included inwhat Allah described in 3:134, making him beloved to Allah. As if 1 dirham (silver coin) was taken from him, but that was replaced by 1000s of dinaar (gold coin).

</span>  

Some people are quick to take revenge, to get *raahah* (personal healing & relax). A person might falsely assume that excusing others is humiliation/belittlement for himself. But its the opposite. Man's *raahah* is in *a'fu*. {This life & this dunyah is flawed, there'll always be oppression, taking of rights, mistakes, etc. None is exclusive to this. Think about your own transgressions}. And he doesn't pardon except that he gains in *al-izza* (might & honour) & relaxation of heart.  
- 24:22
  - *Sabab An-Nuzool* (reason of revealing down) of this verse: Indirectly addressing Abu Bakr (radhiAllahu anhu), tho is for all Muslims in general. This is related to the incident of *al-ifk* against Aisha (radhiAllahu anha), when one of the compaions (named Mistaah ibn Uthaathah) spread that slander, w/o verifying. So many Muslims were angered with him, even his mother disowned him. Mistaah (radhiAllahu anhu) was a Muhajir, poor & relative of Abu Bakr, who used to give him charity. Upon hearing this verse, Abu Bakr (radhiAllahu anhu) said that he will never stop giving Mistaah & both Abu Bakr & Aisha (radhiAllahu anhum) forgave him. 
    - The Imaan of Abu Bakr (radhiAllahu anhu) was such that if it was weighed against Imaan of the whole Ummah, the latter won't outweigh the former. 
    - *Sabab An-Nuzool* isn't *mukhassis* (i.e., it doesn't make the verse specifoc only to that incident), but the lesson to be learnt from the verse is based on how general& all-encompassing the wording is. 
- *Hadeeth Al-Musalsal bil Awwaliyyah* (the first hadith narrated by many in their chain):
   "The people who ahve mercy on others, Allah has mercy on them. Have mercy on people on earth & The One who is above heavens will have mercy on you".
***
<div align='center'>  

### <big>05</big></div>
<span style='color:green;'> 

Know that none ever got revenge for himself except that brought with itself *dhullan* (disgrace & humiliation), internally. Rusulullah said: "One who excuses others isn't increased by Allah in anything but honour". This honour is much bigger than that which he actually gets by taking revenge. The latter is only outward & apparent honour, but it brings inetrnal disgrace. *Al-afoo'* may seem disgrace outwardly, but it brings about honour inside & outwardly, in the long run. He'll be considered a man of strength & upright character

</span>  

{People might've noticed this - that when they get back their revenge, they still are bothered & internally upset & unresolved}
{The christain religion promotes too much of an imbalanced *afoo'*, where they are supposed to pardon all the time, even if its not in that person's best interest to be forgiven.}

***

<span style='color:red;'>[L5 | 25/08/1441]</span>

<div align='center'>  

### <big>06</big></div>

<span style='color:green;'> 

This one is from the *A'adham al-fawaid*(greatest of benefits) in this topic. That he remembers that *"الجزاء من جنس العمل"* [The recompense for your behaviour is related to your own behaviour] {Thus if you always carry out retributions & never overlook, you may not be excused by Allah on *yom al-qiyammah*, but if you overlook & pardon when you have oppurtunity/authority to, you hope for such interaction/respnse from Allah. ∴ we should connect our worldly situations & interactions with *yom al-qiyammah*}. And he remembers that he himself is sinful & that whoever excuses the people, then Allah will excuse him. This is enough for an *'Aaqil* (person of intellect).

</span>  

{  
Whoever *satara* (screens) a Muslim (while he did some act in private, & you got to know it), Allah screens him in this life & hereafter.
  - Allah is *As-Sitteer Al-Adheem* (One Who Screens people with the best screen)
  
Whoever removes a hardship from a believer, from the burdens of this dunyah, then Allah will relieve him from a burden from the burdens of hereafter.  
"For *Rahimooon* (people who are merciful), *Ar-Rahmaan* extends his mercy to them". Thus ***Al-jazaa' min jinsil 'aml*** [e.g., 24:22 again].  
Allah assists his servant so long as he assists his brother.   
The *munafiqeen* (hypocrites) tried to decieve Allah, but Allah is the one decieving/deluded them. 
  - So with evil too, *Al-jazaa' min jinsil 'aml* holds. Allah responds to their trickery with what they deserve.

Hadeeth: "The believer that mixes with the people & he bears their harms, is *khair* (better) than the believer who doesnt mix with the people or bear their harms".
  - This hadeeth is the crux of this whole book
  - There is no "other place" you can go where people won't harm you. So no matter who you mix with, you'll be harmed. So better be & bear with the *Mumineen*. 
  - [15:47]  
    - ∴ even those who will go to paradise, had *ghil* (rancour) in their hearts, [coz we are flawed]. But Allah removed that. 
      - So we should have realistic expectations & remove the burden from ourselves.   

  	
}
***

<span style='color:red;'>[L6 | 28/08/1441]</span>
<div align='center'>  

### <big>07</big></div>

<span style='color:green;'> If he busies himelf with taking revenge, then his time will be lost., his heart will be split up here & there (w/o tranquility), he'll lose benefitial oppurtunities (for good deeds), and possibly this calamity will be more of a loss than the one earlier. If he excuses, his heart & body will be empty (of rancour & planning resp) & now will be available for benefitial & important matters. </span>

Occupying w/ revenge makes one plan, and make his focus on retribution. He loses a portion of his life, losing religious or worldly matters. ∴ it's befitting that he relaxes & instead of losing his precious time & efforts in doing harm, he excuses for Allah (تعالى) or remains patient, seeking Allah's reward.   
{Hadeeth:  
<u>**نعمتان مغبون فيهما كثير من الناس: الصحة، والفراغ**</u>  
*"There are 2 blessings that so many people are tricked into losing: ***as-sihah***(good health) & ***al-faraagh***(free time)"*  
Explanation by Imam Ibn Baz (رحمه الله): [https://binbaz.org.sa/fatwas/8363/%D9%85%D8%B9%D9%86%D9%89-%D9%82%D9%88%D9%84%D9%87-%D9%86%D8%B9%D9%85%D8%AA%D8%A7%D9%86-%D9%85%D8%BA%D8%A8%D9%88%D9%86-%D9%81%D9%8A%D9%87%D9%85%D8%A7-%D9%83%D8%AB%D9%8A%D8%B1-%D9%85%D9%86-%D8%A7%D9%84%D9%86%D8%A7%D8%B3]   
}  
{  
<u>**اغتنم خمسا قبل خمس: شبابك قبل هرمك و صحتك قبل سقمك و فراغك قبل شغلك و غناك قبل فقرك و حياتك قبل موتك**</u>  
Ibn Abbas reported from Rusulullah (ﷺ): *"Take benefit from 5 (things) before 5 (things happen):* 
- *your youth before your old age;* 
- *your health before your illness*
- *your riches before your poverty*
- *your free time before preoccupation*
- *and your life before your death"*

}  
{  
The Messenger (ﷺ) said, ***"The over-stringent ones are destroyed"***:  
Imaam Nawawi (رحمه الله) said: *"The over-stringent ones are those who look too deeply (into affairs) and are excessive- those who exceed the Hudood [prescribed legislated sharia boundaries] in their statements and actions"*.  
}

***
<span style='color:red;'>[L7 | 29/08/1441]</span>
<div align='center'>  

### <big>08</big></div>

<span style='color:green;'> That a person getting revenge in full & getting angry for that sake, <u>Rusulullah never got angry in a way that motivated him to act our of anger **for his own self**.</u> So if this is the case w/ the *khaira khalqillah* (best of the creation) & the most *akram* (noble) of them w/ Allah, while offenses against Rusulullah were actually offences against Allah & the things connected w/ him were connected to the *huqooq ad-deen* (rights of religion) & himself - the *ashraful-anfus wa azkaaha wa abarruhaa wa-ab'aduhaa* (most honoured of souls & the most pure & the most pious & the most distant of all) from bad behaviour, the most deserving of every beautiful behaviour. And yet, with all of this, he never was angry for a personal issue. even while he had the most right to be respected. 
Then how can one of us seek to get revenge for our own soul, with which we are acquainted {our evils & defects}. For *ar-rajulul-A'arifu* (man of good understanding), he doesnt have a personal reputation valuable enough to his ownself that he should get angry & upset for it. A real righteous person doesn't have that status that he feels for himself, or his soul deserves that kind of reverence, that would require him to get revenge
</span>



Man should look at the *seerah* of Rusulullah. Allah made him a *qudwah* (role model/best example) for His *ibaad* (worshippers). [33:21].   
{  
the opponents of messengers used to say that if youre a rusool, you should have an angel w/ you or be an angel yourself. Reply: 
- [17:94-95] 
  - Angels don't have lives like men - they don't eat, drink, sleep, have spouses, etc. 
  - So in so many spheres of life, angels can't be a role model for men. 
  - Thus, its against the Wisdom of Allaah to make for us prophets from angels
  - Thus Allah Chose the best from the men.
- [6:8-10] 
- [6:111-113] 
- [11:12] 
- [25:7-10] 
- [25:21-22] 
- [43:53-54]  

}  
The only time Rusulullah would be upset is when the boundries of Allah were violated. Then nothing could stand up to his anger, for the sake of Allah. When the companions, who knew he was *ra'oofun raheemun*, saw this, they knew it was for Allah's sake.    
{ Like, when one sahabi brought some pages of *Taurah*, Rusulullah's face changed, until the sahaba said: "I seek the Refuge of Allah from Allah's Anger & the anger of His *Rusool*". It was so cause he thought the sahabi was seeking guidance from that corrupted book. So he said: "If Moosaa was alive today, he would have no except to follow me" (as what was revealed to Moosaa was abrogated by what was given to Rusulullah)}.   
Ai'sha said: "*Rusulullah* didn't become personally offended & angry for himself, for any affair that had come to him, only until the *hurumaatillah* (limits set by Allah) had been violated. Then he'd be angry & upset for Allah's sake".   
Thus, look at this sweet *seerah* of Rusulullah & struggle hard to follow it.  
{As hard as we try, we still may have some rancour, in our hearts against any muslim, on the day we die. But this won't deny us paradise & a pure & untainted brotherhood in hereafter. There won't be such brotherhood in this dunya, as it is flawed & <u>there's no brotherhood that will resemble that of paradise</u>. We supplicate to Allah that He lets us enter that paradise & also that He makes us brethren in this world & we don't harm our muslim brothers in it. 
***

<span style='color:red;'>[L8 | 30/08/1441]</span>
<div align='center'>  

### <big>09</big></div>

<span style='color:green;'> If he was harmed:    
a) *fee sabeelillah* (in the path of Allah) (coz he was obeying Allah or due to any Ibaadah), then here it is <u>*waajib* upon him to have *sabr* & he isn't allowed to take revenge.</u> He was harmed fir sake of Allah, so his recompense is with Allah [26:109]. So the *mujaahidoon*, who lose their blood or money in the way of Allah, may not get replacement for that, as they're people from whom Allah purchased their lives & property. So their payments are upon Allah in hereafter. If any of them seeks compensation for harms they bore from the creation, he doesn't get reward from Allah. For those whose <u>*talaf* (destruction/loss)</u> has been for Allah, then it is upon Allah to <u>*khalaf* (reward him)</u>.  
{ Hadeeth: "There's no day except 2 angels come out. One of them says "O Allah! give the one who spends, a *khalaf* (compensation/replacement/reward)" & other says "O Allah! give the one who holds back(doesn't spend) a *talaf* (opposite of khalaf, destruction) (for one who doesn't spend on obligatory ways of sepending, not on voluntary ways. For latter, you're not blamed, tho you lost the virtue)"." }  
b) due to a calamity (not a transgression. Like an accident/mishap), then let him busy himself w/ blaming himself & not one who accidentally harmed him.  
c) while seeking a share of *dunyah* (like trade), then let him plant himself on *sabr*, as for him to get worldly shares w/o patience is something hard to expect. One w/o patience in midday sun,  rains, snow, hardships of journeys, highway robbers (worldly affairs), then he has nothing to do in business. One who is honest in his search/seeking out of something, then he'll be prepared to be as patient as the level of his honesty.   
</span>

***

<span style='color:red;'>[L9 | 01/09/1441]</span>
<div align='center'>  

### <big>10</big></div>

<span style='color:green;'> He brings to mind, the *Maa'eeyah* (presence) of Allah w/ him [8:46], Allah's *Mahabbah* (love) [3:146] & *Ridhaa* (pleasure), if he's patient. Whoever Allah is w/, He will repel from him all harmful matters, the likeness of which none else can repel. </span>

Look for such a reward & it'll keep you busy away from *intiqaam* (revenge). 
This specific *Maa'eeyah* is *Maa'eeyah Khaassah*, which includes:  
| *an-nasr* (victory) | *al-hifdh* (protection) | *at-tawfeeq* (success) | *at-tasdeed* (firmness) | *al-ma'oonah* (assistance) | *al-khair* (goodness) | *al-barakah* (blessings) |    

{  
Sufis misuse these ayaat / ahadeeth about *Maa'eeyah*.  
- [58:7] → Here Allah is talking about His Knowlege & His Hearing & Seeing, as that's what is what the Ayah behan w/ (*sibaaq*), ended with & the whole Surah is about Allah's Ilm, Hearing & Seeing  
Also this *Maa'eeyah* is not specific to a pious person. Allah is even with the disbelievers in this sense of knowledge.  
Ibn Taymiyyah: "There is no Aayah used by the people of falsehood, except that same Aayah can be used as a proof against them"  
- [57:4] → This Aayah also mentions the Knowledge of Allah again & again, while saying that Allah is Above His throne.   
}

{*Maa'eeyah Khassah* is for *Awliyah of Allah*, like when Rasulullah & Abu Bakr were hiding in the cave, from the Mushrikeen & Rasulullah said that Allah is with them & Will protect them.   
[16:128]
As for *Mahabbah*, then it is not general for all. Allah doesn't love kuffar [1:7]. We affirm that Loves some & Hates some of His creation, unlike what christains believe.
**Hadeeth of the Walee** → Abu Hurayrah: Rasulullah (ﷺ) said: "Allah said: 'I will declare war against him who shows hostility to a pious worshipper of Mine. And the most beloved things with which My slave comes nearer to Me, is what I have enjoined upon him; and My slave keeps on coming closer to Me through performing Nawafil (praying or doing extra deeds besides what is obligatory) till I love him, so I become his sense of hearing with which he hears, and his sense of sight with which he sees, and his hand with which he grips, and his leg with which he walks; and if he asks Me, I will give him, and if he asks My protection (Refuge), I will protect him; (i.e. give him My Refuge) and I do not hesitate to do anything as I hesitate to take the soul of the believer, for he hates death, and I hate to disappoint him." [Saheeh Bukhari: 6502]
}
***


<span style='color:red;'>[L10 | 03/09/1441]</span>
<div align='center'>  

### <big>11</big></div>
<span style='color:green;'>
Bring to mind that patience id *nisful-Imaan*, so don't spend any amount of your *Imaan* gaining retribution. If you're patient & guarded your *Imaan* from deficiencies, Allah Defends the believers from harm
</span>

{deen of Allah is 2 things, from an angle. Depending the type of day you're having, you either have a challenge of a *ni'amah*(be thankful for it) or calamity/loss(be patient about it).}   
Hadeeth: [Saheeh Muslim 2999]   
[14:5] | [31:31] | [34:19] | [42:33] → such are complete believers  
So, one who's harmed says: "I'm not getting revenge, rather i'll reamin patient, so i can preserve my status on this great station (of patience) & i won't spend from it". So, even if it was permissible to take revenge, it won't be of such high level of *Imaan* as is being patient

***


<div align='center'>  

### <big> 12 </big></div>
<span style='color:green;'> He brings to mind that his patience is him being able to give an order/verdict to, dominating, overtaking his own soul {he's the *haakim*/incharge}. When one's done that, that soul won't have hopes of loosening him up, weakening, enslaving & sending him to destructive behaviour.   
Else, his soul'll dominate & destroy him, if he's not saved by Allah's mercy.
Person controlling his soul & the shaytaan w/ him, shows his authority. He's happy, strong, w/ strong *Imaan* & able to cast away enemy from his heart. 
</span>

- Your patiece on their harm, is you getting back your soul victoriously, as opposed to him who takes revenge, who does what his desires ask him to do. 
- Hadeeth: [Saheeh Al-Bukhari:2039]  
  - for wherever the blood goes   
- Hadeeth: [Jaami' At-Tirmidhi:1105]
  - *Isti'adha* from he evils of our souls & our actions, is a Sunnah.
***

<div align='center'>  

### <big> 13 </big></div>
<span style='color:green;'> 
If he's patient, then Allah will aid him, absolutely. Allah is the *Wakeel* (Disposer of affairs) for patient. This person is leaving off the affair of *dhaalim* to Allah. One who gets revenge, then Allah leaves/entrusts him to himself. And Allah is *Khairun-Naasiren* (Best of those who Help/Aid). And one who is left to himself is *A'jazun-Nasireen* (most incapable of all to help{himself}). 
</span>

- He seeks his right from Allah. He looks forward to the result of his patience.   
- Hadeeth: [Musnad Ahmad:2800]  
- Hadeeth: [Saheeh Muslim:2558]  {also proves that Rasulullah didn't know unseen independently}
- Hadeeth: [Sunan Abi Dawud:4896]  
  - Responding & not continuing to remain patient leaves us to our own selves.
  - Abu Bakr did have the right to defend himself.
  - For a *Zaahid*, leaving this right of his, in return for what is with Allah is of higher level, even if taking retribution is not sin
    - A similar case is that of one who gets ruqyah done on him by someone else, instead of doing it himself. Even if this isnt a sin, but this disqualifies him of the virtue of being among those 70,000 who will enter paradise w/o any account, as mentioned in [Saheeh Al-Bukhari:5705]. 
      - Such are the people who actualise their *tawheed* 

***

<span style='color:red;'>[L11 | 03/09/1441]</span>
<div align='center'>  

### <big> 14 </big></div>
<span style='color:green;'> His patience & bearing it, brings a recovery in oppresser from his *dhulm* & he'll be remorseful & appologise. The people will blame him & the oppressor will be ashamed & ally with one he oppressed. [41:34-35].   
</span>

Many have found this to be true. 
They are patient w/ constant harm from other & they react w/ what is better. Then at the end, the oppressor will be ashamed & may appologise. W/ this, you aided him in gaining control of his desires, bringing rectification [42:40]. This might take some time & forbearance.   
[41:34-35] → This *wilayah* (friendship) can be seen in the life of Rasulullah, where many of those who used to harm & hate him, ended up being his companions & supporters. E.g., Safwaan bin Umayah bin Khalaf. 

***

<div align='center'>  

### <big> 15 </big></div>
<span style='color:green;'> It can be that when one gets revenge/responds, that increases his opponent in evil & self determination to harm him further, while bringing witnesses of your response to him. If one's patient, then he's safe from this harm. An *aaqil* (person of intellect) doesn't choose the greater of 2 harms.   
{ some scholars said: To be able to identify the lesser of two harms, or greater of two benefits & applying it, thats the meaning of *fiqh* (understanding) in religion. Hadeeth: [Jaami' At-Tirmidhi: 2645] → "Whomever Allah wants good for, He gives him *fiqh* of *deen*"}  
How many times has taking revenge caused evil (greater than original evil), leaving the person unable to respond back further oppression, lives been lost, positions of authority been squandered & wealth lost.   
</span>

Patience repels greater harm. Responding w/ what is better has in it, safety from severe oppression for the patient.   
{A lot of *As-Siyasah Ash-Shar'eeyah* is linked to this, like the Ahadeeth of obeying the ruler & not fighting him. You're not to respond to violation of your right in a way that'll lead to more of your rights being taken. }

***

<span style='color:red;'>[L12 | 11-13/09/1441]</span>
<div align='center'>  

### <big> 16 </big></div>
<span style='color:green;'> One who often gets revenge & isn't patient, then its a must that he going to fall into oppression, as people's soul don't suffice themselves w/ what is perfectly just /sufficient, not in action nor in intention. Perhaps one will be unable to do that. Anger/fury can increase in a person to a level where  he doesn't know what he is saying or doing. So when he was *madhloom*, waiting for honour & victory from Allah for his patience, he turned himself into *dhaalim* w/ Anger of Allah.
</span>


- <big><big> اتَّقُوا الظُّلْمَ ، فَإِنَّ الظُّلْمَ ظُلُمَاتٌ يَوْمَ الْقِيَامَةِ </big></big>
- Sabr is safer & relieves you of accountability, keeps you innocent on day of judgement. [16:126] denotes the permissibility for revenge, not an order. 
- People arent able to perfectly measure the exact response that is allowed, when emotions are involved, so as not to do *dhulm*. Apart fromm the vast virtues of forgiving. 
***

<div align='center'>  

### <big> 17 </big> </div>
<span style='color:green;'> Act of oppression by which one was oppressed, is a reason for which the oppressed will have his sins expiated or his status w/ Allah will increase. But if he isnt patient & gets revenge, than that won't be an expiation or won't raise his status. 
</span>

Going for revenge makes one lose this great virtue
{[Bukhari:5640], [Bukhari:5641] - For the person who looks forward for reward for it, being patient w/o revenge}
***

<div align='center'>  

### <big> 18 </big> </div>
<span style='color:green;'> His patience & overlooking of opponent is his greatest *jund* (assistance/troop). One who is patient, then his patience brings an inner disgrace & fear in the one who oppressed & is excused, as people will shame him, even if you don't mention it to them. Taking revenge takes away this advantage. Thats why many people want the one they oppressed, to respond to balance out to not give him an advantage. And when the oppressor gets a response, he is able to get a relief from burden.  
</span>
{so in this act of one oppressing (in terms of money, etc) & other forgiving, the latter raised his status & former is lowered. So thank Allah that you are one who is raised.}

***


<span style='color:red;'>[L13 | 14/09/1441]</span>
<div align='center'>  

### <big> 19 </big></div>
<span style='color:green;'> If one excuses the oppressor, the latter will internally realise that former is above him in rank & he got over on him, in front of Allah & thus the oppressor will view himself under the one he oppressed, in terms of goodness. This is sufficient as a virtue & honour of forgiving people. 
</span>

Being closer to Allah is more honourable than benefits in *dunya*. We should be competitive in terms of gaining in *Akhirah*, & not for worldly gains. So this worldly loss is in reality a gain. Allah never increases the one who forgives except in honour. 

***

<div align='center'>  

### <big> 20 </big></div>
<span style='color:green;'> The act of overlooking others is in of itself a *hasanah* (good deed), & the nature of *hasanah* is that it leads to more *hasanah* {thus increasing his *Imaan*}. This might be a reason for him to attain eternal salvation. If he rather gets a revenge, then all of this goes away. 
</span>

{ *hasanah* (good deed) → Has in it *Ikhlaas* (inner sincerity) & *Mutab'ah* (outward confirmity to Sunnah of Rasulullah). 
*sayi'ah* / *ma'asiyah* (evil deed) → it is *Mukhalafatu Amrillah* (opposition to Allah's order). It doesn't have *shuroot* 
A Muslim shouldn't look down at good deeds, even if they are as minute as giving half a date in charity [Bukhari:1413]. Even such a *hasanah* can enter us into paradise
After a hasanah, the next hasanah is a bit more easier & *sayi'ah* is bit farther away. Likewise, a *sayi'ah* makes more *sayi'ah* easier for us & *hasanah* is difficult. Following bad deeds w/ bad deeds makes black stains on our heart & makes it hard, until the heart no longer recognises *Ma'roof* (good things) as being good & vice versa. He starts looking for angles to praise evil. Like lying leads to *fujoor* (wickedness) & that leads to hell fire. Fear Allah wherever you are, follow up a bad deed w/ a good deed & latter will wipe away the former, w/o losing its own value. From the reward of a good deed is the ability to do another good deed & vice versa }

***

Two advices by Sheikh Abdur-Razaaq Al-Badr:
1. Review these points, reherse them & look for oppurtunities in our life to apply them.
2. Be vigilant about spreading these great *fawaid* (benefits) by various means, as the one who guides others to do good is like the one who does it [Muslim:1893]. Let's try to conquer the evils & animosity b/w muslims

And through Allah alone is *tawfeeq* (success)

***
### <u>Additional Notes: </u>
**Ten ways to attain patience in times of trials & afflictions, Ibnul Qayyim:**  
This involves having knowledge of the following:   
1. The reward of having patience.
2. It wipes out sins.
3. It is pre-ordained by Allah. It has been written.
4. The rights of Allah regarding this affliction - fulfill them.
5. The active role of one's sins in the occurence of afflictions - so one should seek forgiveness from Allah.
6. Allah is pleased for it to occur.
7. This trial is a medicine given by the All-Knowing, Allah.
8. That the result of the prescribed medicine (the trial) has beneficial healing effects.
9. The trials are not there to destroy you, but to test how patient & truthful you are.
10. For every situation there is a kind of worship to be practised. Times of hardship & trials are included 
in this. Be consistent in your worship of Allah in all situations.

[Summarised from Tariq al-Hijratayn Vol2 Pg600-604]  
@UwaysT

***
