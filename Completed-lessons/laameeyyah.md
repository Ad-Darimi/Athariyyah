# Laameeyyah by Shaykh ul-Islaam Abul Abbas Taqiuddin Ahmad ibn Taymiyyah Al-Harraani رحمه الله

Using explaination of Sheikh Saaleh As-Suhaymee & Sheikh Ahmad bin Abdullah Al-Mardaawi
- Mentioned by No'maan Al-Aloosee
  - From a scholar family of Iraq.
  - A Hanafi Maturidi
  - Wrote a book to investigate the claims against Ibn Taymiyyah, made by Ahmad Al-Haythami. 
  - He supported Ibn Taymiyyah in most of the issues.

- Ascription to Ibn Taymiyyah is questioned. 
  - Some scholars affirm it, like:
    - No'maan Al-Aloosi, himself
    - Sheikh Abdus-Salaam Al-Burgis رحمه الله, who wrote a book on works of Ibn Taymiyyah & included Laameeyyah in it.
    - Sheikh Abdul-Azeez bin Naasir Ar-Rasheed, who did a commentary on Ibn Taymiyyah's other books like Aqeedatul-Waasitiyyah
    - Sheikh Saaleh As-Suhaymee
    - Sheikh Saaleh Al-Fawzan, who commented on its Sharh by Sheikh Ahmad bin Abdullah Al-Mardaawi Al-Hanbali
  - Some scholars do notaffirm it, like:
    - Sheikh Ibn Uthaymeen



***

7:25

