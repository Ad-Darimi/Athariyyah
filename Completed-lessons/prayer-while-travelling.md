# How to pray while travelling -- Tariqa of Jama' & Qasr during journey

If travel time is >4 days or travel distance is >85 km, then it counts as a a Safar


Jama' (Combine) 
Pray 2 separate Salah back to back (like you pray Dhuhr at 2pm & after finishing, pray Asr too at like 2:04pm. But pray BOTH. After you finish the Dhuhr, recite Astagfirullah 3×, but not the other Adhkār. Then start next prayer along with a new iqamah). 
(Dhuhr + Asr     Maghrib + Isha)
{Asr isn't added to Maghrib} {Fajr isn't added at all} 

Jama' Taqdeem (Early) 
Pray Dhuhr & it will include Asr in it  
Pray Maghrib & it will include Isha in it  

Jama' Takheer (Later) 
Pray Asr & it will include Dhuhr in it  
Pray Isha & it will include Maghrib in it  



Qasr
Only 2 Rakat Fardh instead of 4 rakat fardh
But pray Maghrib full




Pray in Jamaat if possible 

Praying Qasr while travelling is Mustahab. So pray Qasr & not full. NOT praying Qasr is against the Sunnah. Likewise Jama' is also important. 


Whatever from the pillars (arkaan) is possible to follow, then do it. Whichever is not possible, then it's okay. If e.g., we couldn't stop at any station, then pray standing if you can, otherwise pray qiyam standing, & rest [Ruku, sujood, jalsa] sitting. If not possible, then pray whole while sitting (with bodily movements, not just finger movements) salah (i.e., w/o qasr) but combine

If e.g., we stop at a station, then pray full salah (i.e., w/o qasr) but can combine

Taharah is a must in any way. If wudhu then be it. Otherwise tayammum

Witr of Isha & Sunnah of Fajr are both 'Sunnah Muaqqadah'. The Prophet ﷺ prayed it even in Safar, yet it isn't Fardh tho. 

Tarteeb of 5 daily Salah is WAJIB. So e.g., pray Magrib before praying Isha. If in a travel you enter a Masjid & they are praying Jamaat of Isha, then pray with them but with the niyah of Maghrib (not Isha). So pray only 3 rakaat. When Imam stands for 4th Rakaat, you still keep sitting. You now have 2 options, both are allowed. 
A) Remain sitting & wait for Imaam to reach your position. Then finish the Salah with him. 
B) Finish this Salah (you're still praying Maghrib) on your own while Imaam is still in 4th rakat (of Isha) & join Imaam (if time remains) in Isha Salah. Thus you pray both Maghrib & Isha in congregation. If you don't get time to pray Isha with him, then anyway pray the Isha on your own or in another Jamaat
If Imaam is himself a Musafir praying only 2 rakaat of Isha, then you still pray 3rd rakaat (of Magrib, duh) after Imam does taslim. 

