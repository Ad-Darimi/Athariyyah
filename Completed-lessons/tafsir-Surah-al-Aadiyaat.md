## <big> Tafsir of Surah Al-'Aadiyaat </big>
#### by Shaykh Uthaymeen

<div style='text-align: justify;'>  
<span style='color:red;'>[14/09/1441]</span>


1. ***By the (steeds) that run, with panting (breath),***
   - This is *Qasm*
   - *Aadiyaat* (meaning to run)is a *Sifat* of a *Mausoof* which isn't mentioned here
     - This *Mausoof* here are either horses or camels. 
     - Some Mufassir: Since it is a Makki Surah, so horses (for jihad) aren't refered here
     - Other (majority) Mufassirs {Correct opinion}: Mausoof here are khail (horses), so its *taqdeer* (estimation) is *Wal Khailil-Aadiyaat*(By running horses). Running horses were *maroof* even before Jihaad & their running is either upon Haqq or Baatil, depending the intent of owner.
   - *'Aadoo* = move quickly
   - *Dhabh* = sound coming from chest of horses when they're running & then breathing quickly, which gives them a further push.  

2. ***Striking sparks of fire (by their hooves),***
   - *Muriyaat* is derived from *Aora* or *Waria* meaning striking of stones w/ one another, producing spark & fire.
   - Speed of horses & their pounding on earth w/ force makes stones crush/strike each other, rising spark.  

3. ***And scouring to the raid at dawn***
   - *Mugiraat* is from *igaarat*, doing *gaarat* in morning, wh/'s best time to attack the enemy, who're sleepy at that time or yet lazy. 
   - So Allah took *qasm* of this perfect time of attacking via horse
   - Rasulullah didn't used to attack enemy at night time, but used to wait. In morning if he heard Azaan, he would wait, else he'd attack [Bukhari]

4. ***And raise the dust in clouds the while,***
   - *Naqa'* = that *ghubaar* (dust) raised due to their running

5. ***Penetrating forthwith as one into the midst (of the foe);***
   - *Wast* = middle
   - ∵ of *ghubaar*, one doesn't know he reached *wast* of *Jumoo' ul-A'daa* (group of enemies)
   - This is the usefulness & goodness of horses, at time of need, w/o being scared or tired. 

6. ***Verily! Man (disbeliever) is ungrateful to his Lord;***
   - All this *Qasm* is in general for *insaan* 
   - If man isn't given *tawfeeq* of *hidayah*, then he is ungrateful of Allah's blessings 
     - [33:72]
   - *Insaan* here specifically refers to *kaafir*
   - Kaafir refers to:
     - disbeliever
     - *kufr un-nia'mah* 
       - man becomes *kanood* (ungrateful, na shukr) to Allah, especially for some when they find themself w/ enough blessings
       - this *shukr* isn't to be merely by words, but by actions
       - Being rich & then being ungrateful has been a mighty trial for mankind

7. ***And to that fact he bears witness (by his deeds);***
   - *Inna<u>hu</u>* : Here *hu* (Dhameer of 3rd person singular) refers to:
     - Some mufassireen: Allah. Thus Allah is witness to that ungratefulness of man
     - Others: Person himself. He knows & bears witness to his *kufr*  
     The *Sawaab* (correct saying) here are both. Allah knows our heart. Man in this world may or may not accept his sins, but on *Yom al-Qiyamah*, he has to bear witness [24:24]

8. ***And verily, he is violent in the love of wealth.***
   - *Khair* here refers to wealth, as is in [2:180]
   - Men love welath, in general
     - But what is blameworthy is its extreme love [89:20]
     - What is better, since eventually all men love wealth, is that a man cares only about the wealth that is sufficient for him for his needs

9. ***Knows he not that when the contents of the graves are brought out and poured forth (all mankind is resurrected).***
   - For the life in & specifically after *qabr*, man has to make legislated use of this wealth
   - Hardwork shouldn't only be to amass it, but for benefitial use of it, for hereafter
   - W/ just one call, all will come out of their *qaboor* [36:53]

10. ***And that which is in the breasts (of men) shall be made known.***
    - What the earth hid & what our chests hid, everything will become *dhaahir* (clear & apparent. 
    - In this world, we see every matter only as what is *dhaahir* to us, like even a munafiq looks muslim
    - Actions of heart (niyyah,tawakkul, ragbah, rahbah, khawf, rajaa', etc) & everything in heart which were hidden will become apparent that day, w/ none to help us [86:9,10]
    - Thus even before we do an *'aml*, we should peek into our hearts & rectify it

11. ***Verily, that Day (i.e. the Day of Resurrection) their Lord will be Well-Acquainted with them (as to their deeds), (and will reward them for their deeds).***
    - Allah is aware of everything all the time — what happened, is happening, will happen & would've happened — but here His Knowing is specified & asociated w/ *yoma-idhin* ("that day"), as its the day of *jazaa* (recompense).   

***
</div>
