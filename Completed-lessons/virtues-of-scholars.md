# Virtues of scholars & their tremendous legacy

By Sheikh fawzan
Abu Iyaad

27th-28th Safar, 1442 | 15th-16th Oct 2020

## 9 points
  1. Imp of knowledge
  2. types of fiqh (understanding)
  3. types of knowledge that are obligatory
  4. Knowledge provides life for heart
  5. Travelling for knowedge
  6. Excellence of scholars
  7. Ecouragement from wahiayn
  8. Intent behind knowledge  - Is knowledge a goal in itself?
  9. How knowledge will be taken away from this dunya
 
- Muawiyah bin Abi Sufiyaan narrated: Rasulullah said: "Whomsoever Allah intends to show goodness, He will grant him fiqh of deen"
  - Fiqh = fahm (of deen).
    - Elements of fiqh: Person ==understands== his deen & what Allah made obligatory for him to ++perform++ & ++abandon++, then to ==act== upon that with ++*ilm*++ (knowledge), ++*A'ml*++ (action) & ++*ikhlaas*++ (sincerity). Or, knowing sharia rulings w/ specific proofs.
      - [9:33]:
        1. `guidance` = *ilmun naafi'*
        2. `religion of truth` = *amal*
    - 2 types of fiqh:
      	1. ***Fiqh al-Akbar*** : *Maa'rifatullah* (Knowledge about Allah, His Names & Attribiutes); obligations towards Him; freeing Him of deficiencies that man is described with
      	2. ***Fiqh al- Asgar*** : Ahkaam of Sharia; *Ibaadaat*; *Mu'aamalaat* (dealings w/ people); various other rulings
    - 2 types of obligatory knowledge:
      1. ***Wujoob/Fardh A'ynee*** [knowledge obligatory for all people]. No excuse for ignorance. W/o it, your deen & actions aren't upright. 
         - Consists of dhurooree knowledge like:
           - Knowledge of Aqeedah & Tawheed; knowing Allah. 
           - Types of worship (like dua, istigaatha, khushoo, etc)
           - Rulings regarding ibaadah (like obligations of wudhu, fasting, etc)
         - MbA-W in 3 principles: "Know, may Allah have mercy upon you, that its obligatory upon us to have knowledge of 4 matters..." (based on Surah Al-A'sr)
      2. ***Wujoob/Fardh Kifayee*** [collectively obligatory]. If some perform (have) it, then rest are excused from seeking it.
         - Consists of things like knowledge of inheritence, talaq, hudood etc if we're not involved in them; writing a will/legacy, ko  

### Knowledge provides life to hearts
- Like rain gives life to earth. Giving life to one's heart is more important
  - 3 situations/lands/hearts:
    1. The *mubarak* (blessed) land: When rain falls on it, it ratains the water as streams & cause plants to grow, thus aiding the people. Example - scholars. Hearts filled w/ knowledge, they understand & reflect & look-into it & derive fiqh from it & then feed it to the people. 
    2. The barren land: retains water but no herbage. Such are the scholars who memorise & preserve texts of Sharia. 
    3. The slippery barren land: Doesnt retain any water, nor has any herbage. Such are hearts that don't accept guidence that Prophets come w/. They don't memorise/retain or give-concern-to the Speech of Allah or Rasulullah in their hearts. These are ones whom Allah intends evil. 

### Travelling for knowledge
- [9:122]
  - 2 understandings of this verse:
    1. Not possible that everyone leaves & travels for knowledge. From each faction, there should rather be a group of people traveling to seek knowledge & then transmit to the people fataawa, lead them in Salaat. 
    2. Not all shoulld leave for jihad to keep learning knowledge & benefit people. 
- Al-Laalika'ee: Imam Al-Bukhari said: I met > 1000 men from ahlul-ilm, from Hejaz, Kufa, Basrah, Waasit, Baghdad, Shaam, Misr, numerous times generation after generation. The people of Al-jazeera, Shaam & Misr twice. People of Basrah 4 times. People of Hejaz numerous times in 6 years alonh w/ Muhadithoon of khurasaan.

### The excellence of scholars
- Travelling for knowledge means travelling to the Ulema. 
- [3:18]: Allah puts His testimony alongside the testimony of angels & scholars - showing the loftiness of a scholar's status
- [39:9]: Allah negated an equality b/w a jaahil & an aalim 
- [35:28]: True Ulema are those who have *khashyah* (awe) of Allah ∵ they know Him how He should be known
- "Scholars are inheriters of Prophets"
- Be careful of applying the label of *Aalim* to ignorant people or to those who merely possess dunyawi knowledge
- Abu Dardha narrated: Rasulullah said: Whoever treads out on a path to knowledge, Allah will make easy for him the path to paradise. Never do people gather together in a house from the houses of Allah, reciting the book of Allah & studying it, except *As-Sakeenah* (tranquility) descends upon them & mercy envolopes them & the angels lower their wings upon them & Allah mentions them to those around him.
  - Can physically leaving to travel & can be to get books & study/review/memorise at a mosque.  
- Rasulullah said: The virtue of an *Aa'lim* over an *Aa'bid* is like the superiority of the moon over all of the other stars
  - In another hadith He likened Scholars to the stars as well.  
- Allah has put an instict in people's heart that they repect an *Aalim*, more so than a man with wealth. [19:96]
- Ibnul-Qayyim in***Miftaah Daar As-Sa'adah*** gave >150 angles giving excellence of knowledge & its people.

### Al-Maqsoodu min al-Ilm (what is intended by knowledge)
- Its not sought in & of itself, rater for to be acted upon. 
- Knowledge can be mere knowledge of tongue or it can be knowledge of heart. Only the latter is a means to *khashyah* 


### Knowledge will be removed
- First thing to be removed is the *Khashyah* of Allah, i.e., the knowledge of the hearts. 
- Next, the scholars will die & the knowledge of the tongue will too be removed. Ignoramous will increase, who will give verdict based on jahl. Also the *qurra* (reciters) will remain.
- The Qur'an itself will be removed
