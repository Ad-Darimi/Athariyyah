## <big> أسماء الله كلها الحسنى
ASMA’ ULLAHI KULLUHA AL-HUSNA   
THE NAMES OF ALLAH ARE ALL AL-HUSNA</big>
#### Taught by Dr. Saleh As-Saleh 

***

<div style='text-align: justify;'>

<span style='color:red;'>[L1]</span>

### <big>01</big></div>

"Al-Husna"= the best, the most perfect & most beautiful. All of Allah’s Names indicate & affirm His perfect attributes. 
- His Names are Attributes too. 
  - Had they been mere *alfaaz* (words) w/o meaning, they won't be *Al-Husna*, they would’ve been Names devoid of praise & perfectness. Glorified is Allah above having Names w/o meanings, free is He from imperfections.
- [7:180] | [17:110] | [20:8] | [59:24]
- One of Allah’s Names is *"Al-Qawee*" (The Most Strong) → affirms the perfect strength of Allah. 
  - "A-Qawee" implicates that His perfect Strength isn’t preceded by weakness nor it will come to an end.   
- Another Name of Allah is "*Al-Hayy*" (The Ever-Living) 
  - Implicates perfect living, nether preceded by *A’dm* (non-existence), nor coming to end. 
  - His living has perfect qualities, like Knowledge, Power, Hearing, Seeing, etc. 
- Allah is *Al-A’leem* (The Most Knowing)
  - Implicates perfect knowledge, neither preceded by *Jahl* (ignorance), nor does it end in forgetfulness.
    - [20:52]
- So is the case with other Name of Allah: 
  - Ar-Rahman (The Most Beneficent) implicates perfect mercy of Allah.
    - Rasulullah (ﷺ) spoke about Allah’s Mercy when he saw a woman from *assabee* (women & children from prisoners of war), breastfeeding her child which she had lost earlier. He (ﷺ) said to the companions “Do you think that this lady can throw her son into the fire?”. They replied “No”. Prophet (ﷺ) said to them “Allah is more merciful to His slaves than this mother to her son” [Bukhari 8/28]. 
    - "Ar-Rahman" → mercy of Allah embraces everything.
***

### <big>02</big></div>

All of Allah’s Names are perfect, unique & qualitative Nouns. Denote the essence of Allah Himself. In this regard, they are synonymous. 
- They are qualitative ∵ they are Attributes of perfect meanings. 
- However, as Attributes, they are distinct. 
- So, *Al-Hayy* (The Ever-Living), *Al-‘Aleem* (The All-Knowledgeable), *Al-‘Azeez* (The Almighty), *As-Samee’* (The All-Hearing), *Al-Baseer* (The All-Seer), *Ar-Raheem* (The Most Merciful), etc are all Names of Allah, but each has a different meaning but aren't different entities.
- 'Allah’ denotes all Names & Attributes of Allah. 
   - One may say “Allah is Ar-Rahman or Ar-Rahman is a name of Allah” but it would be wrong to say “Allah is one of the Names of Ar-Rahman”. 
   - "Allah’" → Attribute of ‘Al-Ilaahiyyah’ (being the only true God deserving to be worshpped, glorified, loved & sought at all times) as Allah is the Lord of the *A’lameen* (All that exists). 
     - He doesn't leave His slaves w/o guidance. 
     - His Actions, Ability, Giving & Holding, Perfectly Running of affairs of world, Bestowing of favours & Inflicting harms, are specially associated w/ *"Ar-Rabb"*. 
   - Ar-Rahman doesn’t neglect His slaves.
     - Implicit in His name are the acts of sending Messengers & Revelations, bringing life to the hearts of men by making them knowledgeable of Him. 
       - It's even more profound than physical nourishment of creation. 
       - *‘Ar-Rahma’* (Mercy) is His Attribute & embraces everything.
       - This is why Allah’s *Istiwa* (Rising above His throne) most often is associated w/ *Ar-Rahman*, ∵ *Arsh* (throne) is above other creations & encompasses them & Allah’s Mercy embraces all creation.  
         - [20:5] | [7:156] | [25:59]
         - Rasulullah (ﷺ) said: When Allah completed the creation, He wrote in a Book with Him on His Arsh, “My *Rahmah* overpowers My Wrath”. 
       - ∴ this empahsises the great Attribute of *Ar-Rahmah*, the greatness of which is deduced from its link to greatest of all creation (Al-Arsh)
***

### <big>03</big></div>
*Maalik-ul-Mulk* (Possessor of The Kingdom) & *Maalik* of Day of Resurrection: 
 - All Attributes of justice, *Qabdh* & *dhaft* (decreasing & increasing the provisions), exalting some & lowering others, giving & taking, enduing honour & humiliating, judgment & compelling, etc, are denoted by His Name *Al-Maalik*. 
 - The Name Allah is linked to the Name ‘Ar-Rabb’ (The Lord), Ar-Rahman (The Most Beneficent), Al-Maalik (The Possessor) & all the Attributes denoted by His Name & He is praised in His *Ilaahiyyah* (Divinity), *Rububiyyah* (Lordship) & *Rahmaniyyah* (Mercy) & His *Mulk* (Kingdom). [1:1-3]. 

***
### <big>04</big></div>
- Each Name is perfect on its own. 
- When mentioned with other Names, perfectness on top perfectness is emphasized. 
  - E.g., "Al-A’zeez" (The Almighty), "Al-Hakeem" (The All-Wise) & "Al-A’leem" (The All-Knower). 
    - Being *Al-A’zeez*, He can do anything He wants. 
    - However He doesn’t do anything w/o *Al-Hikmah* ∴ He's *Al-Hakeem*. 
    - He is The All-Wise in His creation & commands. 
      - Both are linked to His *Might* & *Hikmah*. 
    - His actions are according to His Knowledge , Wisdom, Might & Ability & they are perfectly inseparable as all are Attributes of Him alone.    
    - [2:220] | [2:260] | [6:96] | [2:32] | [4:26]

***
### <big>05</big></div>

Some of His Names are characterized by possessing many Attributes. 
- Allah is *Al-Majeed* (The Glorious), *Al-A’zeem* (The Most Great), *As-Samad* (The Self-Sufficient). Each is characterized by specific & perfect attribute. 
  - *Al-Majeed* possesses perfect Attributes whose meanings testify to His perfect Names — The extreme Abundance, Being sufficient to fulfil all the needs of His creation — signifying magnitude, profusion & honour   
    - [85:15]
    - [11:73]
    - That’s why *"Al-Majeed"* is part of the dua of asking Allah to send abundant salaat upon Rasulullah (ﷺ).  
  - *As-Samad* is *As-Sayyed*, the Master, He's in no need of anyone. 
    - Ibn Abbas (رضي الله عنهم) s:aid “This means with perfect Mastership, perfect in His sovereignty & above Whom there is none. All creation is in need of Him & they seek His refuge”. 
  - Thus here each Name is characterized by more than one attribute </li>

***
### <big>06</big></div>

- Although some names & qualities of His creation have *ishtiraak* (lit: sharing) w/ some of Allah’s Names & Attributes, this doesn't degrade His Names which are perfect & unique to Him & that of creation are unique to them. 
- This *ishtirak* is since there's **non-specific & general** agreement in the terms, meanings & attributes b/w the Creator & creation. 
  - When specified however, each is distinguished by his own unique names & attributes. The Creator’s Names & Attributes are perfect & those of His creation are imperfect. 
- [112:4] | [19:65] | [42:11]
- Examples: 
  - Allah is *Al-Hayy* (The Ever-Living) [3:2]
    - And Allah calls some of His slaves as *Hayy* (living) [30:19]
    - Allah being *Al-Hayy* is Ever-Living, in a perfect way & this Name is unique to Him & it is'nt same as *Hayy* of  creations. 
  - Allah is *Al-A’leem* (The All-Knower) & *Al-Haleem* (The Most Forbearing). 
    - Allah also called Is’haaq & Isma’eel عليهم السلام as *A’leem* & *Haleem* respectively. [51:28] | [37:101]
  - Allah is *As-Samee* (All-Hearer) & *Al-Baseer* (Al-Seer). Man is hearer & seer yet with imperfect & limited characteristics. [11:24]
  - Allah is *Ar-Ra’oof* (The One Full of Kindness) & *Ar-Raheem* [24:20]
    - He named some of His slaves as *Ra’oof* & *Raheem*. [9:128]
    - Yet there is no comparison. The One who is full of Kindness isn’t like the one who is kind & the One who is Most Merciful isn’t like one who is merciful. 
  - Allah is *Al-Malik* (The King), & He called some of His slaves as king.
  - Same is true for Attributes of Allah. 
    - *Ilm* is an attribute of Allah [2:255] & that of Allah’s slaves [40:83]
      - But, the Knowledge of Allah can’t be encompassed [2:255].
    - Allah has a will & He Loves, Hates, Likes, Acts, Speaks & man too does so.
    - Allah described Himself as having two Hands, Face & man too so. There is no similitude or equivalence or likeness.
- *Asmaa' wa Sifaat* of Allah can’t be denied just coz of this general, non-specific *ishraak*. 
- Also they can’t be affirmed in ways that make them similar and/or equivalent to those of creation  

***
### <big>07</big></div>

Allah's *Asmaa' wa Sifaat* are *Husna* also as they came from the way of *Al-Wahi* & not from human intellect. 
 - Creation's mind is incapable of comprehending many existing matters in this world, let alone the nature of the unseen. 
 - ∴ understanding of the Salaf regarding role of intellect, wrt *Wahiyain* follows a medium course b/w two extremes of 2 schools of philosophical thought:
   1. Those who over-exaggerated the role of the mind
   2. The mystics sufis who throw away reasoning & replace it with *“zauq”* (spiritual experience), aiming to achieve so called *“fanaa”* (spiritual state of unification with Allah) & *“wusool”* (arrival to Allah) نعوذ بالله من ذالك
  - Prophets knew about Allah through *Wahi*. Had this so-called *“Kashf”* (sufi revelation) & philosophical scholastic theology, been a pre-requisite to the knowledge about Allah, then Salaf would’ve rushed to adopt them. Both are unable to comprehend what Names & Attributes Allah deserves & it's speaking about Allah w/o knowledge.
  [7:33] | [17:36]
  - Assigning Names or Attributes to Allah that He didn’t reveal is a serious injustice against Allah & a Muslim abides by the Most beautiful Names & Attributes as they are in the Wahiyain

***
### <big>08</big></div>

All of Allah's Names are from the *Muhkam* & declare meaning either via own or other evidences. Their meanings in Arabic is known. The only thing unknown is their *Kayf* (how they are) 

***
### <big>09</big></div>

In general, it's wrong to derive specific Names of Allah from certain specific Actions that came restricted in the Wahiayn. E.g., 
  - [2:15]. 
    - Allah Mocks the hypocrites (who mock believers when they meet them) & gives them increase in their wrongdoings. . 
    - But no Name of Allah is justified/derived from this Act.
    - This Mocking by Allah is His perfect Attribute, but it can't be generalised that Mocking is an Act of Allah ∵ in unrestricted use, this word has possible negative, apart from positive, meanings.
  - [4:142]
  - [3:54]
    - Allah's deception of hypocrites is perfect & good & in response to their evil deception
  - Allah tests people w/ *fitan*, yet no specific Name is derived from this Act. 
  - [32:22] [14:47]
    - *al-muntaqim* (derived from *intiqaam*) is not a name of Allah. It came in Quran in a restricted way

Thus, assigning Names to Allah that aren't in Wahiayn is a grave sin & is like speaking about Allah w/o knowledge

***
<span style='color:red;'>[L2]</span>

### <big>10</big></div>
- All Actions of Allah originate w/ perfectness 
  - ∵ they originate from His perfect Names & Attributes & they're themselves perfect. 
- His actions are linked to His Will & Decree. Allah is eternally the Creator, Provider, etc as He wills. He Speaks as He Chooses & whenever He wants.
  - He is The Creator even if doesn't create. 
  - He Possesses the Attributes of His Actions, although He may Choose not to Act. When He Speaks or Creates, it doesn't mean that Attributes of Speech or Creating, came into being after they were "inaccesible" to Him. 

*** 
### <big>11</big></div>
The Names comprise no evil.   
Rasulullah said: <u>"I respond to Your call, O Allah!, and I am obedient to your orders. All good is in Your Hands, & evil is not imputable to You. I am by You & to You. Blessed are You & Exalted"</u> [Saheeh Muslim]  
Ibn Taymiyyah said: There is no single Name of *Al-Asma' Al-Husna* (The Beautiful Names of Allah), that comprise evil. Evil is, however, mentioned relative to His object. 

***
### <big>12</big></div>
Since Allah's Names are Al-Husna, the Muslim affirms & believes in each Name & that it denotes His Essence, w/o negation, metaphoric alterations, drawing parables & similarity, suspending meaning. Muslim believes in the meaning denoted by each Name, & the relative implication of each name. So we affirm the Name, its obvious meaning & the relevant implication of each Name. ∴ Allah is  
 - *Ar-Raheem* 
   - Meaning: He has *Rahmah*
   - Implication: He is Merciful to His slaves
 - *Al-Qadeer* 
   - He has Attribute of  *Al-Qudrah*
   - He is able to do all things.
 - *Al-Ghafoor*
   - He has Attribute of *Maghfirah*
   - He forgives His slaves
 - *As-Samee'*
   - He has Attribute of *As-Sam'*
   - He Hears everything

***
### <big>13</big></div>
Implication of Allah's Names influence creation as well as hearts of the believers, making him Glorify his Creator & properly estimate Allah, His Actions, Names, Attributes. This should manifest itself on his tongues & limbs. The heart fears, loves & hopes from Allah, which promotes true dependence upon Allah, while executing His commands. Motivation of hearts towards Allah are of 3 types: *Al-Mahabbah* | *Al-Khawf* | *Ar-Rajaa'*. The strongest is *Al-Mahabbah*, which is a goal in itself, in this life & hereafter, unlike *Al-Khawf*, which'll vanish in hereafter [6:48] | [7:49] | [10:62] | [43:68] | [46:13]. The purpose of *Al-Khawf*: prevent believer from stepping out of Allah's path. *Al-Mahabbah* sets the slave moving in the path to Allah, which corelates to the strength of this *Mahabbah*. *Ar-Rajaa'* leads him through. We must be a slave to Allah & none else. 
***
***

***Ilhaad* in the Names & Attributes of Allah:**
Ilhaad = depart from the truth. Wrt Names & Attribute of Allah, it takes any of following forms:
1. Denying all or some of Names or denying the Attributes or meanings denotes by those Names. 
   - Ibn Abbas: "Ilhaad is *tadheeb* (denying)"
2. *Tashbeeh*: Drawing similarities b/w Attributes denoted by Allah's Names & those of created. [42:11] → this Ayah is a foundation in understanding the Names & Attributes of Allah, Allah Affirms 2 of His Names & Attrubutes in it → Hearing & Seeing. 
3. Designating Names to Allah that He didn't Affirm to Himself. 
   - Christains called Allah "The Father", attributing children to Allah. 
   - Philosophers called Him "The Acting Cause". 
4. Appointing names to statues, graves, etc, deriving them from Allah's Names, in order to give divine level to them. 
   - Al-laat, Al-Uzzah, Al-Manaat derived by Mushrikeen of Makkah from "Allah", "Al-Azeez", "Al-Mannan
   - Some muslims give their sons names like "Azeez", "Jabbar", "Izzah"
5. Contending w/ Allah for His Names & Attributes
   - Fir'aun in [28:38] | [79:24]
***
***
**Names of Allah aren't limited to a specific number**  
Allah has Names & Attributes not known by Angels, Prophets, etc & are only known to Him. 
Rasulullah invoked:  
اللهم إني عبدك، وابن عبدك، ابن أمتك، ناصيتي بيدك، ماض في حكمك، عدل في قضاؤك، أسألك بكل اسم هو لك سميت به نفسك، أو أنزلته في كتابك، أو علمته أحدا من خلقك، أو استأثرت به في علم الغيب عندك، أن تجعل القرآن ربيع قلبي، ونور صدري، وجلاء حزني، وذهاب همي. إلا أذهب الله عز وجل همه، وأبدله مكان حزنه فرحا.    

"O Allah! I am your slave, son of your slave, son of your female slave, my forelock is in your Hands, Your judgement is continuously being carried on me, Your divine sentence upon me is just, <u> I ask you by every Name of Yours, which You've Named Yourself, or brought doen in Your Book, or taught to someone of your creation, or you preferred for Yourself in the hidden knowledge with You </u>, that you make the great Qur'aan the spring of my heart, & the light of my chest, the -- of my sadness & the clearence of my pre-occupying concerns."  
 
∴ Rasulullah classified Names of Allah in 3 categories: 
1. Those not in Quran, but He may make them known, as He Wishes, to his angels or righteous slaves. 
2. Revealed in Quran
3. Those not known to anyone except Him, which can't be estimated by creation. 

Ibnul Qayyim said: The levels of ennumerating of Allah's Names are 3:
1. Ennumerating their number
2. Understanding the meaning of each Name
3. Invoking upon Allah by His Names [7:180]

Scholars tried to ennumerate Names of Allah from Wahiayn, such that some collected 150, while some less than 99. This variation is due to:
- Some relied only on Quran, some relied on both, some relied on even non-authentic ahadeeth e.g., *Ar-Rasheed* (The Guide to the right path), *As-Saboor* (The patient). 
- What Allah Tells about Himself is sometimes more general than being considered as His Names & Attributes. e.g., He Says He is an Enemy of kafireen, He is the Accepter of repentence, He Does whatever He Wants. Some scholars considered them as Attributes, but such *Akhbaar* don't classify as His Names.
- Must'nt be less than 99
- Dua by Allah's Names is right only when they are actually His Names. {Also, Dua to Allah isn't limited to His Names, It is justified to invoke Him by His Attributes & Actions e.g., Ya Munazzil Al-Gaif, send us the rain, Ya Muhliq az-zaalimeen, etc}
- Names that don't befit Allah aren't His true Names. e.g., Al-Burhaan (The Proof), Ad-Daee (The Caller), As-Saree' (The Swift), etc aren't Names of Allah. Al-Qadhi Abu Bakr Ibnul Arabi considered those who call upon Allah by other than His Names as *Mulhideen*. 
- Attributes of Names or Actions of Allah aren't Names of Allah. e.g., *Shadeed ul-Iqaab* (Severe in punishment), *Saree' ul-Iqaab* (Swift in punishment). These are Attributes of His Actions. 
- Names that are different in their words but have common meaning, are independant, not redundant Names. e.g., *Ar-Rahmaan*, *Ar-Raheem* aren't a single Name, though they share in the basics im meaning (Mercy). *Al-Qaadir*, *Al-Muqtadir*, *Al-Qadeer* are 3 different Names. But Names that are redundant in their words aren't different Names. e.g., *Ar-Rabb*, *Rabb ul-Mashriq*, *Rabb ul-Maghrib*, *Rabb ul-Arsh*, etc are all the same Name - *Al-Rabb*. 
- ***Al-Asma' ul-Mubaasa***: The Pre-fixed Names of Allah are true Names of Allah. Pre-Fixation itself doesn't rule them out as Names of Allah. e.g., *Ar-Rabb*, *A'lim ul-Gaib Wash-Shahadah*, *Maalik ul-Mulk*, etc are His True Names, though they are Pre-fixed. 
- *Ball* Names aren't True Names e.g., *Ad-Dhahr*, *Al-'Amad* are names of Time. [45:24] → Allah Manages the affairs of all creation, including Time. Allah says in a *Hadeeth Qudusi* that He is Time, meaning He is the creator of time, so it is created & Allah is The Creator. 
- Any Attribute, Action or creation of Allah when next to word *"Dhu"*, mostly isn't considered Name of Allah. e.g., *Dhur-Rahmah* (To Whom belongs the Mercy), *Dhul-Quwwah* (To Whom belongs *Al-Quwwah*), *Dhul-'Arsh* (To Whom belongs the *Arsh*)
- Un-Prefixed Names stated in the form of *Af'aal ul-Tafdeel* (Verbs denotinhg high preference & Most Effectiveness ) are Names of Allah. e.g., *Al-Aa'laa* (The Most High), *Al-Akram* (The Most Generous). 


***
Part 2  
35:30
</div>
