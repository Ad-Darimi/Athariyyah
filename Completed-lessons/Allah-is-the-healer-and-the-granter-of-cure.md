## <big><big> Allah is the Healer & the Granter of Cure </big></big>

### Taught by Abu Iyaad    
[SalafiSounds.com](https://www.salafisounds.com/allah-is-the-healer-and-the-granter-of-cure-by-abu-iyaad-amjad-rafiq/) `{single lecture}`
<div style='text-align: justify;'> 

***
- [7:180]
  - From the actualisation of *Tawheed* → We invoke Allah by those Names that relate to our particular needs
- From Names of Allah is *Ash-Shaafi* (The Healer), as He created life, death, health & sickness, & He created all the ways & means by which all these ways & means take place [26:78-81]
  - [Bukhari:5675, Ibn Majah:3520] {"Remove the harm, O Lord of mankind!. Heal, for indeed You are The Healer. There's no cure, except Your cure, a cure wh/ leaves not any disease behind"}
- [Ibn Majah:3438,3439] "Never did Allah send down any disease, except He sent down a cure for it."
  - Certain foundational & general means of cure → Dua, Quran itself (a healing for believers) [10:57], honey, black seed, hijamah, zam-zam, 
- Allah's *Qudrah* (Power) & *Hikmah* (wisdom) → *Al-Qadir*, *Al-Qadeer*, *Al-Hakeem*:
  - *Qudrah* → He created these cures - water, plants, minerals, etc
  - *Hikmah* → He made connections b/w things He created, placing causes & effects & tied them together. e.g., water is the cause, effect is removal of thirst
    - Such is the case w/ all of His creation, showing us His *Hikmah*, after we saw His *Qudrah*
- *At-Tabeeb* (not a Name of Allah, but a description that we can give) → [Abu Dawud:4207]
  - "The Tabeeb is the One who Created it. And you are just a *Rafeeq* (companion)"
  - Al-Bayhaqi: *At-Tabeeb* = One who knows the reality of disease & the cure, Has the power & ability for health & healing. 
  - Example: A sword/whip. Person doesnt say "the sword pierced me" or "whip lashed me". Rather we say "the swordsman pierced me". 
    - We ascribe the act to one who is using the instrument as a tool. Similarly, here Rasulullah said "The Tabeeb is Allah", and the person only facilitated & implemented the ways & means placed by Allah & all of it is in His control. He may or may not grant or delay the cure. ∴ just like we can't speak of the sword independant of swordsman, all the ways & means that Allah placed, including the physician, only come into effect ∵ of Allah & He Controls them w/ *Al-Qudrah* & *Al-Hikmah*
- Ayyub عليه السلام
  - Stories of Prophets are mentioned as a model for us & to increase our Imaan [11:120]
  - In his story is a lesson & inspiration on patience wrt disease, calamity in wealth, family. Strengthens person's heart, gives firmness. 21, 38
  - [21:83-84], [38:41-44]
    - Mufasireen & Historians: Ayyub used to be wealthy, in a Buthainah (Shaam).
    - Ibn Asaakir: Allah put him to trial & he lost all his wealth. Then he was put to trial by way of his body & every organ of his body became diseased, except his tongue & heart, which kept him alive & he used to make *adhkaar*. People began to loathe him & expelled him out of city & none remained w/ him except his wife, who lived a good life earlier due to him. She used to work during day & was patient, even when they lost their offsprings. When people found that she's Ayyub's wife, they stopped employing her for fear of bad omen in her. She then sold her hair braids & sold to some rich women. Ayyub found this out & made this dua [21:83] & Allah accepted his dua [38:42]. So he struk the ground, which released water, which cured his outer diseases after he washed his body. He went to another place & did same w/ another spring & he was ordered to drink this water w/ heaed him internally. Allah restored his wealth by golden locusts [Bukhari:279, 3391 7493] {"but i am still in need of your blessings" → shows that a person can never be free of being in need of Allah's blessings}. 
- Lessons:
  - Trial only increased Ayyub in *Sabr*, expecting reward from Allah, in *dhikr*, gratefulness to Allah & he never complained, while this lasted for 18 years(!). As-Suddhi: The disaese was so severe that his flesh was hanging & disappeared, withered away. 
  - Trials for prophets [Tirmidhi:2398, Ibn Majah:4023], [Ibn Majah:4024]
  - Example of Ayyub is a mercy & reminder for others who're ill. Allah decreed this upon His servant, so we could take a lesson & be hopeful & patient.
  - Person doesn't remain patient after calamity, anticipating rewards from Allah; except that Allah rewards him w/ what's better. [Muslim:918].
  - Lesson for muslim wives towards husbands - enduring patience in good & bad times - poverty, sickness. She recognised the favours of her husband. [Tirmidhi:1159]
  - If you fear Allah, the He will give you a *makhraj* (way out/escape) [65:2]
  - Believer is optimistic, w/ tawakul 

***
</div>
