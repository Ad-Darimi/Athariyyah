﻿## <big> Rushing to good deeds is from character of the Prophets & Messengers </big>

### Taught by Abu Iyaad  
[SalafiSounds.com](https://www.salafisounds.com/rushing-to-good-deeds-is-from-the-character-of-the-prophets-and-messengers-by-abu-iyaad/) `{single lecture}`

<div style='text-align: justify;'>

- From the greatest of the qualities of Prophets
- Surah Al-Anbiya → Allah metions stories of 14-15 messengers(عليهم السلام) in brief. <check who>
  - Moosaa | Harun | Ibrahim | Lut | Dawud | Sulayman | Ayyub (Job) | Ismaeel | Idrees | Dhun Noon (Jonah/Yunus) | Zakariyyah | Yahya | Eesa
  - Imam As-Sa'adi: 21:90 → Allah mentions 3 traits of Zakariyyah, his wife & Yahya, but applies to all Prophets in general:
     - used to race to do good deeds. [Meaning:
       - did so in their most virtuous times
       - completed these deeds in a desirable manner
       - didn't leave any excellence / virtue which they're able to do at every oppurtunity.]
     - used to call on Allah w/ hope (aspiration) & fear (apprehension). [Meaning:
       - ask for things they aspire for in both lifes
       - seek refuge from affairs that they feared (harmful in both lifes)
       - not heedless & engaged in pastime.]
     - used to be filled with *Khashyah* (awe / humble themselves before Allah) [Meaning:
       - from the perfection of their knowledege regarding Allah.]
- Allah mentions "racing to do good deeds" many times in different words/ways like:
  - <u>*Al-Musaara'a*</u>
    - not looking at anyone else
    - rushing purely by yourself
  - <u>*Al-Musaabaqa*</u>
    - racing/rushing in relation to somebody else, out of competition
    - motivated by their presence 
  - <u>*Al-Munaafasa*</u>
    - compete with others in apparent or secret 
  - <u>*Al-Mubaadara*</u>
    - doing things in your own accord, not in realtion to others
- *Al-Musaara'a* & *Al-Mubaadara* are a greater quality than *Al-Musaabaqa* & *Al-Munaafasa*
  - Some people are only motivated when seeing others doing that deed too → Maybe, if wasn't for the other people, that person wouldn't have worked 
  - In case of former , person has *"U'loow Al-Himmah"* (lofty intentions/goals/ideals)
    - Messengers were *"Ulul Azm"* (people of firm determination) (46:35)
    - Thought about important/serious & loftiest of things → gives you firm determination 
    - Hadith: "Whoever *hamma* (thought/intended) to do a good deed, but didn't do it, then Allah will write for him a complete good deed". 
    - Hadith: "Whoever asks Allah for martyrdom with truthfulness, Allah will make him to reach the level of martyrs, even if he dies on his bed". → Lofty intentions/goals
    - Having noble goals → lofty aspirations → trmendous rewards → raised level.
    - Umar said: "Do not betlittle your *himmah* (aspirations), for indeed, i haven't seen a person left away from noble things, except one with *sigaril-himam* (minor/mundane concerns) {as it occupies him away from more noble things}"
- In the Sunnah of Messenger:
  - Abu Hurayrah (رضي الله عنه) said: Rasulullah(ﷺ) said: "*Baadiroo* (hasten) {ralated to *Al-Mubaadara*} towards good deeds, before there comes trials, which are the portion of a dark night, in which, a man will arrive morning as believer & reach evening as disbeliever, & reach evening as believer & reach morning as disbeliever. One of them will sell his religion for a portion of this world". {Saheeh Muslim}
    - ∴ trials can come at anytime, & we must hasten to do good, whenever we get the chance.
    - Many dont benefit from their eemaan. They don't acquire good deeds, which are the fruits of Eemaan, binding to eemaan. When big trials cone to him, he would sell his eemaan for a lowly price. His neglect for years in not fortifying his eeman with acting on it, caused his eeman to be stagnant & weak. 
      - ∴ *Al-Mubaadara* = important
  - Uqbah (رضي الله عنه) narrated: Once I prayed Asr behind Rasulullah(ﷺ) in Madinah. After he made tasleem, he stood *Musri'an* (hastily) & went through the people, making sure not to walk over them, until he reached the appartment of one of his wives. So people were surprised at how quickly Rusulullah went away. When he came back, he said: I remembered, that I had something to donate & I disliked  should keep hold of them. So I went & ordered that they should be divided & distributed.
  - Ibn Abbas (رضي الله عنهما) said: Rusulullah(ﷺ) was the most generous of the people, & he was most generous in Ramadhan, when Jibreel would meet him every night & revise the Quran with him. And when Rusulullah(ﷺ) would meet Jibreel, he was more abundant in goodness, than a pleasant wind that is sent. Good deeds would flow from him. 
- Ibrahim & Ismaeel  (عليهم السلام) obedience→ 37:102
  - Ismaeel (عليه السلام) hastened to submission & patience.
- Moosa (عليه السلام) hastened to be in isolation with his Lord & worship Him → 20:83-84
- 23:60-61
- 57:21
- 2:148
- 3:114
- 3:133
  - Means rush to follow Allah & His Messenger, accept Islam, fulfil the obligations, rush with sincerity, make *hijrah* (emmigration) from the land where you can't express Islam, to do *jihaad* (struggle) in the path of Allah, to recite the *takbeeratul-ihraam* (opening takbeer) of slaaah, every act of obedience, to hasten to repentence from Allah. ∴ applies w/ different meanings to different people- to believer, to kaafir, to one living in land of kufaar, to one who's to fulfil obligations, to a repenter, to an *'abid*, etc 
- Rusulullah (ﷺ) said (during a battle): "Stand/race & towards a paradise, whose breadth is of the heavens & the earth". So a Sahabi Umair bin hammam Al-Ansari (رضي الله عنه) stood up & was surprised & confirmed about this Paradise. Rusulullah (ﷺ) said "Yes". So he said *bakh! bakh!* (a statement of amazement & joy, like "wow"). He said this out of *rajaa* (hope) to be among its inhabitants. So Rusulullah said "Indeed you are from its people" [Saheeh Muslim]
  - So seeing the *u'loow al himmah* of this Sahabi, Rusulullah(ﷺ) made dua for him.  
  - This statement of Rusulullah (ﷺ) didn't let him stay around & only rejoice. He had with him dates & he started to eat them. So he thought to himself that if he dies, he won't need those dates. So he threw them on ground & said "This is going to be a long life" & rushed into the army & fought until he was killed. This is *azm* (firm determination), after having *u'loow al-himmah*. His concern wasn't the food, drink & pleasure he was going to have, like animals {who are only concerned about these things of mere survival}. 
  - ∴ one shouldn't be relying only on people & be complacent. One shouldn't be dependent for his motivation from good deeds of others only. This should come from within - *u'loow al-himmah*, & it'll bring *azm* into him. All this comes with *yaqeen* (certainty), being firm & lofty aspiartion for guidance & safety. 

***

</div>
***
