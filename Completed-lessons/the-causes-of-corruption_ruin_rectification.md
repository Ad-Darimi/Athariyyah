## <big> Ibn Taymiyyah & Ibn Al-Qayyim On The Causes of Corruption, Ruin & Rectification </big>

### Taught by Abu Iyaad  
[SalafiSounds.com](https://www.salafisounds.com/ibn-taymiyyah-ibn-al-qayyim-on-the-causes-of-corruption-ruin-rectification-lecture-by-abu-iyaad-masjid-abi-hurairah-conference/) `{single lecture}`

<div style='text-align: justify;'>  

***
- [2:126] | [14:35] | [104:3-4]
  - 2 key things that an individual, a nation, a society is concerned w/ : 
     1. *Rizq*
     2. *Aman*
- [16:112]
  - how a society fluctuates from safety & prosperity to its opposite?
  - Muslim countries today: complaining about economy & safety
  - Guidance & methodology of Messengers is the most compete & wise. 
  - Different sects, groups, parties, persons, etc took wrong aaproach & went to kuffar ideas & philosophies & ruined the situation.
- [13:11] 
- [30:41]
  - Imam Qurtubi: Views about corruption refered here: 
    - Qatadah said: *Al-Fasaadu Ash-Shirk, wa huwa A'dham ul-fasaad*. 
    - Ibn Abbas, Ikrimah, Mujaahid said: Corruption on land is the kiling of *Qabeel* of *Habeel* (the first murder). And in the sea, it is the king from Surah Al-Kahf, who captured the ships. 
    - Itis a famine, w/ no herbage & *baakah* disappears
    - Ibn Abbas: The *barakah* decreases due to actions of people. They are made to see this decrease so they are able to repent & link the decrease in *barakah* & their own actions
    - It is economic depression. 
    - It is disobediece, roads being cut-off (one's fearful of travel), & occurence of *dhulm*. 
  - Loss of *barakah* in provisions can be: 
    - loss of taste/nutrition in fruits. 
    - lack of jobs
    - fear of crime
  - The Salaf connected the actions of servants w/ these symptoms manifest in society
  - Ibnul Qayyim: 
    - Apply this verse to the affairs taking place in the world. Compare b/w the reality that you see & what this Ayah is telling, & you'll see how calamities always appear, whether in vegeration, etc. They come in succession & are tied. 
    - Every time people bring about *dhulm* & *fujoor*, Allah brings out a calamity or a flaw in their food, harvest, environment, streams, air, their bodies, physical form, apearences, qualities, etc which are necessisated & demanded by the actions they commit. 
    - If you take a grain of wheat today, you will realise that it used to be much larger than it is today, just like the blessing in that grain used to be more.
    - Imam Ahmad narrated: They found a treasure chest from the era of Bani Ummayyah. In it was a grain of wheat of the size of date-stone & it was written on it "This is what used to grow in the days of *adl*"
    - Most of diseases in society are a remanant / after-effect / remains of a punishment on previous nation. Then they were allowed to remain for anyone doing the same deeds later. This is a just judgement & ruling. 
    - Plague is a remanent of the the punishment on Banee Israeel. 
    - Likewise, Allah sent wind for 7 nights for nations like Aad
    - Allah has tied to actions of every righteous or wicked person,  certain effects, in this worlds, such that they can't be disconnected. 
      - Such rules set by Allah, like drinking water removes thirst, are known by experience, help us understand the world
      - But some other rules aren't known by our imited experience. Allah has Explained some of them in Wahiayn
    - When people withold being kind to others, giving zakat & charity, Allah has made this a cause for rain to be witheld → real cause of drought & famine. 
    - When people oppress poor & cheat in weights & measures & a strong person devours a weak person → Causes tyranny of ruler, who are just the physical form of deeds of his subject, and what they deserve by the *'Adl* & *Hikmah* of Allah. 
    - Allah Shows the physical appearence of the wrong doings of people by way of famine, enemy overpowering them, oppressive rulers, general diseases, people being stressed/grieved/anxious, by witholding *barakah*, by inciting shayateen against them to lead them further astray... All in order to establish/prove His Word over them & that every individual, by Qadr of Allah, will be lead to his eventual destination. 
    - Thus a man of intelligence realises Justice of Allah, and that Messengers are on the path of deliverence & opposers on path of destruction. 
- <u> Once, Rasulullah turned to Muhajireen & explained that there will be 5 things, I seek refuge that you'll be put to trial w/ them or reach them. they are:
    1. Never does *fahishah* openly manifest in people, except they except plagues & diseases appear, which their forefathers didn't know
    2. Never do they cheat in weights & measures except they are taken  over by years of hardship, scarcity of provision & tyrranical ruler.
    3. Never do they withold the *zakaat*, except the rain will be withheld. Had it not been for the animals, they would've never been given rain from sky
    4. Never do they break the covenent of Allah & His Messenger, excpet that Allah empowers enemes over them which take some of what they used to possess
    5. Never do their leaders not rule by the Book of Allah, except that Allah makes the infighting b/w them to be severe 
    </u>
 - Ibn Taymiyyah: {On why did the first ruling dynasty - Bani Ummayyah, come to an end} The last of their Khulafa, Marwaan bin Muhammad Al-Ja'di. Al-Ja'adi is an ascription to Ja'ad bin Dirham, whom he used to support, the misfortune of which returned back to him. When innovations appear in a nation, then Allah seeks revenge from whoever opposes the Messengers. Thus Christian Rome came out to Shaam & Jazeerah & took over the coastal regions, bit by bit, until they took over the *Bayt Al-Maqdis* at end of 4 Century AH. People had broken the covenant of Allah. Bidah of many kinds was manifested until outright disbelief & shirk appeared in North Egypt by Ubaydiyyah. So, after a period, Romans beseiged Damascus & people of Shaam were in worst of situations b/w christains on one side & Batiniyyah (outwardly Shia, inwardly syncretists, fire-worshippers, persian pagans, hellenistic philosophers) from Egypt, Bahrain, North Iran. In East, when they were established upon Islam, they were aided against pagans (Turks, Hindus, Chinese), but when *bidah*, *ilhad*, *nifaq* & *fujoor* appeared, the kuffar were unleashed against them - Tartars, Mongols via Iran & Black sea. 
    - This continued after the era of Sheikhul Islam, like imperialism. 
 - We've to relate this to ourselves, our families & nations, why fear & poverty is among us. 
 - People of Tawheed & Sunnah are true rectifiers of individual & society, with detailed knowledge & insight. Compared to others dawah, like MB, HT, etc upon misguidance. 
 - E.g., It was thought that direct cause for heart disease is cholestrol molecule, that clogs artries. So medicines were developed to keeping this in mind. It turned out that its not actual cause, rather symptom of something else, which is the inflamation of veins due to diet & stress, causing a response from immune system & cholestrol was just a bystander. They had to alter 3-4 decades of progress & they distinguished b/w good & bad fats, HDL & LDL cholestrol.
   - Thus, they couldn't distinguish b/w symptom & cause & ruined people's lives. 
 - Ibnul Qayyim: Allah made the rulers of people, of same species of the actions of people... It isn't from Divine Wisdom, that evil people are made to rule, except on their likes. The *Hikmah* of Allah refuses that in our time, we are ruled over by the likes of Muawiyyah & Umar bin Abdul-Azeez, let alone by Abu Bakr & Umar.
 - Muadh bin Jabal said: Verily, the appointing of *Ameer* is from the affairs of Allah, so whoever reviles the *ameer*, then he is reviling the affair of Allah. 
 - Muhammad At-Tartooshi: I never seized listening to making this statement: "*A'maalukum Umaalukum* (Your own actions are your workers/agents) & as you behave, you shall be ruled likewise". I never understood this statement, until i grasped its meaning from [6:129]. Whatever you're complaining about in your time, the its your own deeds that corrupted it for you.
 - Abdul-Malik bin Marwaan, from Banu Umayyah, said: O Subjects! you haven't dealt justly w/ us. You want from us the *seerah* of Abu Bakr & Umar, while you yourselves are not like them amog yourselves. 
 - Ibn Taymiyyah: 60 years of tyrannical rule is better than a single night w/o a ruler(i/e., living in chaos & confusion, w/o organisation)


***
</div>
