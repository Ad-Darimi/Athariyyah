## <big> “Fear Allah wherever you are…” </big> 
### Taught by Abu Iyaad   
[SalafiSounds.com](https://www.salafisounds.com/fear-allah-wherever-you-are-by-abu-iyaad/) `{single lecture}`

<div style='text-align: justify;'> 

10 - 28 May 2020
***
Allah sent Rasulullah w/ *Jawaami' Al-kalim* (comprehensiveness & conciseness in words/speech) & Allah made him unique, gave him the most expressing wisdom, such that he ﷺ  would combine various sciences in a single sentence or half of it. From that is "اتَّقِ اللَّهَ حَيْثُمَا كُنْتَ وأَتْبِعِ السَّيِّئَةَ الْحسنةَ تَمْحُهَا، وخَالقِ النَّاسَ بخُلُقٍ حَسَنٍ" [Tirmidhi:1987] "Have taqwa of Allah wherever you may be, & follow up an evil deed w/ a good deed & it will wipe it out & behave w/ people w/ excellent manners". Rasulullah combined b/w the right of Allah & rights of servants, in simple wording 
***
==<big> "اتَّقِ اللَّهَ حَيْثُمَا كُنْتَ</big>==  
  - Right of Allah → Taqwa (He should be feared in a manner He truly deserves).
  - General meaning of Taqwa: One places, b/w himself & whatever he fears & takes caution from, a barrier or means of protection that'll protect him from that which he fears. 
    - ∴ Taqwa of Allah means: Servant puts b/w himself & what he fears of Allah's anger, dislike & punishment, a means of safety. This "means" is that he does whatever Allah commanded (obeys Him) & abstains from disobedience. Allah commands in Quran.
      - taqwa of Himself (He is worthy of being feared, venerated, glorified in His servants hearts) [5:3], [6:72]
      - taqwa of fire, be cautious of it [2:24], [3:131]
      - taqwa of day of judgement [2:48], [2:123]
  - Whenever Rasulullah sent a group (for an expedition, etc), he would command its leader to have Taqwa of Allah in himself & have goodness wrt those w/ him.
  - When Rasulullah gave khutbah in his farewell pilgrimage, he advised w/ taqwa of Allah
  - Description of Taqwa
    - Ibn Abbas: Muttaqoon are those cautious wrt Allah & His punishment. 
    - Hassan Al-Basri: Muttaqoon are those cautious of that which Allah made haraam on them & they fulfil that which Allah obligated on them
    - Umar bin Abdul-Azeez: Taqwa isn't to simply fast during day & fast during night, while falling to whatever one falls into in b/w that. Rather Taqwa of Allah is to abandon what Allah made haram & fulfil what He obligated. So anyone who is granted goodness after this, then it is goodness on top of goodness
    - Talq bin Habib: Taqwa is that you (i) act in obedience of Allah & not disobedience, (ii) upon a light(knowledge, insight, evidence) from Allah, (iii)hoping in His reward & fearing His punishment
    - Maymoon bin Mehran: Al-Muttaqee is more intense & severe in calling himself to account (in terms of his deeds, thoughts, statements), than a covetous business partner will hold his partner to account.
    - Ibn Mas'ud on [3:102]: It means that Allah be obeyed & not disobeyed. That He be remembered & not forgotten. He be thanked (being grateful) & not disbelieved.
  - A Muslim should have taqwa wrt his dealings all his life - his self, worshipping Allah truely, family, responsibilities, employment / business, 
***
==<big>وأَتْبِعِ السَّيِّئَةَ الْحسنةَ تَمْحُهَا،</big>==  
  - A servant always falls short, but Allah gave a means to make up for this shortcoming in fulfuling the right of this taqwa, from His Mercy. This is by following it w/ good deeds. [11:14]. From the best *hasanah* that one can do here is sicere repentence
    - [3:135] → *Dhakarullah* means to remember Allah's Greatness, severity of His Reach, His Vengeance, His punishment & this will make remind them to return & seek His forgiveness
  - Servant should be hasty & not delay seeking repentence ∵ one of the signs of acceptance of repentance is that it occurs before one's death & we don't know when we'll die [4:17-18].
***
==<big>وخَالقِ النَّاسَ بخُلُقٍ حَسَنٍ</big>==    
  - From the characteristics of taqwa → behaving well w/ people & w/o it, taqwa isn't complete/whole.
  - Rights of servants also comes under taqwa, along w/ Rights of Allah
  - In it comes: You don't harm people, pardon their shortcomings & their harms to you, behave w/ them w/ *Al-Ihsaan* (benevolence), meet them w/ pleasant face, kind words & behave w/ each of them in a way apt & befitting him [3:133-134]
***

- At-Taqwa has been associated w/ many things like paradise, *rizq*, ease of affairs & *makhraj* from hardship, benefitial knowledge, guarantee of one's reward, etc [39:73]|[3:133]|[65:2-3]
- Umar to his son: Indeed I advice you w/ Taqwa of Allah, for indeed one who has Taqwa of Allah, then He will protect him, & whoever loans to Allah a goodly loan, then He will reward him, whoever is grateful to Allah, then Allah will give him increase
- Ali to a man: I advice you w/ Taqwa of Allah, The One whom you will inevitably meet & The One besides whom there's no eventual return & He is the One who owns & controls the world & the hereafter
- Umar bin Abdul-Azeez to a man: I advice you w/ Taqwa of Allah, that wh/ Allah doesn't accept besides it & he doesn't show mercy except to those who possess it, nor does He reward except upon/for it. Indeed those who advice w/ taqwa are many but those who actually act upon it are few. 

***
</div>
