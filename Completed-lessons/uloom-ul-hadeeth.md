# Uloom ul-Hadith (Science of Hadith)

Taught by: Shaykh Abul-Abbas Moosaa Richardson

<div style='text-align: justify;'>

## Terms 
- *Rivayah*: the narration  
- *Dhirayah*: understanding and technical aspects dissecting it into matn and sanad, fiqh of hadith

- *Mutawatir* → evolves to *Mutawfiq* if Daarqutni hasn't criticized it.
- *Ahad* narrated by small group of people and have to be scrutinized and studied well

- *Nasqh*: A ruling that is abrogated. 
  - The fiqh ruling that came later are the ones followed. 
  - <u>Abrogation is only for fiqh and not in case of Aqeedah.</u> e.g., doing a new wudhu after eating food cooked on fire was abrogated. 
***

## Great researchers of Hadith: 
1. Daarqutni (master of *illal*, critic of *sahihaiyn*),
2. Ibn Hajar (Authored *Fath al Bari*, an exegenisis of *Sahih Al-Bukhari*) (Taqreeb wa Tahdeeb by Ibn Hajar. A book about details of all the narrators in the 6 books of hadith),
3. Imam Ahmad,
4. Ibn Abi Hatim,
5. Ibn Salah (Anwa Ul Uloom ul Hadith{Miqadimah of Ibn Salah}– founder of Hadith science), 
6. Shu'ba Ibn Hajjaj
*** 

## Conditions for hadith to be Saheeh:  
1) *Adaala* (Uprightness) : Narrator has to be *adl* <check this>
   - Should be a Muslim
   - Over the age of puberty (the age of hearing can be accepted by some scholars if he heard at young age but has to be older at least at the time of narrating Hadith)
   - Should be *Aaqil*(sane)
   - Shouldn't be *fasiq*(committer Major sin or minor sin consistently) {openly at the very least}
   - Shouldn't oppose the *Murooa* (personal integrity) {related to culture of people, not the religion} of his people 
     - e.g., shouldn't be culturally a weirdo, like wearing bathrobe in public outside home is religiously accepted but culturally insane/crazy. 
     - Even eating ice cream at one time was considered weird but not today. Rejection due to violation of Muroo'a has to be acceptable according to Sharia like rejection of violation of *Murooa* because of attending Masjid with common people
   - [A Sahabi is always accepted as *Thiqaah*. Conditions of being considered a Sahabi:
      1. Met Prophet at least once
      2. Believed in the call of Messenger
      3. Died as Muslim ]

2) *Dhabt* (Precision in narration) : Narrator has to be *dhaabit*
   - Types of Dabht:
     1. Dabht al qitab (precise via book/manuscript)
        - In this case muhadith check if the person carried his book while narrating
     2. Dhabt al Sadr (precise via memory)
        - Such kind of *Dhaabit* has advantage of possibly being precise in both memory & qitab. 
        - Though some can be dhabt via memory but not via book like illiterate or blind   
{Thiqqa-A narrator having both *Dhabt* & *Adala*}

3) *Sanad* (Chain) {plural: *Isnaad*} has to be connected like there shouldn't be any unnamed person (*ittisaal fil sanad*)

4) There should be no *shuthuud* (contradiction by a sanad with stronger narrators or a hadith which is narrated by an 'alone person'). Such a weak sanad is called *Shaadh*

5) There shouldn't be *Illah* (hidden defects) in the hadith. After studying wide range of books, the *Muhaditheen* find minute details like realising that if the narrator uses such & such word, it means something like he took this information from a book & that book might be unreliable, & other type of 'hard to find' defects
***

## Conditions for hadith to be Hassan:
All characteristics of Saheeh Hadith except:
  - Dhabht of narrator is little less (*khafeef ad-dhabt*)
     - Narrator isn't considered *thiqqah* but might be *sadooq*(little lower level than thiqqah)
**Note**:
- {Adl is discreet - A person is either Adl or not. But dhabt is varying & non discreet}
- {If the mistakes became more (called wa'), scrutiny increases}.
***

## Misc

- {3rd century AH scholars, like Imam Bukhari, included Hassan Hadith in his Sahih because for him Sahih in general meant "accepted"}
- If a person becomes liar, <u>all</u> narrations with him being in chain are rejected. 
   - Some scholars accepted him if he later repented but most didn't.
- Imam Al-Baghawi considered Sahih Bukhari & Sahih Muslim as Sahih & other 4 (Sunan An-Nasai, Sunan Ibn Majah, Abu Dawood, Jami Tirmidhi) as Hassan. But this was wrong.
   - Such terminology & categorization of isnaad is called *Mustalah*
- *"Hassan Sahih"* (first used by Imam Tirmidhi & later by others) means of them (as explained by Ibn Hajar) 
   - *Hasan wa sahih*: It is Sahih in one chain & Hassan in another () 
   - *Hassan aw Sahih*: It came by one chain only, so it is either sahih or hassan 
- *Hassan li Gairihee*: A dhaeef hadith that becomes Hassan by way of support of other many, (might even be just 2 daeef coming from different angles) daeef ahadith
- *Hassan Ghareeb*: Hassan and came only him one source 
- *Sahih Li Gairihi*: A Hassan hadith that becomes Sahih because it is supported by other Hassan ahadith. 
- *As-Sihah as Sittah* (The 6 sahih books) {more accurately: Kutub as Sittah as they all aren't 100% sahih but are just the Cannonised books}     - according to some scholars: 
       - Bukhari, Muslim, İbn Khuzaymah, Ibn Hibban, 
       - Al Hadith al Mukhtara of Al Maqdisi
       - Al Mustadraq of Al Hakim 
       - Al Muwattah
   - And according to other:
       - Bukhari, Muslim, Abu Dawood, Ibn Majah, An Nasai, Tirmidhi (aka *Ummahaat as-sittah*) 
- *Daeef Hadith* 
  - Doesn't meet requirements of Sahih or of Hassan
  - Has *da'af* (weakness) in it, like:
    - weakness of narrator(s)
    - weakness of broken chain
    - weakness of adalah (like narrator(s) being fasiq), 
    - weakness of *Mukhalafa* (contradicting a stronger narration)
- *Daeef Jiddan* (Extremely Daeef)
  - Given no importance/consideration & isn't used to strengthen another Hadith. 
  - These aren't even written, unlike daeef which are at least written & possibly might be right or wrong. But Daeef Jiddan isn't even at that level. 
- *Mawdhu* (fabricated)
  - A Hadith narrated by 
     - A *Wathda'* (fabricator) or 
     - A *Kadhaab* (liar), 
     - Someone who stole the Hadith from other person, even if the Hadith was correct in hands of a right person. 
   - Not accepted even if the meaning is good & done with good intention.   
{*"Al Mauduaa"*t by ibn al-Jawzi is a book about Maudhu ahadith by subject}. 
 
- *Matrooq*: An Abandoned narrator
***

## Break in chain

#### <u>In terms of no. of missing links:</u> 
1. *Munqati'*: 
   - Those which have an *inqitaa'* (break in chain).
   - One link in chain-or-more than 1 but not in a row missing at a point) 
   - Two usages:
     - In general: Applies to any hadith with a break in it i.e., any Narrator who didn't hear from the person from whom he's narrating, anywhere in the chain be it the end or middle or beginning. 
     - In specific: A hadith that has only 1 break in the chain, it isn't successive, just one person is missing. 
    - More than 1 break in chain but not successive, maybe a missing link at one place then another missing link at other place, & so on. That is, there's no place with two continuous missing links
2. Mu'dal(More than 1 missing simultaneously) 
A hadith with more than one missing links at a single time, simultaneously. This it is severely broken chain. 


#### <u>In terms of point of missing link</u>
1. *Mursal*: 
   - Two types:
     1) Any break in chain, thus including *munqati'*. That is why these both terms (mursal & munqati') can be used interchangeably
     2) Tabi'e doesn't mention the Sahaba in the Hadith but directly the Prophet ﷺ thus there is a break in chain at top
       - Tabi'e can be of four types:
          1. *Tabi'e kabeer* (Major Tabi'e): met a lot of companions(100+) e.g, A'taa Ibn abi Rabah(200 companions), Sayed ibn Musayb
          2. *Tabi'e wasat* (Middle Tabi'e): met 10-20 companions(loosely) e.g., Hassan Al Basree(85 companions), Muhammad ibn Sireen  
          3. *Tabi'e wasat II* (lesser than wasat): Most of their narrations are from other Tabi'e & not Sahaba but met more sahaba than sageer) e.g., Zuhree, Qatadah
          4. *Tabi'e sageer* (lesser Tabi'e): Met 1-5 companions(loosely) e.g., Al A'mash
       - A hadith narrated by Tabi'e who directly narrates from Prophet ﷺ  can be of two types:
         1) He narrated from Sahabi but ommited Sahabi's name
         2) He narrated from another Tabi'e who in turn heard the Hadith from any Sahaba.  
         {In case 1, there's no problem in the Hadith as all the Sahaba are thiqqah, so no matter which Sahabi narrated it, we have no need of checking the authenticity of the Sahaba link in the chain. However in case 2, there is a chance of the Tabi'e being non-thiqqah}.
         {Last Tabi'e died around year 220H}.
   - Mursal narration missing the link between Prophet ﷺ & tabi'e may or may not be acceptable. It depends on the tabi'e. If it is a major tabi'e who mostly narrates from Sahaba, his narrations lacking the name of the Sahaba might be accepted by many scholars. 
   - In all other cases, Mursal ahadith are not accepted.   

{ *Mu'alaq* (opposite of Mursal):  
 - When there is a break in the bottom {top is Prophet ﷺ and bottom is the final narrator} of chain (the other end of Sahabi) or in other words, missing at the point of collection/narration e.g., if Imam Bukhari narrates from a missing link, who narrates from someone above him and so on until the tabi'e narrates from Sahaba who narrated from Prophet ﷺ
 - This is common in some places in Sahih Bukhari, especially in titles of chapters where he narrates a hadith with no chain, directly from Prophet ﷺ. These are weak in such sense, but Ibn Hajar in his book connected many of these hadith and some of them were Sahih, some Hassan and some daeef.  }

2. *Mubham*: anonymously mentioned/narrator is mentioned without name is Mubham
  - *Mubham as Sahaba*: a narration in which it is said to be from a Sahaba without naming him 

***

## Tadlees
{There are 2 ways a person can narrate to another person:
1. Either he literally means that he was present when the person narrated it 
2. Or he speaks in a sense that could mean that he heard it indirectly.}
- *Tasveeh*
  - No possible chance of a middle man between the one who first narrated & the one who mentions it ahead as the next Narrator) 
  - There are ways of narrating a hadith (by way of using words that directly mean that the person definitely heard it himself) like: "Haddathana", "Akhbarana", "Sami'tahu yaqool", "Amna alaina", etc (which translate to "He narrated to us", "We took dictation from him", "I heard him saying on the Mimbar", etc). 
- *'An 'ana*
  - Using the word 'an ("On the authority of")
  - When, on the other hand a person narrates with words that sound as if there is possibility of him not hearing it directly from the main narrator like " 'An Fulaan" 
- *Mudallis*
  - A person who narrates using *'An 'Ana*. 
  - Some narrators intentionally use it every time they consider the middleman as weak & thus try to remove his weakness from the narration. 
  - Such a person who does it intentionally is considered a *Kaddhab* (liar).
  - Tadlees of a Mudallis isn't acceptable. Anytime a person is found to be an intentional Mudallis, his Hadith are considered weak & with a broken chain. 
  - Although some might do it unintentionally.
- If a Narrator is mentioned as *Sheikh* or *Rajulun Salih* or *Aabid*, etc instead of Thiqqa, he is considered as possibly not Thiqqa coz a reliable Narrator would have been mentioned as Thiqqa. Thus such narrations are generally weak 
- *Marfu'* 
  - Raised upto Prophet (ﷺ) 
  - Links up at the top to the Prophet (ﷺ). 
  - It may or may not directly mention the Prophet's saying, but in reality it is related to the Prophet (ﷺ). 
  - It can be Sahih or not. 
-*Mawqoof*
  - Same but with a Sahabi
  - It too can be Sahih or not.
- *Maqtu'*/*Mawqoof ala fulaan Tab'i*
  - Same but for Tab'i 
  - Can be Sahih or not 
***

## Uluuw(Highness/loftiness) of a Hadith

- A'ali
  - A high or lofty hadith because of any of the following reasons:
    - A short chained Hadith of just 3-4 people 
    - A Chain of narrations having all the links as great people or Imaams of Ummah. 
       - e.g., Imam Malik narrating from Nafi'(The freed slave of Ibn Umar), narrating from Ibn Umar (رضي الله عنه) who narrated from Prophet (ﷺ). 
          - This particular chain is considered as the **"Golden chain"** & the most cleanest chain of narrations in Islam. 
    - There can be chains of narrations with even 10 narrators but still be *A'ali*. 
      - This is possible in case the chain is narrated on a large time scale. 
        - Example, a hadith reported in 833H with even 10  narrators is quite rare in terms of its number of narrators as ideally it should have 15-20 narrators. 
      - Thus <u>the number of links is related to time in order to check for its Uluuw</u>.
  -  Just because a chain is A'ali, it isn't necessary that it is authentic. It can be Sahih as well as daeef, etc. 
- *Nazil*: A lowly chain having the opposite of the features of *A'ali* chain. 
***

*Musalsal*: 
- A hadith which is repeatedly narrated in a particular way by several chains. 
- Examples: 
   - The Hadith of Umar(Actions are by their intentions) is usually narrated as the very first hadith in books. 
   - The Hadith of Muadh (the dhikr after Salah) 
- It may or may not be Sahih.
  - Most of the Musalsalat hadith are weak, not the Hadith itself but the way the people add the description of how it was narrated. 

***

</div>

