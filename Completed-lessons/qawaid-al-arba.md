# Qawaid Al-Arba - The 4 principles regarding Shirk commited by polytheists of Makkah

|--|
1st| The Mushrikeen of Makkah understood meaning of Tawheed and affirmed Tawheed Rububiyah(Tawheed of Lordship— i.e., Allah is the creator, sustainer and Lord of the universe) and still weren't considered Muslim. 
2nd| They only took their idols as intercessors and ways of seeking nearness to Allah and knew that Allah manages their lives but thought that whatever they're worshiping also aids them.
3rd| They worshiped trees, stones, prophets, saints, angels, sun, moon, etc & regardless of what they worshiped, Prophet (ﷺ) fought them all without differentiating among them about who worshiped stones or who worshiped prophets, saints & angels. So all of them were at shirk.
4th| Mushrikeen of Makkah worshiped Allah sincerely & solely when in times of troubles & hardships. They only worshiped others in times of ease. But today the grave worshippers worship other than Allah in both cases, making them even worse than Mushrikeen of Makkah. 
