## 7 Shuroot of of La-Ilaha-illallah

| CONDITION    | OPPOSITE                      | PROOF    |  
| ------------ | ----------------------------  | -------- |  
| Knowledge    | Ignorance	               | 47:19    |  
| Certainty    | raiyb(doubt), shaq(suspicion) | 2:147    |  
| Sincerity    | shirk(polytheism)             | 98:5	  |  
| Truthfulness | falsehood, nifaq(hypocrisy)   | 29:1-3	  |  
| Love         |                               | 2:165	  |  
| Compliance   |                               | 39:54	  |  
| Acceptance   |                               | 43:23-24 |  
