# Shuroot As-Salah, Arkaanuha Wa Wajibaatuha


Content| <big>***==Salah==***</big> | <big>***==Wudhu==***</big> |
:-----:| :---- | :--- 
*Shuroot* | <big>**09:**</big> <p>Islam, Aql, Tamyeez, Niyyah, Waqt, Raf-ul-Hadath, Izlatul-Najas, facing Qibla, Covering Awrah</p> | <big>**10:**</big> <p>Islam, Aql, Tamyeez, Niyyah, Waqt, Iqita, Istinja/Istijmar, Purity of water, removing oil, continuity in Niyyah</p> 

*Wajibaat*| <big>**08:**</big> <p>Takbiratan, sayings of: Ruku, 2 Qiyams, Sajda, Jalsa, Tashahud awwal, sitting in Tashahud</p>  | <big>**06:**</big> <p>Washing: face, arms & feet; Wiping head; Order; Continuity</p> 

*Arkaan*  | <big>**14:**</big> <p>Standing, Takbiratul-Ihram, Surah Fatiha, Ruku, rising from it, Sajda, rising from it, sitting in Jalsa, Itminaan, order, final tashahud, sitting in it, salat upon Prophet, Tasleemataan</p> |    —            

*Nawaqid* | — | <big>**08:**</big> <p> </p>  



