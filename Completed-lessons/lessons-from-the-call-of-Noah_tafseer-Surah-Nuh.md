## <big><big>Lessons from the Call of Noah to his People </big></big>
### Commentary on Surah Nuh (71) by Imaam As-Saa'di
### Taught by Abu Iyaad    
[SalafiSounds.com](https://www.salafisounds.com/lessons-from-the-call-of-noah-to-his-people-by-abu-iyaad/) `{single lecture}`
<div style='text-align: justify;'> 

***
<span style='color:red;'>[06/09/1441 | 30/04/2020]</span>

- Sheikh Rabee explained dawah of prophets in → *Manhajul-Anbiya fid-Da'wati Ilallah **feehil-hikma wal Aql***  
- Surah Nuh (71)
  - Dedicated only to Nuh, the first Messenger 
-Purpose of Prophetic stories: guidance, instruction for dawah & rectification of society → distint way of call.
  - Khwarij didn't understand tawheed of Prophets & Hikmah of Allah, ∴ they differed on these 2 matters above
- People of Nuh were misguided by elites
  - idol, ancestor worship; social evils
- People of Sunnah have firm grasp of these issues in general & detail → returning back to scholars
  - Today the fitrah of people is attacked by the elites to exploit them.
- Allah sends Prophets out of Mercy for people 
  - applies to all prophets in general: what motivates them in dawah → *Rahmah* | Fear of punishment for them 
    - [16:64], [17:86-87], [46:21].
  - Same should motivate us too, in Muslim or Non-Muslim land. 
  - unlike what Moududi & Qutbis claim about revolution against tyrants
    - Revolutions are driven by worldy reasons - power, economy, etc. 
- Nuh's warning:
  - what is Nuh using: admonition, reminder, arguements of reason
  - what is Nuh warning against: Allah's punishment.
  - what will bring them deliverence [71:3]: 
    1. Worshpping Allah alone
    2. Taqwa. Talq bin Habeeb, the tabi'ee, explained Taqwa as:
       - Do actions of obedience, abandoning His disobedience
       - Hoping for Allah's rewards
       - Fearing His punishment
       - Doing so on the basis of Ilm
    3. Obeying the messenger 
       - They're our leaders & exemplary in terms of manners, characteristics, patience, call. 
- Motivators used by Nuh:  Yours will be a goodly life | Allah will give you an enjoyable term of life & repel destruction from you. 
    - Way of Dawah. We tell those whom we're calling, that Allah will make your your life wholesome, your'll move from the misery your're living in to a happy life, by virtue of Tawheed. 
    - Tawheed, obedience to Messengers & all thats attached to it, allows one to understand this creation & the affairs of world - good, evil, etc. For others, these things can't make sense for them. They are pessimisstic sophists w/ false arguements. Muslims are the most firm, stable, patient people. 
- [71:4]
  - Nuh called them so that Allah forgives them. 
- Their response:
  - Put their fingers in their ears
  - Not interested in seeking truth
  - Persisted upon their kufr
  - Increased in arrogance
- Messengers called towards seeking Allah's forgiveness, & even they needed His forgiveness as it isn't possible for anyone among the creation to show the gratitude & worship  thats truly due to Allah for all His favours. To make up for that, we seek Allah's forgiveness for our shortcomings. 
  - Messengers  didn't commit major sins, & even if they commited minor sins, they repented & didn't persist on it
  - They had high standards than common men
  - They made *Ijtihaad* in certain case & thus made *khata'* (mistake), but there is no evil attached to it.
  - Allah corrected them simultaneously. 
- [71:8-9]
  - Shows his eagerness
  - From the ways & means of Dawah is that one comes to people w/ every *baad* (way) that you assume might benefit them. 
- [71:10-12]
  - Wisdom in calling those drowned in sins →  Don't begin by condemning them but by informing them of:  
    - Of Allah's *Rahmah*, 
    - paradise
    - benefits in dunyah, etc   
Example: story of man who killed 99 people
- [71:13-20]
  - rational argument.
  - Stages of life - from a detested drop of sperm to a full frown man - masterful creation of Allah.
    - An indirect indication of resurrection. 
  - Creation of the sky, sun, moon, earth, etc & phenomena that makes lives possible. 
- [71:21]
  - Elites & their followers are in loss
  - Elites have vested interests & exploit commoners. The latters' turning to Allah will free them of formers' control.
  - Thus Elites have been enemies of Prophets in past
- [71:23]
  - These 5 were pious worshippers
  - Shaytan makes people commit shirk by way of:
    - Exaggerating pious 
    - Exaggerating in the phenomena around us, like fire, sun, star, etc
  - Elites scared people away from Prophets by saying that abandoning their idols will bring upon them misery & loss.  
- Stages of shirk:
  - Ignorants raise & decorate graves of pious people & compete w/ each other to show respect to dead.
  - Visit frequently that grave. Make dua to Allah for the dead
  - Make dua to Allah through the dead (e.g., by the right, honour, status of that dead), for themselves → A Bid'ah
  - Asks the dead to make dua to Allah, from that grave → Shirk
  - Sacrificing & other forms of worship for the dead 
  - Some oppurtunists among them try to benefit from these gullible & form a full fledged religious system around this shirk. These people gain influence & are from biggest enemies of Prophets 
- [71:28]
  - As these kufaar reached a stage of disbelief, where guidance isn't open to them anymore. So Nuh made dua against them. 
- [71:25]
  - A rule in the creation of Allah: none destroyed except by their own sins, 
    - for non-muslims: whole nation is destroyed
    - for muslims: not completely wiped out, but face trial & hardship 
  - Ibnul Qayyim: It's Allah's Wisdom & Justice that He shows people their own actions in certain forms & representations - by way of famines, overpowering of enemy, tyranny of rulers, diseases, anxiety, restricting of provision, inciting of shayateen against them.
    - This has been witnesses in stories of Prophets & in our times too 
    - Likewise for an individual, all problems faced in life are result of some reason
    - rectification lies in what Nuh called to: Tawheed, taqwa, Taa'a, etc
- [71:26]
  - Nuh, who was w/ them for 950 years, knew them well
  - In those people remaining alive was a pure *mafsada* 
- [71:27] 
  - From the Mercy of Allah was that 40 or 70 years before the punishment, Allah made all the kufaar barren. 
- [71:28]
  - Made dua for the Muslims



***
