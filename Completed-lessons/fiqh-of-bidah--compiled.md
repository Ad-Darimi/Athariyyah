## <big> [The Completion and Perfection of the Shari'ah](http://www.bidah.com/articles/zhayx-the-completion-and-perfection-of-the-shariah.cfm) </big>

- [5:3]
  - Ibn Kathir: From greatest of favours of Allaah upon this ummah that He perfected their deen, so they're not in need of any deen besides it, & nor any prophet besides Rasulullah. ∴ there's no halaal/haraam except what he made halaal/haraam, & there's no deen except what he legislated. & everything he informed about, it is true & the truth, there is no lie in whatsoever or any discrepancy [6:115], meaning it's truthful in information & just in commands & prohibitions. So the favour upon us is complete.
- "There was not any Prophet before me except that it was a duty upon him to direct his nation to every good he knew for them and to warn them from every evil he knew for them". [Muslim]
- "I have not left anything which brings you closer to Paradise or distances you from the Fire except that I have informed you of it". [Tafsir Ibn Kathir - (18:17)].
- "By Him in whose hand is my soul, I have not left anything which brings you closer to Paradise and distances you from the Fire except that I have commanded you with it and have I left anything which brings you closer to the Fire and distances you from Paradise except that I have prohibited you from it". [Ahmad & Ibn Khuzaymah].
- Abu Dharr al-Ghifaree: "Rasulullah left us whilst there was not a bird flapping its wings in the sky except that he mentioned knowledge regarding it to us". [Al-Tabarani in al-Mu'jam al-Kabir (no. 1647)].
***
## <big> Avoiding bid'ah involves knowing 6 affairs </big>
**By Sheikh Uthaymeen**  
**Taught by: Abu Khadeejah**

*Ittibaa'* of Rasul: 
  - Allah stated: [60:6], [3:31] & [98:5]. Here, Hunafaa means they don't deviate to right or to left. 
  - This is Al-Mutaab'aa (itibaa of Rasul), just as is prescribed in ahadeeth "Pray as you've seen me praying" [Bukhari:631, 6008, 7246] & likewise for hajj, wudhu, etc. 
  - *Mutaab'aa* isn't actualised (& it'd rather be a sin) unless person's *ibadah* is w/ that which Rasulullah came w/ in 6 affairs, in its: *Sabab* (cause, reason) | *Jins* (type, species) | *Qadar* (number) | *Sifat* (way) | *Zamaan* (time) | *Makaan* (location)

### ==1. *SABAB*==
Ibadah must be done for the reason that Rasulullah did it for. E.g., 1] Slaughtering meat & giving it in charity is ibaadah, but not if the reason for doing so was end/begining of new year |  2] Sending *Salaat* upon *Nabi* ∵ of *Mawlid* | 3] Praying *salatul-janaza* for eclipse, instead of praing *salatul-kusoof* 

### ==2. *JINS*==
E.g., Sacrificing horse, chicken, etc for *Eidul-Adha*

### ==3. *KAMM / QADR*==
E.g., 1] Praying 6 instead of 4 *rakaat* of *fardh* for dhuhr | 2] washing limbs 4 times during wudhu instead of estd 1, 2 or 3 times intending to be more pure 3] Doing tawaaf 3 or 9 times instead of estd 7

### ==4. *KAYFIYYAH / SIFAT*==
E.g., 1]  Starting wudhu by washing feet, mas'ha over head, etc | 2] Starting Salah by saying *Assalaamu Alaikum* instead of *Allahu-Akbar*. 

### ==5. *ZAMAAN*==
E.g., 1] Performing *Udhhiyyah* (sacrifice) before *Salatul-Eid*, instead of after it. | 2] Praying *Dhuhr* before its time reaches | 3] Making *Hajj* in Shawwal | 4] Specifying the 2 Eeds & Jumu'ah for visting graves | 5] Specifying sacrifice of animal w/in Ramadan | 6] Fasting Ramadan fasts in another month 7] Specifying act of worship for a time that Shari'ah hasn't specified e.g., making particular remembrance or recitation of a Surah at particular time of day or day of week, such as Surah Mulk on Wednesday, believing it to be righteousness & nearness to Allaah to do so. 

### ==6. *MAKAAN*==
E.g., 1] Performing *I'tikaaf* at home | 2] Performing *tawaaf* at a grave instead of *Haram* of *Makkah* | 3] Stops at other than Arafat during Hajj, on day of Arafat

***
##  <big>[Imam al-Shatibi's Comprehensive & Definitive Definition of Bid'ah in al-I'tisaam (1/43):](http://www.bidah.com/articles/muagm-imam-al-shatibis-definition-of-al-bidah.cfm) </big>

[The word] innovation (al-bid'ah) then, is an expression of: (فالبدعة إذن عبارة عن:)
- A path (طريقة) taken in religion (في الدين)
  - Intent behind initiation of such a path is for it to be taken as a course of action.
  - Only religious affairs are refered as Bidah. Worldly affairs are outside the definition of bid'ah
  - Some scholars use "Bidah" in its wider linguistic sense,  like Imaam al-Shafi'ee, Ibn Taymiyyah & Shaykh Muhammad bin Abd al-Wahhaab
- Which is invented (مخترعة)
  - Has no previous example or model 
  - ∴ many things excluded from definition of bid'ah, like those that do have foundation in Shari'ah (e.g., compilation of Qur'an, writing & compiling of hadeeth, congregational tarawih prayer, principles of fiqh, knowledge of grammar & morphology, etc). Even if they weren't formally present, their foundations are found in Shari'ah. ∴ not considered legislative *bidah*, & whoever referred to them as innovations, only meant in linguistic sense, (e.g., saying of Umar "excellent bid'ah" about gathering people together in single jamaa'ah for taraaweeh → this had foundation in Sunnah as Rasulullah led people for 3 days in Ramadan in congregational taraweeh). 
- And resembles the Shari'ah (تضاهي الشرعيّة)
  - *Bidah* is of 2 types: 
    1. *Bid'ah Haqeeqiyyah* (Essential, Proper): Has absolutely no basis & is completely alien to religion
    2. *Bid'ah Idaafiyyah* (Relative): <u> Resembles Shari'ah in its foundation, but opposes the Shari'ah in its form & details </u>.
       - Most innovations are of 2nd nature.
       - e.g., Congregational dhikr, taking birthday of Rasulullah as day of celebration (Eid).
       - If it wasn't the nature of innovations to resemble Shari'ah they would be treated as habitual actions.
       - Mubtadee' innovates them from outset to resemble Shari'ah & deceive others & mix affairs of Sunnah w/ his innovation, ∵ this is how he'll get response from people.
       - There are innovated ways & matters, resembling Shari'ah but w/o intending nearness to Allaah & worship. 
         - These don't enter into bid'ah. 
         - E.g., Imposed fines by ruler that have particular value which may resemble that of zakah, likewise using sieves & washing hands w/ potash, etc from things that weren't previously present. 
- And by whose practice exaggeration in worshipping Allaah, the Exalted, is intended (يُقصد بالسلوك عليها المبالغة في التعبد لله تعالى).
  - From the completion of bid'ah's definition, ∵ it's the main objective behind inventing it, other than jahl, wanting to be followed, etc.
  - Mu'adh bin Jabal: "It is feared that a man might say: They are not my followers and will not follow me, even though I have recited the Qur'an to them. They will not follow me until I innovated something else for them. So beware from what he innovators, for what he innovates is misguidance" [Sunan Abu Dawud, Sunan Ad-Darimi, Shari'ah of Al-Aajurree, Al-Bida' of Ibn Waddah]
  - Also includes *"bid'ah tarkiyyah"* → when person abandons an action, seeking nearness to Allaah, & this abandonment is in opposition to Shari'ah.
***

## <big> Refuting the Notion of *Bid'ah Hasanah* in Worship </big>

### <big> Part 1 </big> [Ibn Hajar & Ibn Kathir on Linguistics of "Bidah"](http://www.bidah.com/articles/ezaqa-the-notion-of-bidah-hasan-good-innovation-destroyed-part-1.cfm)
- Both words "bidah" (البدعة) (innovation) & "muhdathah" (المحدثة) (newly-invented matter) were used by Rasulullah in blameworthy sense 
- We have to make difference b/w using words with their linguistic meanings & with their *Shariah* (legislative) meanings 
  - When *shariah* uses certain words & terms, it intends meanings above & beyond mere linguistic meaning & definition. 
  - E.g., Word "Salaah" (prayer), literally means du'a (supplication). However in Shariah, it means much more than supplication & refers to collection of inwards states & feelings, statements & outward actions, without which there can be no prayer, as defined by the Shariah. "Zakaat" (charity) linguistically means purity, & in *Shariah*, it has a conceptual meaning that goes beyond the linguistic meaning. 
  - Often Shariah terms qualify, restrict, or expand linguistic meanings, & hence when using terms, once must be operating upon the Shariah defined meaning to avoid confusion & ambiguity. 
- Various statements of Scholars (like Umar, al-Shafi'i, al-Izz bin Abd al-Salam, Ibn Hajar, al-Nawawi, etc) where they used it in linguistic sense.

![14a71da0f281581450edc0aaa22630a4.png](../../_resources/99f1e5da51eb4700ac0d76d32087035c.png)

- There's no such thing as "good bidah" in Shariah, & where bidah is spoken of in praiseworthy sense, it's with its linguistic meaning. 

- Ibn Hajar al-Asqalani explains the usage of word "Bidah" in scholars' language:

  - "As for البدع, it is the plural of بدعة & it is everything which doesn't have any prior example. Linguistically, it encompasses what is both praiseworthy & blameworthy. In the usage of the people of legislation (Scholars) it is specifically for what is blameworthy & <u> if it is used in connection to what is praiseworthy, then it is upon its linguistic meaning" </u>.  
[Fath al-Bari (13/278) in the book "Holding Fast to the Book & Sunnah", Chapter "What is disliked of delving & disputing in knowledge, exaggeration in religion & innovations"]

  - "And المحدثات (newly invented matters), with fathah on the daal, is the plural of محدثة (novelty) & what is intended by it is what has been newly-introduced & doesn't have any basis in legislation. It is referred to in the usage of the Shari'ah as بدعة. As for what has a basis indicated by the Shari'ah then it isn't بدعة. For بدعة (innovation) in usage of the Shari'ah is blameworthy as opposed to its usage (with its) linguistic (meaning), for everything that has been newly-invented without any prior example is named "bid'ah" irrespective of whether it is praiseworthy, or blameworthy."  
[Fath al-Bari (13/253)]

- Ahlul-Bidah present statements of some scholars like al-Shafi'i & al-Izz bin Abd al-Salam to people by citing them through Ibn Hajar's citation of them in Fath al-Bari (Kitab al-Tawhid), to make it look as if Ibn Hajar's view is one of total corroboration & acceptance, but when we see the entire section, we see Ibn Hajar's clarification of matter shows that his view & definition of bidah (in Shariah) is one that agrees with that of al-Shatibi & Salaf in general. 

-  The saying of Ibn Kathir
Ibn Kathir said in explanation of [2:117]:
"And bidah is of 2 types. Sometimes it is a bidah *shar'iyyah*, such as his saying, "For every newly-invented matter is an innovation and every innovation is misguidance..." & sometimes it is a bidah *lughawiyyah*, such as saying of Umar bin al-Khattaab regarding his uniting them together for Tarawih prayer & making them maintain this practice, "What an excellent innovation this is..."..."

### <big>Part 2 </big> - [Understanding *Al-Masalih Al-Mursalah* — Matters of Public Interest](http://www.bidah.com/articles/uvlmw-the-notion-of-bidah-hasanah-good-innovation-in-worship-destroyed-part-2.cfm)

- Important to grasp diff b/w condemned bid'ah & b/w what enters into matters of public interest & to which linguistic application of the word bid'ah has been applied by some scholars (al-Shafi'i, al-Nawawi, etc). 
- Imam al-Shatibee addressed this in detail in al-I'tisam.

*Maslahah Mursalah*
- There's necessary binding relation b/w *Shari'ah* & matters of personal & public beneficial interest ∵ thats what Shariah came to actualize. This is embodied in the fact that:
  - *Shariah* is founded upon actualizing all beneficial interests for servants & repelling all harmful matters from them in worldly & religious matters 
  - That *Shariah* hasn't omitted any beneficial matter of interest at all, ever. It either specifically mentioned it, or has mentioned broad principles that encompass it in a non-specified way. 
    - *Shariah* came to protect 5 necessities, which the preservation of RELIGION | LIFE | REASON, INTELLECT | LINEAGE | WEALTH 
    - Masaalih Mursalah relate to these 5 areas. 

There are some interests that Shariah hasn't recognized & ignored them (such as use of alcohol for medicinal purposes, fornication as enjoyment outside marriage), so these are unlawful to pursue (<u> ∵ their limited, subjective, personal interest or benefit is overwhelmed by their broad harm </u>). But, there are many beneficial interests which don't have specific evidence from <u> Quran, Sunnah, concensus or analogy </u>. Nevertheless, there may be universal Shariah principles that indicate beneficial interests beyond what is specifically mentioned & legislated. This is called *Maslahah Mursalah* → "a beneficial interest (*maslahah*) that's non-specified, left open (*mursalah*)". Such beneficial interests may be indicated by the spirit of Shariah & universal principles (such as <u> "that by which an obligation can't be fulfilled is itself an obligation"</u>, <u>"there is no harm or reciprocating harm"</u>, <u>"leave that which makes you doubt for that which does not make you doubt"</u>, <u>"repelling evil comes before actualizing good"</u>, etc), ∵ of nature of human life & progress, specific situations & matters may arise & may not be covered or mentioned specifically by Shariah rulings. ∵ environment, state & condition that people live in changes from time to time & place to place, & progress takes place in many areas of life, culture, civilization, tech, industry, etc.
∴ there may arise issues that'd become issues of public interest (as defined by the 5 preservations), & they may be legislated to protect that interest if certain conditions are fulfilled, which generally are:

1. That it mustn't clash w/ Shariah texts or with an ijmaa'
2. That it returns back to goals & objectives of Shariah aiming to protect a beneficial interest (among th3 5)
3. That it's not in relation to any rulings already estd in the Shariah, which have been legislated specifically for that particular interest.
4. That it's verified that it's an actual, true & real beneficial interest & not a presumed or suspected one. So it must be verified that legislating it will bring about good or repel a harm.
5. That it's a matter of public interest & isn't for any personal benefit for any particular individual or group of individuals.

∴ *Maslahah mursalah* is:  
<u>"Making consideration of matters which are of public interest (acquisition of good or removal of harm) for which there is no specific evidence or basis (in relation to it specifically) in the Shariah, but which in a general sense would agree with universal Shariah principles and goals. Consideration is made of them in order to help protect and preserve the five necessities (life, religion, intellect, wealth and honour).</u>

- Example:
  - People die or are injured ∵ of reckless driving. ∴ legislation is enacted to impose fines for speed violations. There's public interest involved that *Shariah* aims to protect. There's no conflict w/ any Shariah text. Its benefit be realized. ∴ after consideration of these matters, ruler implements legislation, & this legislation isn't sought in & of itself but as means to attain an objective of Shariah, i.e., it's from the *wasaa'il* (means to an end) & not *maqaasid* (actual goals, objectives sought in themselves). This legislation is down to purely circumstantial reasons. If in another country, public transport is excellent & most people don't use cars, or that all drive carefully, there's no need for such legislation. 
  -  A small town rapidly expanded & many people aren't able to hear *Azaan*. So loudspeakers are used to tackle this issue. This relates to an objective of Shariah. It doesn't clash with any Shariah text. The beneficial interest will definitely be realized. After these considerations, loudspeakers will be used, & this policy isn't sought in & of itself, it's a means (*waseelah*) to an end, & actual use of speakers isn't considered religion or worship, but simply a facilatating means for an already existing Shariah objective. 
     -  Here, one sees rulings that *Khulafa Ar-Rashideen* made such as Umar treating 3 pronouncements of divorces as one (that isn't treated as *bid'ah* in *Shariah*, but as matter of public interest); gathering of Qur'an into single mushaf (a means to an end to serve a Shariah objective. It's innovation from linguistic sense, but not from Shariah sense, and is simply a *waseelah* (means) by which a Shariah objective {preserving religion} is fulfilled). Al-Shatibee gives this example in al-I'tisam.
- These issues of *Al-masaalih al-mursalah* aren't to be considered as Shariah rulings (*haram*, *wajib* etc.), but are considered additional & new legislations which serve goals (*maqaasid*) of *Shariah*. If issued by ruler, one must obey him in those matters. One must also follow these rules if not doing so leads to harm. 

####  Differences Between *al-maslahah al-mursalah* & *Bid'ah* 

- Common grounds:
  - Linguistically they both are new, & invented, ∵ not present in time of Prophethood.
  - Both don't have any specific evidence from Shariah & only generalized texts or principles are used as evidence for them.  In case of bid'ah, these are doubts which appear to be evidences. 
- Differences:
  1. Maslahah mursalah aims at bringing about or preserving a benefit or protecting from or removing a harm. ∴ it's *waseelah* to an end & not an end in & of itself. It's used to reach & acquire 5 Shariah defined objectives. Bidah is sought in & of itself as a goal, & claims to bring about nearness to Allaah, in its own right & is from religion.
  2. Bidah is related uniquely to matters of worship & matters of religion associated w/ them, unlikd *maslahah mursalah*.
  3. Maslahah mursalah has a rationally understood meaning behind it, unlike affairs of worship to which bid'ah is related. Affairs of worship can't be fathomed via reason & rational answers can't be given like why there are 5 prayers, & why they are at specific times, or why wudhu is invalidated by relieving himself & why he must wash his hands, face, arms and feet, despite no impure thing touched them, yet if any impure thing touched his body, it wouldn't invalidate his wudhu, he only has to remove impurity. ∴ affairs of worship don't follow reason. But *maslahah mursalah*, since it relates to acquisition or preservation of a benefit or repulsion of harm, it has a rational meaning behind it that can be understood & fathomed. Al-Shatibee gives 10 examples to illustrate difference b/w *Maslahah mursalah* & *bidah* — compilation of Qur'an into single book (for preservation of deen), & Companions agreeing upon 80 lashes for one who drinks alcohol (for preservation of intellect, wealth etc.), & likewise killing a group of people as retribution for their collusion in killing single individual (for protection of life).
  4. Bidah imposes further burden upon people in terms of something above & beyond what's in Shariah, but *Maslahah Mursalah* facilitates affairs for people, by protecting from harm or removal of hardship or protecting beneficial interest.

  5. Bidah essentially clashes with objectives of Shariah (takes people away from ittibaa'), but *maslahah mursalah* supports & comes under objectives of Shariah (preservation of 5 essentials).

  6. *Maslahah mursalah* is unique in the sense that the reason why it didn't take place in time of Rasululah or Sahaba is because of absence of what may've reqd or necessitated it, or the cause that may've reqd it was present but there was something preventing from it being considered. But bidah was never acted upon or introduced despite every reason, cause & motive being present, w/o any preventative barriers, for it to take place, & yet it didn't take place. e.g., 
     - every reason was present to commence celebrating birthday of Rasulullah after his death, because of his absence, & there was nothing to prevent that. Yet, it was never done by Salaf at all, making it a bidah. 
     - There was no reason to add 2nd athaan for Jumu'ah in time of Rasulullah, yet, Uthmaan added it ∵ of reasons that reqd it (increase in people & expansion of city). ∴ he added a pre-existing act of worship (aadhaan), w/o  changing anything from it or adding to it, in order to fulfil a Shariah goal, which is preservation of religion, & by which people were notified. So this doesn't enter into bidah ∵ such reason wasn't present in time of Rasulullah, & had it been, revelation may have been sent legislating this aadhaan. But, as Rasulullah ordered Ummah to follow Sunnah of *Khulafa ar-rashideen*, then what they instituted are treated to be from Sunnah, & not from blameworthy bidah. 

### <big>Part 3 </big> - [Statements of Imaam Al-Shafi'i](http://www.bidah.com/articles/rouuz-the-notion-of-bidah-hasanah-good-innovation-in-worship-destroyed-part-3.cfm)

- Al-Bayhaqi, Ibn Hajar, Ibn Taymiyyah, etc cited statements of al-Shafi'i:  
"The newly-invented matters are of 2 types: The 1st of them is that which has been introduced from that which opposes (something from) Qur'an, Sunnah, Athar, Ijmaa'. This is the misguided innovation. & the 2nd is that which has been introduced of goodness & there's no opposition to anyone of these. This is the newly-invented matter which isn't blameworthy".  
[Al-Bayhaqi in Manaqib al-Shafi'i (1/469)] & [Al-Fath (13/253)]
- Ibn Hajar mentions: Al-Shafi'i said:  
"Bidah is of 2 types: praiseworthy & blameworthy. Whatever is in agreemenet w/  the Sunnah it is praiseworthy & whatever opposes it is blameworthy".  
[Al-Fath (13/253)]

- What al-Shafi'i is referring to goes beyond & outside the scope of *Shariah* defnition of bidah, & this is estd by his followers (such as al-Izz bin Abd al-Salam) who made it clear in their particular classification of bidah that they enter matters which go beyond affairs of worship & enter into *wasaa'il* & *masaalih mursalah* (matters of public interest). Shaykh of Qatar, Ahmad bin Hajar Aal Butami Al-Shafi'i (d. 1423H) explains in 

"As for this saying of Imaam al-Shafi'i, intent behind "praiseworthy innovation" is whats been innovated of beneficial matters relating to worldly affairs & [affairs of] habitation, livelihood such as the use of radio, electricity, airplanes, cars & using phone & what is similar to that of good & beneficial inventions ∵ they;re not harmful (in & of themselves) and don't lead (in & of themselves) to any evil that comes to people, or to performance of what is haraam or destroying any foundation from foundations of religion. Allaah permitted His servants to invent whatever they wish to look after their worldly interests in [2:77]."  
[Tahdhir al-Muslimin anil-Ibtidaa' wal-Bida' fil-Din (pg 114)] 

Ibn Taymiyyah:
"& from here, the misguidance of one who innovated a path or belief - claiming that faith can't be completed except w/o it - becomes known, alongside the knowledge that Rasulullah didn't mention it. & whatever opposes the texts, then it's an innovation by agreement of Muslims, & that which isn't known to oppose [the texts], then it's not always called 'bidah' (in Shariah sense). Al-Shafi'i (rahimahullaah) statement or what's similar to it has been related by al-Bayhaqi with in authentic chain of narration in al-Madkhal..."   
[Majmu' al-Fatawa (20/163)]

Al-Mubarakfuri:
"For his saying (sallallaahu alayhi wasallam), "Every innovation is misguidance" is from the concise, profound words (*jawaami' al-kalim*), nothing exits from them, & it's a mighty foundation from foundations of religion. As for what occurs in speech of some of Salaf of considering some of the innovations to be good, then that's in relation to linguistic bidah, not Shariah. From (the examples) of that is saying of Umar (radiallaahu anhu) regarding tarawih: What an excellent innovation."  
[Tuhfatul al-Ahwazi (7/366)]

Example here is what's cited by Ibn Hajar in Fath al-Bari from al-Shafi'i in relation to touching corners of Kabah. In Kitab al-Hajj, Al-Bukhari brings a chapter heading titled "On the one who does not touch except the 2 Yemeni corners" & in his commentary Ibn Hajar discusses the various narrations in this regard that pertain to Mu'awiyyah (radiallaahu anhu) touching all 4 corners & Ibn Abbaas (radiallaahu anhu) advising him that only the 2 corners are to be touched. Mu'awiyyah states, "There is nothing from the house (ka'bah)  that's abandoned." Ibn Hajar cites al-Shafi'i: 
"& al-Shafi'i responded to the saying of one who said, "There is nothing from the house (ka'bah) that is abandoned" with (the response) that: We haven't left touching the (2 corners) out of abandoning the house, & how can a person be abandoning it when he's making tawaf around it. Rather, we follow Sunnah both in *fi'l* (performance) & *tark* (abandonment), & if not touching them both constitutes abandoning them, then not touching what is b/w the corners would also be abandonment of it, yet there's none expressing this [view]."  
[Fath al-Bari (3/473-474)]


![fa9772949e2c781d666f53a806a021c2.png](../../_resources/7cc1c772d32b4834a5b90feb74f6d750.png)




***
