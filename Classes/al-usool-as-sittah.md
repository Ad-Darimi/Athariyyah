Al-Usul As-Sittah

<div align='center'>  

## <big><big><big> سِتَّةُ أُصُولٍ
</big></big></big>
## of Shaykh Muhammad bin Abdul-Wahhab bin Sulayman bin Ali bin Muhammad bin Ahmad bin Raashid At-Tamimi (1115AH-1206AH)

### Taught by Abu Iyaad
</div>

***

<div style='text-align: justify;'> 
 
<span style='color:red;'>[L1 | 06/10/1441 | 30/05/2020]</span>

#### Explanations by: 
- *At-Ta'leekatul --- ala Rasailil-Aqadiyyah* (<u>Shaykh Ahmad bin Yahya An-Najmi's</u> Sharh of Usul Ath-Thalatha, Qawaid Al-Arba, Usul As-Sittah & Nawaqid Al-Islaam), 
- *Sullamul-Wusool ila bayaani As-Sittatil-Usool* [Step-ladder in arriving at explanation of the 6 principles] (<u>Shaykh Zayd Al-Madkhali</u>), 
- *Tanbbeh Dhawil-Uqool As-saleemah ila fawaid --- min Sittatil-Usool Al-Azeemah* [Notifying the people of sound intellects of the benefits derived from 6 mighty principles] (<u>Shaykh Ubayd Al-Jaabiree</u>)


## INTRODUCTION

- 4 of the greatest men in history of Islam, after Prophets:
  1. Abu Bakr → Took firm stance against apostates & dealt w/ enemies, fighting for the sake of Allah, those who witheld from givig zakaat. 
  2. Ahmad bin Hanbal → Stood w/ patience & determination, against Ahlul-Kalaam when they were strong
  3. Ibn Taymiyyah → Innovations had become institutionalised & rooted & were many in number. He refuted all of them & left behind a heritage of work & reconnected the Ummah w/ the Salaf
  4. Muhammad bin Abdul-Wahhab → Lived in an era when shirk returned to Arabian Peninsula
- Imp to understand the author while studying a book ∵ of the historical background & reason of writing that book. This helps us appreciate it
- Banu Tamim mentioned in many ahadith, that they're the most severe people against Dajjal
- Born in Uyaynah, north of Riyadh to a family of ulama. Memorised Quran before 10yo. At 12yo, he used to lead Jama'ah Salah & was married.
- Studied under → his father, Abdullah bin Ibrahim Aal-Sayf An-Najdi, Muhammad Hayaat As-Sindhi Al-Hanafi, Ali Afnadi Ad-Dagistani, Ismaeel Al-Ijluni, Abdul-Latief Al-Afaaliqi, Muhammad Al-Afaaliqi, Muhammad Majmoo'ee, Abdullah bin Abdul-Latief Ash-Shaafi'ee, 
- Travelled to → Haramain, Najd, Basrah, Al-Ahsa, Huraymila
- 1153AH: His father passed away
- 1158AH: Sheikh went to Dir'iyyah after being expelled by Ibn Ma'mar (ruler of Uyainah <?>) after warned by Sulayman (ruler of Al-Ahsa)

***
<span style='color:red;'>[L2 | 11-17/11/1441 | 03-9/07/2020]</span>

Except a small no. of people, many of the bani-Aadam erred in these 6 principles. This represents their being far from Allah & their abandoning action upon Book of Allah. 

### Usool
- *Al-Asl* means the lowest part of something, upon which something else is built upon. 
- 2 reasons why Shaykh restricted himself to only 6 principles:
  1. These specifically were the ones in which was the most opposition in his time.
  2. Entire deen rests upon these 6 principles. Deviations in, or absense of these 6 principles shows incompleteness/unsoundness of one's religion. 
- The core Usool ud-deen wrt Sharia texts:
  - **Inner Imaan** → Inward Imaan w/ 6 *arkaan*
  - **Islam** → classified into 2 elements:  
    i.  Outward Islam → 5 *arkaan*
    ii. Its reality: <u> Worship Allah alone | Reject worship of *taghoot* | *Al-Baraa'ah* (freeing oneself) from *shirk* & its people. </u>
- In their various books (e.g., *Usool us-Sunnah*, *Asl us-Sunnah*, *Thalaathatul-Usool*), by term "*Usool*", scholars of *Ahlus-sunnah* refer the <u>other principles</u> found in Quran & Sunnah, relating to matters of belief, actions, methodology, etc. In these Usool, we differ from Ahlul-Bid'ah. Also, these usool arent the same as the earlier Usool of deen (pillars of Imaan, islam, etc).
  - Like Mu'tazilah mention some of their Usool as:
    - *Tawheed* (while meaning by that as denial of Attributes of Allah), 
    - *Al-Adl* (denial of Al-Qadr), 
    - *Al-Wa'ad wal-wa'eed* (making takfeer of major sinners), 
    - enjoining good & forbidding evil (rebel against sinful rulers), 
    - *al-manzilah baynal-manzilatayn* (commiter of major sinner is neither kafir, nor believer, but in b/w)
  - Rawafidh have Usool ul-Arba' which they mainly took from Mu'tazilah:
    - Imaamah
    - Nabuwaah
    - Tawheed
    - Al-Adl
  - Likewise Ashari Maturidis too have Usool, which they call Usool ud-deen by which they mean the foundational beliefs of Islam (like existence of Allah, resurrection, etc) that they establish by *aql*
- Shaykh Ul-Islam mentions *Al-Malik Al-Ghallab* as one of the Names/Attribute of Allah. The latter isn't actually from His Names but Shaykh mentioned it as it is an Attributr of Allah. Attributes of Allah are more vast than His Names, yet both are restricted to whats in revealed texts. Also Allah's Attributes can, in many cases, be derived from His Actions, but we don't claim it to be His Name. 
  - e.g., <big>**الرَّحْمَٰنُ عَلَى الْعَرْشِ اسْتَوَىٰ**</big>
    - Here Allah is described with the Action of *Istiwa*. ∴ we can say that Allah is *"Mustavin"*, *Ala Arshihee* (upon His Arsh), & it is a description, not a Name of Allah
  - *Al-Ghallab* is from the meanings from *Al-Azeez* which has many meanings, like *Al-Izzah* (Might & Power), *Al-Qahr* (He Subdues & Dominates), *Al-Ghalabah* (One who overcomes & is Victorious)
- *Awwaam* (sing: *Aami*, antonym: *khaassah*) = person from general folk, not able to distinguish. A person not knowledgeable in any particular field is *aamee* in that specific field & he doesnt have *tamyeez* in it. 
- *Ajeeb* = *Khilaaf ul-Aadah*, uncommon to what is commonly seen. These 6 principles are *ajeeb* ∵ people don't know them, despite being clear in wahiayn.
- These principles are *akbaril-ayaat* that one can witness, for the power of Allah ∵ these foundations, upon reflection. 
- *Ayaat* are of 2 types: 
  1. *Maqroo'ah* (uncreated Quran)
  2. Created ayaat like sun, moon, rain, etc
Allah used specific words like Ayaat (signs), Baraheen (evidences). Words like *mu'jizah* (= something that renders someone incapable) aren't found in Quran. Muta'zilah were the first ones to use word *mu'jizah*. It is an ambigious term, unlike the terms used in Quran. We should try to stick to terms used in Quran as there is safety in them
- *Adh-dhukaa* (intelligence) means one has sharpness (like in understanding), by which he can see that which is obscure. But <u> *Al-Imaan* has no connection w/ *Adh-dhukaa*, but it has with *Az-zakaa* (purity of heart, soul, intentions) </u>. Ibn Taymiyyah on philosophers (applicable to today's naturalist, scientists): "They were the most intelligent of the people in worldly sciences. They were given *Adh-Dhakaa*, but not *Az-Zakaa*. They were given *fuhoom* [understanding (of worldly affairs)], yet they weren't given *uloom* [(actual, true) knowledge]. They were given faculties of hearing, seeing & understanding, [46:26]".
  - ∴ many commoners with pure hearts are upon certain guidance, while many shrewd & sharp ones aren't
- It is impossible for *aql* to independently arrive at whatever is benefitial or harmful, w/o the *shari'a*. 
  - Ibn Taymiyyah: *Aql* = to have knowledge/understanding of that which is benefitial or harmful & then to traverse the path towards that which is benefitial & abandon the harmful
  - *Aql* is not to merely understand something or have deep knowledge wrt it 
- People of truth are always few in no. & people of falsehood are in majority.  [34:13], [6:116]
  - Indication of truth is making *iltizaam* (adherence) to the *wahi* upin the *fahm* of Sahaba & not to the majority. 

***
<span style='color:red;'>[L3 | 18/11/1441 | 10/07/2020]</span>

- Shaykh Ahmad bin Yahya An-Najmee: Whoever Allah bestows with *Adh-Dhukaa*, yet w/o *Az-Zakaa*, then he hasn't known the right of Alah upon himself, as if this person hasn't heard [91:9-10] (referring to soul). ∴ consideration is given to action & not just abundance of knowledge & intellgence. الله علم"

### <big><big> **Foundation 01: To make the _deen_ purely & sincerely for Allah alone** </big></big>

- Shaytan potrayed to the commoners that Tawheed & Ikhlaas represents belittling righteous people & shirk as honouring them.
- ==Shaykh Ubayd Al-Jaabiree:== 
  - Linguistically *Ikhlaas*, *takhlees* & *Khallasa* mean *At-Tasfiyyah* (purification). 
  - Like purifying honey, by taking out bits of impurity. 
  - Shar'ee: To purufy one's ibadah & make it pure of shirk (whether *dhaahir* or *baatin* {like shirk of *mahabbah*, *ar-riya*, })
  - *ikhlaasud-deen* = *Ifraadullahi wahdahu bil-ibadah*
  - If *Ambiya* had come to ask people <u>just</u> to worship Allah, there wouldn't've been any dispute & enmity.
    - But they added to it by asking to abandon other dieties 
    - Ahlul-kalaam's limiting Tawheed to Rububiyyah laid the foundations of shirk 
  - Priority of *Tawheed Al-Uloohiyyah* & prohibition of *shirk*:
    - [16:36] → generalised meaning
    - [2:22] {*Andaad*, plural of nid, = rivals/similars}
    - [6:163] {sacrifice & *salah* are from the most lofty forms of *ibadah* & they combine b/w worship of heart, tongue, limbs & worship the involves wealth}
    - [4:116] → this Madani verse is a refutation of those *duaat* (like ikhwanis, Hizb ut-Tahreer, etc) who (for justifying their *bid'ee manhaj*) claim the Makkan period was period of tawheed & madinian period was time of *Shar'ee rulings*. Thus Rasulullah for his entire life focussed on *tawheed* like when he warned from building masaajid over graves, at the near end of his life. 
    - [Bukhari : 123] Abu Musa Al Ashari: Rasulullah was asked about which of these are fighting in the path of Allah - a man who fights due to bravery, another man fights due to show-off & another man fights for proving his status/rank. Rasulullah replied : It is the one who fights only so that Allah's word is raied & uppermost.
    - [Ibn Majah : 1783] 
    - [Bukhari : 25], [Muslim : 22] → ∴ tawheed is the chief of all affairs, then comes the other affairs, the greatest of which is the *salah*. Also proves that these 3 things (*shahaadah*, *salah*, *zakah*) are connected & can't be separated
    - [Bukhari : 4477] , [25:68-69] → This proves falsehood of the claims of some who ask to call others to Imaan/tawheed & then leave them to it as it is enough for them to be guided. Rasulullah, after tawheed, gave commands like prohibition of *zina* & *killing*
    - [Bukhari : 2654] → prohibition of shirk
  - Shaykh Abdur-Rahman bin Hassan Aal Ash-Shaykh in Fath ul-Majeed: This undertanding gives us the benefit about being cautious of *ghuloow* & all ways towards *shirk* as shaytaan makes people to enter shirk in the form of *ghuloow* (in *ta'zeem* & *mahabbah*) & *al-bid'ah*. 

***
<span style='color:red;'>[L4 | 18/11/1441 | 10/07/2020]</span>

- ==Shaykh Ahmad bin Yahya An-Najmee:==
  - Ikhlaas to Allah is from the mighty foundation of deen, following on from which is the foundation of *Al-Mutaaba'ah* (following & immitating Rasulullah). Entire religion is based upon these 2. Whoever does an action not based on both of these 2, will have his axns rejected.
    - *Ikhlasud-deen lillahi ta'aala* : [6:162-163] | [18:110] | [98:5] |
    - *Al-Mutaaba'ah* : [59:7] | [8:24] | [33:36] | [3:31] | [Bukhari : 7280] |
  - Tasawwuf & Tashayyu (which are connected) are two sources of entry of bid'ah of worship for other than Allah
    - Ibn Taymiyyah: Tasawwuf emerged in Basrah in the 2nd Century as a minor deviation like exxageration in fear & awe of Allah, goung against the way of Sahaba
    - Innovations crept within them in a step-wise manner, unltil time was apt for Shaytan to delude them into shirk. 
  - Allah clearified in some verses the extent to which Rasulullah shares with Allah some rights while there are specofic & unique to Allah alone 
    - e.g., [24:52]
      - First part → Allah made obedience something that is shared b/w Him & Prophets [3:50], [71:3], <u>[4:64]</u>, [3:132], [4:13], [4:59]. 
      - 2nd half → Allah restricts *Al-Khashyah* & *Al-Itqaa* towards Himslef alone. 
      - ∴ Allah made distinct those affairs which are from rights of Prophets & those (like dua) which are right of Allah alone [3:128]
   - Muwahideen love the pious (if they actually are so) for Allah's sake. Yet they don't give them any of Allah's rights.
     - عن أنس بن مالك رضي الله عنه قال: (أن ناساً قالوا: يا رسول الله يا خيرنا وابن خيرنا، وسيدنا وابن سيدنا، فقال: يا أيها الناس قولوا بقولكم ولا يستهوينكم الشيطان، أنا محمد عبد الله ورسوله، ما أحب أن ترفعوني فوق منزلتي التي أنزلني الله عز وجل) رواه أحمد وصححه الألباني. 
     - قال له رجل: يا محمَّد، أيا سيِّدنا وابن سيِّدنا، وخيرنا وابن خيرنا، فقال رسول الله صلى الله عليه وسلم: ((يا أيُّها النَّاس، عليكم بتقواكم، ولا يستهوينَّكم الشَّيطان، أنا محمَّد بن عبد الله، أنا عبد الله ورسوله، ما أحبُّ أن ترفعوني فوق منزلتي التي أنزلنيها الله)) [804] رواه أحمد (3/153) (12573)، والنسائي في ((السنن الكبرى)) (9/103) مِن حديث أنس رضي الله عنه. وجوَّد إسناده الشَّوكاني كما في ((الفتح الرباني)) (1/336)، وصحَّح إسناده أحمد شاكر في ((عمدة التفسير)) (1/611). .

***
<span style='color:red;'>[L5 | 19-29/11/1441 | 11-21/07/2020]</span>

- ==Shaykh Zayd Al-Madkhali:==
  - Ikhlas towards Allah= "Directing oneself towards Allah w/ every type of worship involving one's body (like *Salah*) or wealth (like *Zakaat*) or both (like *Hajj*)"
  - Since Allah is singled out in His act of rububiyyah in absolute sense w/o any partner or anyone to assist Him, thus He's the alone wrt uloohiyyah, w/o any *nidd* (equal/rival), *matheel* (one like Him), *shabeeh* (resembler) & *nadheer*
  - Everything in shari'ah (& wrt *aql*) has underlying reasons/causes. Like *asbaab* (underlying causes) behind gaining *fiqh* of tawheed: To aquire fiqh of the deen | sit in circles of knowledge where usool of deen are expalained | understand affairs of *ibadah*, rulings of *mu'amalaat* (dealings w/ people), *halal* & *haraam*, *adaab* & *sulook* (behavious & mannerisms)
    - All these rulings branch off from the root of tawheed itself.
    - e.g., knowing the Name of Allah *Al-Hakeem* → *hikmah* in not only Allah's creation but also in His rulings. 
  - Foundations of *Ikhlas*: <u>*tahqeequt-Tawheed*</u> (actualising tawheed),  <u>*al-bara'atu minsh-shirk*</u> & <u>*al-bara'atu min ahlihee*</u> (freeing oneself from watever & whoever clashes w/ any of the 3 types Tawheed)
    - ∴ one's ikhlaas is jeopardised by defects in any of the 3 types
    - Shirk can be:
      1. *shirk al-akbar*
         - [4:48]
         - directing an affair of worship to other than Allah or someone along w/ Allah, after *hujjah* has been established on him.
      2. *shirk al-asghar*
         - Very dangerous for the (ummah due to its apparent insignificance). 
         - Order of shaytaan;ss misguidance: Major shirk > Minor shirk > innovations > Major sins > minor sins > mubah acts wasting time > acts of lesser reward that take away one from greater rewards. 
         - Manifestations: 
           - *Ar-riyaa* → beautifying actions of worship for other than Allah. Countered by sincerity, turning to Allah with all worships, keeping away from lowly intentions like being praised. 
             - Dua against *ar-riyaa* ***"Allahumma inni a’oodhu bika an ushrika bika wa ana a’lam, wa astagh firuka lima laa a’lam"*** 
           - *Al-U'jb* (self-amazement)
           - some of the sayings of people like 
             - "had it not been for Allah <u>and</u> so & so, then this wouldn't have happened", instead one can say "...been for Allah *thumma fulaan* (then after him, so & so)". In the latter, "so & so" becomes just a means. 
             - "*ma sha Allah wa shaa'a fulaan*" (whatever Allah Willed & so & so willed). 
  - What lead to misguidance of ummah : *jahl* (caused by *qillatul ulema rabbaaniyeen* & abundance of feigners of *ilm*) 
  - *Tanaqqus* (belittlement) of <u>righteous</u> scholars, then he is afflicted w/ the disease of *shubhah* (doubt) & *shahwah* (desire) w/ corruption on his tongue & in his heart, creating barrier b/w youth & scholars. So they will rely on the *qiyas* of their own desires, cauing widespread innovations. As time passes on, the later people will consider those rulings as sunnah. 
  - On the other hand there are those who commit *gluloow* in pious worshippers, which is also a cause of destruction & makes people far from & *tawheed* legislation of Allah. 
    - 4 crimes involved in it: *Dhulm* against himself | bad *adab* (behaviour) w/ Allah | wronged the legislation of Allah | *dhulm* against pious servants of Allah by raising them above their rank.

<span style='color:red;'>[L6 | 23/12/1441 | 14/08/2020]</span>
### <big><big> **Foundation 02: *Al-Ijtimaa' fid-deen* Unity upon religion** </big></big>

- Just like Shaytaan made tawheed appear to people as belittling the pious; he also made people think that differing was ilm & fiqh in religion & unity as heresy

- ==Shaykh Ubayd Al-Jaabiree==
  - *A'mr* (command) & *nahaa* (prohibition). 
    - Amr is applies to 2 things: 
        1. *Al-fe'al wal-haal wash-Sha'an* (matter, thing, action, state of being, afair)
        2. *Talb ul-fa'el* (to request an axn from someone), *bil-qawli ad-daal alaihi* (by way of speech), *a'la jihatil-isti`la* (from a higher authority)
      - Here we mean the second one → <u>request an axn</u>, <u>by way of a speech</u>, <u>by a higher authority</u>
    - *An-Nahi*
      - literal: *Al-Man'*(to withold/prevent)
        - sometimes one's *aql* is also called *An-Nahee* as it prevents him from repulsive speech & action
      - technical: to  <u>request one to **withold** from something</u>, <u>by way of a statement</u>, <u>by a higher authority</u>
  - Not all verses prohibiting differing & commmanding unity have explicit commands, coz in arabic sometimes a command may be presented w/o using command form of language. The asl of command & prohibition is by way of explicit speech (e.g., stand up, go!, come here). E,g.,: 
    - [3:103] → explicit command 
    - [Muslim:1715] → one of the ++indirect++ ways of command & prohibition
       - *I'tisaam* : explained in [Muslim : 2408]. Means to make *Tamassuk* to the book of Allah
    - [6:159] → Revealed wrt Jews & Christians. Some Sahaba mentioned these Ayaat to be wrt Ahlul Bid'ah wash-Shubuhaat. Imam Ash-Shatibee in *Al-I'tisaam* included this Ayah in the list of Ayaat that rebuke Bid'ah 
      - Ayaat revealed wrt to specific cases/instances are given consideration for a general sense & not limited for the specific instance only 
      - Here, the prohibition is shown indirectly in form of a threat
  - [42:13]
    - The 5 firmly determined messgengers are mentioned. 
    - Here, 1st & 2nd principles of this book are mentioned. Allah connected unity & establishing the deen with Tawheed. 
    - Establisg yourself in the deen first, then follow that up with aiding & uniting with others upon deen & do not split or cooperate upon transgression. 
    - The affair of unity is manifested in many acts of worship like Hajj, Jihad
  - [3:105]
    - Here Allah prohibits differing by commanding us not to resemble those who differed 
    - [3:19] - blameworthy differeing due to mutual enmity, even after evidences came to them. 









</div>

Part 6  
43:00
