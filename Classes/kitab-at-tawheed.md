# Al-Mulakhkhas fi Sharhi Kitab At-Tawheed
## Sheikh Fawzan's explanation of Sheikh ul-Islaam Muhammad bin Abdul-Wahhab's Kitab At-Tawheed
## Taught by: Abul-Abbas Moosaa Richardson 


### L1 : 07/04/1432 | 23/11/2020
- An exemplary & well-detailed book. Compared to Al-Bukhari's Kitab At-Tawheed & its like a section of Saheeh A-Bukhari coz of its structure of chapters based on an Aayah & a Hadeeth, bringing statements from sahaba & tabieen, explaining meanings of ayaat.
- Sheikh Abdur-Rahman ibn Hassan bin Muhammad bin Abdil-Wahhab, the grandson of Sheikh ul-Islaam, wrote an explanation of this book named "Fath Al-Majeed": This book stands out as an original, it explains Tawheed w/ all its evidences & gathers great no. of proofs to clarify it for people & is a distinguishing sign of people of Tawheed & a proof against mulhideen. 
- Ahadeeth in the book are of 4 types: Saheeh, Hassan, Hassan li-gairihee (Daeef but strengthened by other chains), Dhaeef yet follows the general meaning of established Ahadeeth or ayaat. 
  - This is custom among scholars called "Isti'naas", where they mention proofs first from Quran, the ahadeeth in their order of authenticity (Like first ahadeeth found in both Bukhari & Muslim, then those present in one of them, then those which're authentic in rest 4 books of Kutub As-Sittah, then they may mention other non-saheeh ahadeeth) in order to build upon the issue.
  - The scholar describes the unauthentic ahadeeth as "it is mentioned..." & they don't directly asribe it to Rasulullah (as in "Qaala Rasulullah")
- There're >= 6 books named "Kitab At-Tawheed":
  1. Kitab At-Tawheed from Saheeh Al-Bukhari (d. 256)
  2. "" of Muhammad ibn Is'haaq ibn Khuzaymah Abu Bakr An-Naysaboori (d. 311Ah)
  3. "" of ibn Manda (d. 395)
  4. "" of Ibn Rajab al-Hanbali {Author of Jaami' Al-Uloom wal-Hikam, an explaination of Nawawi's 40 hadeeth} ()
  5. this-one(d. 1206)
  6. "" of Sheikh Fawzaan 
- Recommended for newbies to Arabic: "Al-Qawl As-sadeed" of Imam As-Sa'adi, "Al-Mulakhkhas" of Sheikh Fawzan, "Al-Jadeed" of Sheikh Muhammad bin Abdil-Azeez As-Sulaymani Al-Qar'aawi, "Al-Qawl Al-Mufeed fi Adilatut-Tawheed" of Sheikh Muhammad bin Abdul-Wahab Al-Wasabi.

***
### L2 : 07-09/04/1432 | 23-25/11/2020
- Ar-Raheem = Possessor of Mercy which is specific to hereafter, for believers. Ar-Raheem = general
- In written work, Bismillahi Ar-Rahman Ar-Raheem is apt, & for verbal speech, we use Khutbatul-Haajah. 
  - A common hadith quoted regarding anything not beginning w/ Bismillah being deficient, is unauthentic.
- [51:56]
- "Kitab" = maktoob (something thats written). "Tawheed" is the "masdr" of "wahhada-yuwahhidu" (he/it made something one). Tho this book is about the Shar'ee meaning of Tawheed (Tawheed's 3 classes/sub-domains) & not linguistic.  
- We don't say "Allah is far above the creation". Rather we just say "Allah is above His throne", as we also attribute closeness to Allah (w/ His Knowledge). Also we don't say for a sinner "Allah is far from Allah", but we say "He is far from Allah", attributing the distance & act of being far to the individual, not Allah who's close to His servants. 
- The obligation of worshipping Allah alone is for all of the responsible creation. 
  - Jinn are accountable like humans, tho they're generally unseen for us. Jinns have specific rules, different from men, that govern their existence, the details of which are known to Allah alone.
  - Both jinn & men had prpose of creation as is in [51:56]. 
- [51:56] is one of the many proofs for "Ithbaatal-Hikmati fi Af'aalillah" (affirming Hikmah for all actions of Allah)
  - This is true for all actions of Allah - whether they're af'aal ikhtiyaariyyah (Those that Allah has done at certain times) or Sifaat fi'liyyah (Those that Allah is always described with). 
  - Nothing that Allah has is w/o a complete & perfect objective. Far is Allah from the imperfection of creating anything for no purpose. Allah created us for the highest & most wise reason (i.e., for us to worship Him alone)
- Taghut (derived from tugyaan) = going beyond the bounds/limits. 
- In generl, the proof has been estd for all people in history by way of Messengers. 
  - Specifically there're some to whm the message didn't reach, like Ahlul Fatrah, a child who died before age of tamyeez (distinction), a very old man, etc
  - Ahlul Fatrah = people born b/w the era of 3 prophets.
- Tawheed, the biggest & first obligation for everone on earth, which includes both ithbaat (of worshipping Allah) & nafee (of worshipping anything else). 
- [6:151-153] - Then, Allah mentions the right of parents, being dutiful to them (other than in disobedience to Allah), which Allah mentioned in an unrestricted way - not limiting our dutifulness to certain amount of actions. 
- Allah also prohibits killing our children out of fear of poverty; going anywhere near illicit deeds; murder, especially against a close kin; approaching an orphan's wealth will ill intentions; not weighing in full volume (cheating) while doing transactions; speaking unrighteously & unjustly while giving a testimony. 
- It's obligatory to fulfill covenents & oaths; abandoning everything opposing Islam.
  - Decalaring things to be Halaal & Haraam are sole right of Allah

***
### L3 : 13/04/1432 | 29/11/2020
- The 10 commands in [6:151-153] are called "Al Wasaaya Al-'Ashr" (The 10 admonitions). 
-  Ibn Mas'ud said regarding [6:151-153]: -- <in the book> --
  - This isn't a hadith, but what a sahabi (Ibn Mas'ud) understood from the aayah. 
  - One of the narrators of this narration was weak. Also the wording quoted in Kitab At-Tawheed is a bit of different than whats in the source books of hadith.
  - This is something common in books of tafser. Imam Ahmad said: "3 kinds of knowledge (narrations) many a times have no basis..." One of those 3 is Ahadith quoted in many books of tafseer. (these are quoted as a record iirc). Ibn Taymiyyah also commented on this point in his "Usool at-Tafseer".
    - Just coz a hadith is quoted in a book of tafseer (e.g, ibn kathir's) doesnt neccessitate that the ahadith are authentic, which are studied & analysed in detail by muhaditheen like Sheikh Ahmad Shakir (who worked on Tafsir of Ibn Kathir & Tabari).
- Hadith of Muadh ibn Jabal --<in book>--
  - A very significant hadith. Thus aka "Hadith of Tawheed". Thus is the first hadith in Ibn Rajab's "Kitab At-Tawheed" & second in Imam Bukhari's one. Also the first hadith here. 


***
### L4 :  /04/2020 | /11/2020 




***




***
### L5 :  /04/2020 | /11/2020 



***
12:00
