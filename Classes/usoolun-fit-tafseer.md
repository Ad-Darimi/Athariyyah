Usulun Fit-Tafseer - Principles of At-Tafseer

## <big> Usoolun Fit-Tafseer | Principles of Tafseer </big>

### Shaykh Muhammad bin Saleh Al-Uthaymeen
#### Taught by Dr. Saleh As-Saleh (Rahimahumullah)

***

<div style='text-align: justify;'>  

<span style='color:red;'>[L1 | 16/09/1441 | 10/05/2020]</span>

One deprived from *Usool* (foundation) is deprived from attaining the  *wusool* (objectives)
From the most magnificient sciences is knowledge of tafseer - explanations of meaning of Kalaamullah.
Like hadith, fiqh, etc, there are established principles in tafseer.

"Quran" linguistically: an infinitive. can mean:
  - recitation - Thus it is *Matloo* (being recited) 
  - *jamma'* - Collection (of akhbaar, ahkaam, etc)
  - *Majmoo* - collected (in musahif, & chests of men)

Shar'ee meaning: 
Kalaamullah, brought to Rasulullah, starting from Surah Fatiha, upto Surah Naas
  - [76:23] | [12:2] 
Allah described Quran by many descriptions & qualities - indicating its magnifcience, effects, comprehensiveness, authority:
  - [15:87] → *Azeem*
  - [38:29] → *Mubarak*, for *tadbeer* of men who understand
  - [6:155] → Means of *Rahmah*
  - [56:77] → *Kareem* 
  - 17:9 → guidance to what is most just & wise * glad tidings for believers
  - [59:21] → humbles the mountain
  - [9:124] → increases imaan for believers & vice versa
  - [6:19] → warning 
  - [25:52] {a Makki verse} → strive against kufaar w/ Quran
  - [16:89] → exposition  of everyting, guidance, mercy, glad tidings for believers
  - [5:48] → Quran testifies in truth the previous revelations, supercedes them & is a judge over all
  - [25:1] → criterion, source of Shar'ia 
    - Likewise, Sunnah is other source: [4:80], [33:36], [59:7], [3:31]
  - [14:1] → takes men out of darkness to light
 ***

- Night of Al-Qadr → first time Quran brought to Rasulullah. [97:1], [44:3], [2:185]   
- Ibn Abbas, A'taa, Saeed ibn Musayb, etc: Age of Rasulullah at time of first revelation = 40 → Age of full perfection of intellect 
- Jibreel →
  - Brought Quran → [26:193]
  - Generous, mighty, close to Allah, trustworthy, high rank & respect among angels, beutiful, pure, fit to be a Messenger → [81:20], [53:6], [16:102]
- First Ayaat sent → [96:1-5] 
  - Then there was a pause in revelation for a while. Then Allah sent [74:1-5] → made Rasululah the Rasul [Bukhari:3, 4]
  - There are other narrations mentioning other verses which were first to be revealed but for certain matters. 
- Sening down of Quran is of 2 types:
  - *Ibtida'ee* (initial)
    - Not preceeded by any cause that necessitated it. Most of the verses. e.g., [9:75] → came to initiate exposition of the condition of some hypocrites
  - *Sababee* (causative)
    - there was a cause for sending these verses. Such causes are:
      - a question being answered e.g., [2:189], [2:219], [2:222]
      - occurances needing clarification/warning e.g., [9:65] A hypocrite during Tabuk expedition said "we havent seen like those reciters having big bellies, being coward in battle"

<span style='color:red;'>[L2 | 16/09/1441 | 10/05/2020]</span>

### The general wording & the particularity of the cause
If verse is sent down due to a particular reason, but its wording is general, then its ruling is comprehensive ∵ Quran was sent as general legislation for entire *ummah* ∴ the relevence is for general wordsing, not to that particular cause e.g., verses of *li'aan* [24:6-9] were revealed for certain cause [Bukhari:4747], yet its ruling applies in general. proof of generality → [Bukhari 4746]

### Makkan & Madanite Revelations
- Quran was revelaed in parts [17:106]
- Makkan → revealed before emmigration
- Madanite → revealed after migration, even tho place of revelation may be outside Madinah. 
  - Last part of ayah [5:3] is a Madanite Ayah, even if it was revealed in Rasulullah's farewell pilgrimage in Arafah [Bukhari:45]
- Benefits of this clasification:
  - Manifesting the eloquecy of Quran, as it addresses people of Makkah & Madina according to their conditions. 
  - Highlights wisdom behind legislation, as it comes in stages, as necessitated by situation
  - Motovating the determination of people, as people yearn for revelation addressing their situation. 
- Distinguihing style & substance b/w Makkan & Madanite verses:
  - Wrt style: 
    - Makkan verses mostly had strength of style of addressing, w/ words of magnificance that'd make mountains tremble ∵ mostly they're for people who turned away, so it befitted them ∵ of their haughtiness - e.g., [74], [54]. Madanite revelation mostly comes in a mild style & ease of addressing ∵ those addressed were believers e.g., [5]
    - Makkan revelation mostly  had short verses & strong arguements ∵ they were addressed to disdainful rejecters e.g., [52]. Madanite verses mostly were long, enumerated rulings w/o much arguements, as it suited their situation e.g., *ayatud-dayn* in [2]
  - Wrt substance: 
    - most of makkan revelation establishes *Tawheed* & *aqidah* {specifically *tawheed al-uluhiyah* & *ba'ath* (resurrection)} against deniers e.g., [36:77-78]. Madanite revelation mostly detailed worship, transactions, as *tawheed* already settled in those addressed. 
    - Madanite revelation extensively mentioned *jihad* & its rulings, hypocrites ∵ of situation, unlike Makkan revealation.
- Benefit of knowing the era of verses → imp of knowledge of Quran:
  - Knowing Quran's elloquency known in its highest degrees ∵ addressing people according to what they need
  - Manifesting the wisdom behind revelation & order of revelation — more imp aspects revealed before others, ∴ satisfying the conditions & extent of acceptance of those it was revealed for. 
  - Direction for *du'aat* (callers) so they follow Quran in its style & substance for addressing people. Putting intensity & style in its proper place. 
  - Helps distinguish abrogating verses from abrogated verses. 
    - Abrogation is estd from earlier nations [3:93] | <u>[3:50]</u>
- Wisdom behind sending Quran in parts over 23 years
  - Strengthening the heart of Rasulullah [25:33]
  - Makes it easy for people to understand, memorise & act upon [17:106]
  - Motivaing the people, so they accept & execute the commands eagerly, specially when required. e.g., verses of *ifk*, *li'aan*
  - Gradual legislation perfected the people e.g.,
    - when the verses of prohibition of alcohol were revealed. [2:219] prepared people to accept it, by giving rational reason. Allah didn't say that the sin of drinking alcohol was **more** (i.e., quantitatively), but He said it is **greater** (i.e., qualitatively). Next came [4:43] - temporal relinquish. {proves that the drunk doesn't know what he says, thus if he divorces his wife or endows wealth, thats not to be executed legally. Also proves that people should know what they are saying when praying, thus *khushoo* is necessary}. Next came [5:90] - strict complete prohibition {islamic meaing of success - attaining what is legally required & being saved from that wh/ is dreadful} {*rijz* means *najasah*. *Najasah* is of 2 types: Physical | Non-physical/intangible. Since here it mentions "*min amalish-shaytan*", ∴ it is non-physical. There is no evidence that *al-khamr* is physically *najis*, just if something is *haraam*, doesn't mean it is *najis*, like poison is *haram* but not *najis*. The Sahaba didn't wash their pots containing *khamr* after they spilled it. Also spilling something *najis* can't be allowed to be spilled on streets of muslims}
    - It is a must, in this time, that we gradually command what is right & forbid what is wrong, to people. 

<span style='color:red;'>[L3 | 18/09/1441 | 12/05/2020]</span>

### Reasons for sending the revelations
- Sometimes Rasulullah was asked question & he didn't answer & waited for revelation. e.g.,
  - [17:85] [Bukhari:125,4721; Ahmad:3688,3898]; [Ahmad:2309] A jew asked Rasulullah "What is *Rooh*?" 
  - [63:8] [Bukhari:4900]
- Shows Allah's care for Rasulullah & defending of him by arguements. e.g.,
  - [25:32] | [17:106]; [Bukhari:4141]
- Showing Allah's care for His slaves in releiving them from hardships. e.g.,
  - [Bukhari:334] 
- Make a correct understanding of verses. e.g.,
  - [Bukhari:1648]

<span style='color:red;'>[L4 | 19/09/1441 | 13/05/2020]</span>

### Arrangement of Quran in Mushaf

- Quran's arrangement = reciting it in succession as it is written in Mushaf. It's of 3 types:
  1. <u>Arranging the words, such that each word is in its proper place in the Ayah.</u> 
     - This is affirmed (in Wahiayn & concensus of muslims & none opposed to it) & is mandatory. 
     - e.g., we can't read [1:1] s "*Lillahil-Hamdu Rabbul A'lameen*"
  2. <u>Arranging the Ayaat, such that each ayah is in its proper place in the Surah.</u>
     - This too is affirmed & is mandatory
     - e.g., we can't read [1:3] before [1:2]
     - [Bukhari:4530] {[2:240] was abrogated by [2:234], i.e., an ayah that comes before it}
     - [Tirmidhi:3086] → ∴ Rasulullah ordered for placing aayaat in proper designated order.
  3. <u>Arranging the Surahs, such that each is in its proper place in Mushaf</u>
     - It's affirmed:
       - mostly by way of *ijtihaad* of Sahaba, so that part isn't mandatory.
       - or by being *tawqeefi*, thus can't be changed (for teaching, it is permissible to begin from an easier Surah, like those from Juzz Amma). Its examples:
         - المُعَوِّذَتَيْن  (Rasulullah used to recite [113] before [114])
         - [62] before [63]
         - [87] before [88]
         - [18] before [12]/[10] 
           - Ibn Taymiyyah: It is permissible to recite it this way. Many Sahaba compiled Mushaf in this order, but they ended up agreeing w/ Uthman;s arrangement, since he's from *khulafaa Rashideen*.
         - [2] - [3] - [4] 
           - [Muslim:772] → Here Rasulullah prayed [2], then [4] & then [3]. But it may be that in the last presentation by Jibreel, it cam in the order: [2] - [3] - [4]

### *Kitaabatul Quraani wa Jam'ehee* (Writing the Quran & its compilation)
Went through 3 stages:
 1. Era of Rasulullah. 
    - Depended **mostly** on memorisation, more than writing ∵ 
      - *Quwaatuz-zakira* (strong memory)
      - *Sura'atul-hifz* (easy & fast way of memorisation)
      - *Qillatul-Katibeen* (shortage of scribes)
      - Shortage of means of writing. Used to write on palm leaves stocks, animal skin, thin white stones, chunk of camel bones. 
    - Many reciters, including Abu bakr, Umar, Uthman, Ali, Ibn Mas'ud, Saalim, Ubay bin Ka'ab, Muadh bin Jabal, Zayd bin Thabit, Abi Darda, etc
      - e.g., [Muslim: 677]
 2. Era of Abu Bakr (12 AH)
    - Writing began after many *qurra* (like Saalim) died in Battle of *Yamaama* against Musaylimah *al-kadhdhab* [Bukhari:4679]
      - There was an *ijmaa* on Abu Bakr's decision. Ali said: "Most in reward concerning the *Musaahif* was Abu Bakr, may Allah have mercy on him, he was the first to collect *Kitaabullah*"
 3. Era of Uthman (25 AH)
    - ∵ of diff in recitations amongst people ∵ of diff copies w/ the *Sahaba*, Uthman feared a fitnah & ordered that all copies be combined in 1 copy only. [Bukhari:4987]


<span style='color:red;'>[L5 | 19-23/09/1441 | 14-17/05/2020]</span>

### Tafseer

- Meaning:
  - Linguistically: *Al-Fasr* = unraveling something cover.
  - Shar'ee: *Bayaanu Ma'anil-Quraan Al-Kareem* (clarifying/explaining the meaning of Noble Quran) 
- It is *waajib* for all that one needs to understand, & for that which he may not need, then that is collective duty, not individual duty
  - [38:29] → Allah made clear the wisdom of sending down the Quran - so people ponder on its verses & recieve admonition. Pondering = thinking about the words to absorb its meaning.
  - [47:24] → Allah rebuked those who don't ponder & think deeply about Quran - this is ∵ their hearts are locked up. The Sahaba learned the meaning of Quran so they could act on them. Abu Abdir-Rahman As-Sulamee said: Those who used to teach us the Quran (Uthmaan, Ibn Mas'ud) told us that whenever they learnt from Rasulullah 10 ayaat, they wouldn't exceed until they know what they imply from the knowledge & actions". Sahaba used to say: "And thus we learn the Quran & knowledge(what it means) & actions (accordingly) altogether"
  - Ibn Taymiyyah: "It is a custom to know that, no people would read a book of any field, be it medicine or arithmetic, except they seek to understand it. So what about the words of Allah, the One Who provides them w/ security & through this book is their salvation, happiness & sestablishment of their deen & dunyah. It's duty of people of knowledge to make it clear to the people, as Allah said in [3:187]".
    -  explaining *Kitabullah* is a covenent that Allah took from the *ahlul-ilm*, so they make it known to people.
- Objective of knowing *Tafseer*: To attain the noble goals & magnificient benefits signified in → believing in, benefiting from & applying its rules in acordance w/ the way Allah intented, so He is worshipped based on knowledge
  - Like studying: the stories of previous Prophets, The Attributes of Allah, etc should manifest its effects
- Duty of Muslim concerning Tafseer: Make himself feel that he's acting like an "interpreter", on Allah's behalf, manifesting what Allah intends. Since this is a difficult task, he is required to know its magnificience, fearing to speak about Allah w/o knowledge.
  - [39:60] [7:33] → implications of uttering about Allah w/o knowledge are beyond that person & corrupts others in society too. ∴ it is more dangerous than shirk. Mushrik hurts only himself & if he's guided, his corruption is lifted, but wrt to one speaking about Allah w/o knowledge {e.g., Jahm bin Safwaan, Jaa'd bin Dirham}, his corruption is widespread.

### References of Tafseer
1. Quran itself. e.g., 
   - [10:62-63] → Metion of *Awliya* & then their description
   - [86:1-3] → Describes Tariq
   - [79:30] is explained by [79:31-32]
2. Ahadeeth. e.g.,
   - [10:26] explained by [Bukhari:573]; [Muslim:181]
   - [8:60] "power" here is explained by [Tirmidhi:3083]
3. Sayings of the *Sahaba*, especially *Ahlul-Ilm* from them. They are the most honourable, truthful, purest & upright people, after prophets & lacked the *ma'asee* & it is a barrier b/w a person & understanding of truth [5:13], [83:14]. e.g.,
   - [5:6]. Ibn Abbas explained "contact w/ women" as intercourse.
4. Sayings of *At-Tabi'een*, who were the best after companions. Ibn Taymiyyah: "If *Tabi'een* are in agreement on a matter, then no doubt this stance is a *hujjah*, & if they differ, then the saying of each one of them can't be a proof against the other, nor against those after them. In this respect, we refer to its general understanding in arabic, or *Sunnah*, or saying of *Sahaba* on that matter... Anyone who turns away from the tafseer of *sahaba*, *tabi'een*, to that opposing it, then he has erred in that, rather he innovated, even though he may be striving to reach the conclusion & forgiven for his mistake..."
5. Whats necessitated from the *shar'ee* or linguistic meaning, in context of verse e.g., [4:105], [43:3], [14:4]
   - If *shar'ee* meaning is diff from linguistic meaning, we take the former ∵ Quran is revealed to explained the legality of matters, not to explain the linguistics of matters, unless there is an evidence by which the linguistic meaning takes precedence. e.g., 
     - Salah - Linguistically = *dua* | Legally  = funeral prayer, while making dua to Allah. 
       - legal (*Shar'ee*) meaning given precedence → [9:84] 
       - linguistic meaning given precedence → [9:103]. Evidence: [Bukhari: 1497,6332,6359]
         - Saying *Allahumma Salli Ala Muhammad* is a dua, thus worship directed to Allah, we ask Allah to praise Rasulullah in the highest heaven
         - Thus, we take the *shar'ee* meaning by default, unless there is evidence for ntot to do so, or if there is agreement b/w legal & linguistic meaning, wh/ is common.


### The differences in Tafseer

Are of 3 types:
1. Differences in wording but not in meaning (i.e., there is agreement in meanings but using different words). Thus it doesn't affect the meaning of ayah. E.g., [17:23] → *Qadha*
   - Ibn Abbas said it means *A'mara* (ordained) 
   - Mujahid said it means "Allah Willed{Iraadah Shar'ee}"
   - Ar-Rabi'a said it means *awjaba* (Allah made it dutiful). 
   - All these have similar/close meanings, thus there is no effect on meaning
2. Difference in wording & in meaning, while the verse may accomodate both meanings, ∵ of no opposition b/w them. So both are taken as meaning & understood like examples for it. E.g.,:
   - [7:175]
     - Ibn Mas'ud said this refers to a man from *bani israeel*. 
     - Ibn Abbas said this is a man from Yemen
     - Some said it is a man from *Al-Balqaa* in Shaam
     - Since here w/o opposition, the ayah can accomodate all 3 of them, we take them all. Ibn Abbas related what he heard, & so did Ibn Mas'ud, etc. So it goes to all of them & they are examples. 
   - [78:34] → *Dihaaq* 
     - Ibn Abbas said it means "filled".
     - Mujahid said it means successive (one after the other / continuous) cups
     - Ikrimah said it means *saafiyah* (pure)
     - Again there is no contradiction here & this ayah can comprise them all
3. Difference in words & meanings, while the ayah can't accomodate all meanings ∵ of opposition b/w them. So, the ayah is taken w/ that meaning as indicated the context, by preponderating. e.g.,: 
   - [2:173] → "forced by necessity, w/o disobedience & transgression" 
     - Ibn Abbas said it means that it is w/o wilful disobedience concerning that dead animal, w/o trangressing in eating (you only take from that dead animal what is enough for you to stay alive)
     - Another meaning mentioned is that it means one who isn't out for transgression, doing *khurooj* against *imaam* of muslims, nor seeking evil/sinful acts in his travel
     - Since these two are opposite meanings, what is relevent is the opinion of Ibn Abbas, as this is in context. 
   - [2:237] → "**they** agree to forego it, & **he in whose hands is the marraige tie**"
     - Ali said that the latter is husband
     - Ibn Abbas said it is the *wali*. 
     - Here, the preponderating opinion is that of Ali. Also, the one who can forego mahr is the wife, not the wali, except if the wali is father & there is a disagreement. 
     - Also, there is a hadeeth


<span style='color:red;'>[L6 | 26/09-09/10 /1441 | 20/05-02/06 /2020]</span>

### Famous Mufasiroon 
Some Sahabah were well known for tafseer - Abu Bakr, Umar, Uthman, Ali, Ibn Masoud, Ibn Abbas

- From Sahaba
  - Ali aka Abul-Hassan, Abu Turaab
    - Son of Abu Talib (uncle of Rasulullah)
    - First from Rasulullah's relatives to accept Islam
    - Husband of Fatimah (daughter of Rasulullah)
    - Born 10 years before Nabuwwah
    - Partook in all battles, except battle of Tabuk
    - Ali: "Ask me! Ask me! verily there's no ayah in Quran except that I know if it was sent during night or day"
    - Ibn Abbas: "If a reliable news comes to us from Ali, we won't resort to anything else". "Whatever tafseer I took, I took it from Ali"
    - Part of the Umar's committee set-up for assigning next Khalifah. Abdur-Rahman bin Awf offered Ali the Khilafah, but he refused except for certain conditions
    - Killed in Kufa, on 17 Ramadhan 40AH. 
  - Abdullah bin Mas'ud bin Ghafil Al-Hudhalee
    - Mother: Umm Abd. (muslim) 
    - Father died in Jahiliyyah
    - Migrated first to Abyssinia & then Madinah. 
    - Attended Badr & those after that
    - Received >70 Surahs from Rasulullah
    - One day Rasulullah wiped over his head & said: "May Allah's Mercy be upon you, You are a little boy, yet very understanding". "Whoever wants to recite Quran as fresh as it came down, let him recite it according to recitation of Umm Abd"
    - [Bukhari: 4384]
  - Ibn Abbas (died 68AH)
    - Cousin of Rasulullah, born 3y before hijrah
    - His maternal aunt is Rasulullah's aunt
    - Once Rasulullah higged him & said "Oh Allah! teach him al-Hikmah/al-Kitab" & "Oh Allah give him understanding in religion"
    - Umar used to invite him to his circles
    - Ataa':"I didn't see any circle more generous & honourable than that of Ibn Abbas, in fiqh & fear of Allah. You see w/ him the people of fiqh, people of quran & people of poetry & he excels them all as if he is taking from a vast valley".
    - Abu Waail: "Ibn Abbas once stood when he was placed incharge of the pilgrimage by Uthman. He stood up & gave khutbah, reciting from Surah An-Nur & gave its tafseer. I used to say  I never saw nor heard the words of a man like those of Ibn Abbas. And if Faris, Romans & Turks would've heard him, they would accept Islam".
    - Uthmaan appointed him incharge of hajj in 35AH
    - Ali placed him incharge of Basrah until Ali was killed, then he returned to Makkah & then Taaif  & died there in 68AH"
- From Tabi'een: From Makkah, students of Ibn Abbas like Mujaahid, Ikrimah, Ataa' bin Abi Rabah (expert in fiqh of hajj). People of Madinah, from students of Ubbay bin Ka'ab like Zayd bin Aslam, Abil-Aaliya, Muhammad bin Ka'ab al-Quradhi. People of Kufah, from students of Ibn Mas'ud like Qatadah, Ilqima, Ash-Shabee
  - Mujaahid bin Jabr Al-Makki
    - Born 21AH
    - Abu Is'haaq: "Mujahid said I presented the Mus'haf to Ibn Abbas 3 times, stopping & asking him about every verse"
    - Ath-Thawri:"If the Tafseer from Mujaahid reaches you, then suffice w/ it"
    - Imam Shaafi'ee & Al-Bukhari related from Mujahid
    - Adh-Dhahabi: "The Ummah is unanimous concerning the Imaamah in knowledge of Mujaahid" 
    - Died in Makkah, while in sajda, in 104AH 
  - Qataadah bin Du'aamah As-Sidusi Al-Basri
    - Born blind in 61AH
    - Had strong memory. "I never said to one who's speaking to repeat & my ears haven't heard anything except my heart kept"
    - Imam Ahmad praised him & dissipated his knowledgw & fiqh, & his knowledge in matters of *ikhtilaaf* & tafseer
    - Died in Waasit in 117AH

## *Al-Qur'aan Muhkamun Wa Mutashaabih*
- Muhkam = perfecty clear 
  - e.g., [49:13], [2:21], [5:3]  
- Mutashaabih = not entirely clear to the degree that some may think things that aren't befitting to Allah, or anything in Islam, though well-grounded *Ahlul-Ilm* know its truth. Examples where people don't take the unclear verses to the clear ones as ordered in in [3:7]:
  - [5:64] → Some make Tashbeeh with creation
    - Such people have deviation in their hearts. 
    - Just like they know that Allah Has a Self, unlike creation, similarly Allah Has Hands unlike creation as in [112:4]
  - [4:78] compared to [4:79] → kufaar claim "contradiction"
    - Actually, both good & evil exist by the Allah's Qadr, yet the good occurs as favour from Allah, & that evil occurs due to the action of slave himself as in [42:30]. 
    - Adding the evil deed to the slave is adding the thing to its cause, not the One who pre-ordained it, however adding the good & evil to Allah is in the sense/angle of adding them to one who pre-ordained them in the first place
  [10:94] → kufaar claim that Rasulullah doubted → 
    - Rasulullah is the most knowledgable & certain from men regarding Wahiayn
    - refutes this claim → [10:104] (same chapter), [2:285]
    - It isn't necessary that this verse implies that there is doubt occuring from him. 
    - [43:81] → Question of same pattern. Its doubt refuted → [19:92].
    - "...be not of those who doubt it" doesn't entail that Rasulullah doubted ∵ forbidding something could be directed to one who do so, e.g., [28:87] → here it is known that mushrikeen didn't turn away Rasulullah from the verses of Allah. The purpose of such questions → revilement of those who do so & warn them & from them. 

- Qur'an varies wrt matters of its verses on this issue in 3 types: 
  1. General perfection w/in wh/ Quran is described in its entirety 
     - [10:1] {muhkam} | [11:1] {hakeem} | [43:4] {Quran is exalted in highness & one who takes by it will too be exalted. It has both uloow (exaltation) & sultan (authority)}
     - These verse tell us the general perfection & exaltation of Quran in its entirety. Perfection of Quraan: This perfection is in its wording & meaning. w/ supreme elloquency. w/ truthful, benefitial info. Its rulings are just, w/o inferiority & shamefulness
   2. General <u>resemblance</u> w/in w/ the verses are qualified w/ in entire Quran
     - [39:23] → *Tashaabuh* here = resemblance. 
     - [4:82] → 
   3. Some parts of Quran have special perfection & some are *mutashaabih* (not entirely clear)
     - [3:7] → 
       - Most stop at "...except Allah..." 
         - They say, Allah referring to the reality of the outcome / consequence of outcome is these *Mutashaabeh*, only known to Allah
       - Others stop after "War-rasaikhun fil-ilm" (& those grounded in knowledge)
         - They say here Allah intends the Tafsir, which is knowm to *Ahlul-Ilm*
         - Ibn Abbas: "I am from the ones who know the meaning of these ayaat"

### Types of *Tashaabuh* in Quran
- Wisdoms in classification of Ayaat into *Muhkam* & *Mutashaabih*  
  Testing people by the *Mutashaabih*. Had all Ayaat been Muhkam, there would be no test, & had all Ayaat been *Mutashaabih*, then the wisdom of having a *bayaan* & guidance by way of clear refering points, for mankind would been missed. True *Mu'min* knows that it is all from Allah & w/o contradiction [41:42]
- Two types:
  1. <u>*Haqeeqi*</u> (real)
     - that which mankind is unable to comprehend fromthe *kayf* (how) of Allah's Attributes. 
     - Even though we know their meanings, yet we don't realise their realities wrt kayf [20:110], [6:103], [10:39]
     - Asking for their *kayf* is bid'ah
       - <big> =="الاستواء معلوم، والكيف مجهول، والإيمانُ به واجِب، والسؤالُ عنه بدعة"== </big>  
  2. <u>*Nisbee*</u> (relative) → Not entirely clear to some, but clear to others firmly estd in knowledge
     - So for these, you can ask for the meaning ∵ it's possib;le to do so. 
     - There's nothing in Quran which is totally hidden for everyone 
       - [3:138] (*bayaan* = clarification) 
       - [16:89] (*tibyaan*) 
       - [75:17-19]
         - Here Allah mentions that He recited Quran to Rasulullah, even though Jibreel did so ∵ it was actually from Allah, so as if Allah Himself recited it & Allah generalised it. 
         - It's like [4:174] [48:10] → also generalised
     - It is ∴ clear either directly by text, or by implication.
     - Examples of *Nisbee* unclear verses → used by Ahlul-Bid'ah. Examples:
       - [4:93] - in arabic called *"khaalidan feeha"*
         - The origin of this *khulood*'s (abiding) meaning is that it's abiding for ever, but this verse is from the ayaat of *wa'eed* (threat) ∴ the intentional killing may be a cause for his *kufr*, making him to deserve this punishment, but this verse is understood along w/ [4:48]. *Khwarij* & *Mu'tazilah* hold that such person is in hell forever ∵ they abandoned other verses like [4:48]. 
        - [22:70]
          - *Jabariyyah* use this verse to say that since everything is written in *Al-Lauh Al-Mahfauz*, so there's no need to work & we are compelled w/o choice & man has no will or power, leaving the muhkam ayaat like 
            - Actions of man are of two types:
              1. *Ikhtiyaaree* (w/ choice)
              2. *Gair-Ikhtiyaaree* (w/ no choice)
            - Was the statement made by Jabariyyah "man has no choice" made by their own choice or compulsion? 

<span style='color:red;'>[L7 | 09/10/1441 | 02/06 /2020]</span>

## Impossibility of contradiction
- Meaning of contradiction: 2 verses contrast such that the implication of 1 prevents that of other, like 1 affirming something & other negating it.
- <u>News oriented verses</u> are impossible to contradict in Quran as in such case one or both verses will be a result of lie or ignorance. Free is Allah from such a thing [4:87], [4:122]
- Also, the law-oriented verses (related to Shar'iah) don't contradict as the latter one will be abrogating the first [2:106]. So in this case, the ruling wrt first verse doesn't exist anymore ∴ it isn't opposing the implication of other. 
- If one feels like an apparent contradiction, he should try to reconcile b/w those verses, else take it to *Ahlul-Ilm*. *Daf'u Ee'haam il-Ikhtiraab A'n Ayahil-Kitab* (Repelling the delusion of confusion from the verses of the book) of Sheih Muhammad Ameen Ash-Shinqeeti. E.g., 
  - [2:2] → Quran is guidance for Muttaqoon. [2:185] → Guidance for mankind. 
    - Former is specific for Muttaqoon & it is the guidance of *Tawfeeq* (success). The latter is general, for both righteous & unrighteous & it is guidance of *Huda* (direting) to the way
    - Similarly [42:52] is guidance of directing & [28:56] is guidance of *Tawfeeq*
  - [3:18] & [3:62] → There's no true god except Allah. | [28:88] [11:101]  → Others gods besides Allah. 
    - Divinity/Goship in first is the true godship & that in latter is false-godship. This is explained in another verse [22:62]. We take all verses together & not in isolation
  - [7:28] {Nay! Allah never commands Fahisha} [17:16] {We command *mutrafeeha* & they disobey}
    - In first, it refers to *Shar'ee* (legal) command while latter is *Kawnee* (universal), pertaining to the pre-decree of Allah
    - *Shar'ee* commands → some people obey, some don't. Like believing, praying, etc. Allah never commands (Shar'ee) any evil [16:90].   
    - *Kawnee* commands → Everything obeys. Allah says Be & it is. This relates to His *Mashee'ah* (Will) [16:9]|[16:40],[36:82]. E.g., creation of Iblees. Allah willed it (Kawnee command), yet he is Displeased w/ him. In this creation, there are many wisdom

## *Al-Qasm* (Oaths) & Oath in Quran
- Means to affirm something by mentioning someone glorified, whether that thing deserves glorification or not. If the oath-taker believes that the one being glorified as Allah deserves it, then it is major Shirk. Else it's a minor shirk. 
- One shouldn't resort to too much swearing by Allah as [5:89] (protecting one's oath). 
- It is preferable that one makes an exclusion while making an oath to make his matter easy & avoid *Kaffarah* (expiation) on breaking the oath.
  - E.g., saying "Wallahi! <u>In Sha Allah</u> I will do such & such..." 
- It's permissible to take oath by the Allah's Attributes, like The Face of Allah
- Making an oath can be wrt past & future
  - For past, there is no expiation 
  - Oath of future is of 2 types: 
     1. Lagw (unintentionally) → No expiation for this
     2. The determined oath → Here, person is: 
        - either trying to relate something within himself - no expiation
        - Or he wants to execute that which is related to the oath - if it doesn't execute, he has to expiate. 
- If he  makes an oath about a future matter where he think it will take place but it doesnt, thenhe doesn't need to expiate as he is telling about that which is in his self concerning that matter
- In every oath in the Quran there should be a relation b/w that by which one swears & that upon which the oath is made.
In Quran, tools of oath are 3:
1. و | e.g., [51:23]
2. ب | e.g., [75:1] {Qiyamah is called so ∵ on this day people will be resurrected & will stand before their Lord [83:6]. Also it is the day when justice will be estd [21:47]. Also this is the day when the witnesses will stand up [40:51], [11:18], [4:41]}
3. ت | e.g., [16:56] 

- mentioning that by which one swears or not. There are several verses where it is mentioned & several where it is omitted
  - e.g., [102:8] here the mention of who will ask is omitted
  - [50:1] here *al-muqsam alaih* (that which is sweared upon, or the matter for which the oath is taken) is omitted.

- Benefits of taking oath in Quran:
  1. Making known the greatness of that by which one swears.
  2. Making clear the importance that which is sworn upon (the issue about which we swore) & emphasise. 
- Thus the oath is relevent only in 3 situations: 
  1. That which is sworn upon is important
  2. If the one being addressed is hisitent regarding the matter. So here it is recomended to emphasise by oath but it isn't mandatory


</div>

Part 8  
16:50